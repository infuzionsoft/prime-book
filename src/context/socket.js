import React, { Component } from 'react';
import socketio from "socket.io-client";
import { SOCKET_URL } from "../skyexchange/config/config";

export const socket = socketio.connect(SOCKET_URL);
console.log(socket);
export const SocketContext = React.createContext();
