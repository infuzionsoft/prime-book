import React, { Component } from 'react';

import './skyexchange/assets/css/style.css'
import './skyexchange/assets/css/Responsive.css'

import Dashboard from './skyexchange/pages/Dashboard';
import Inplay from './skyexchange/pages/Inplay';
import MultiMarket from './skyexchange/pages/MultiMarket';
import Cricket from './skyexchange/pages/Cricket';
import Tennis from './skyexchange/pages/Tennis'
import Soccer from './skyexchange/pages/Soccer'
import FullMarket from './skyexchange/pages/FullMarket';
import IndianIPL from './skyexchange/pages/IndianIPL';
import FinancialMarket from './skyexchange/pages/FinancialMarket';
import HorseRacing from './skyexchange/pages/HorseRacing';
import HorseSingle from './skyexchange/pages/HorseSingle';
import Result from './skyexchange/pages/Result';
import SignUp from './skyexchange/pages/SignUp';
import Content from './skyexchange/pages/Content';
import Account from './skyexchange/pages/Account';
import Balance from './skyexchange/pages/Balance';
import AccountStatement from './skyexchange/pages/AccountStatement';
import ProfitLoss from './skyexchange/pages/ProfitLoss';
import Darts from './skyexchange/pages/Darts';
import Race20 from './skyexchange/pages/Race20';
import BetHistory from './skyexchange/pages/BetHistory';
import CasinoReportHistory from './skyexchange/pages/CasinoReportHistory';
import UnsetteledBet from './skyexchange/pages/Unsetteled-Bet';
import changebtnvalue from './skyexchange/pages/ChangeBtnValue';
import ChangePassword from './skyexchange/pages/ChangePassword';
import SecurityAuth from './skyexchange/pages/SecurityAuth';


import CurrentBets from './skyexchange/pages/CurrentBets';
import ActivityLog from './skyexchange/pages/ActivityLog';
import Login from './skyexchange/pages/Login';
import AccountMobile from './skyexchange/pages/AccountMobile';
import { SocketContext, socket } from './context/socket';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from 'react-toastify';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import './skyexchange/assets/fonts/RobotoCondensed-Bold.ttf';
import './skyexchange/assets/fonts/RobotoCondensed-BoldItalic.ttf';
import './skyexchange/assets/fonts/RobotoCondensed-Italic.ttf';
import './skyexchange/assets/fonts/RobotoCondensed-Light.ttf';
import './skyexchange/assets/fonts/RobotoCondensed-LightItalic.ttf';
import './skyexchange/assets/fonts/RobotoCondensed-Regular.ttf';

function App() {

  const [loggedin, setLoggedIn] = React.useState(false);
  let loggedIn = window.sessionStorage.getItem("loggedIn") == null ? false : true;
  React.useEffect(() => {
    loggedIn = window.sessionStorage.getItem("loggedIn");
  }, [loggedin]);
  // alert(loggedIn);
  return (
    <div className="app">
      <ToastContainer
        position="top-center"
        autoClose={2000}
        hideProgressBar={false}
        closeOnClick={true}
        pauseOnHover={true}
        draggable={true}
        progress={undefined}
        limit={1}
      />
      <SocketContext.Provider value={socket}>
        <Router basename="/">
          <Switch>
          
                  <>


                    <Route exact path="/" component={Dashboard} isLoggedin={false} />
                    <Route exact path="/dashboard" component={Dashboard} />
                    <Route exact path="/in-play" component={Inplay} />
                    <Route exact path="/multi-market" component={MultiMarket} />
                    <Route exact path="/cricket" component={Cricket} />
                    <Route exact path="/soccer" component={Soccer} />
                    <Route exact path="/tennis" component={Tennis} />
                    <Route exact path="/game-detail/:event_id" component={FullMarket} />
                    <Route exact path="/indian-premier-league" component={IndianIPL} />
                    <Route exact path="/financial-market" component={FinancialMarket} />
                    <Route exact path="/horse-racing" component={HorseRacing} />
                    <Route exact path="/horse-single" component={HorseSingle} />
                    <Route exact path="/result" component={Result} />
                    <Route exact path="/signup" component={SignUp} />
                    <Route exact path="/content" component={Content} />
                    <Route exact path="/account" component={Account} />
                    <Route exact path="/summary" component={Balance} />
                    <Route exact path="/reports/account-statement" component={AccountStatement} />
                    <Route exact path="/profitloss" component={ProfitLoss} />
                    <Route exact path="/casinoresults" component={CasinoReportHistory} />
                    <Route exact path="/game-list/darts" component={Darts} />
                    <Route exact path="/casino/race/t20" component={Race20} />
                    <Route exact path="/bethistory" component={BetHistory} />
                    <Route exact path="/changepassword" component={ChangePassword} />
                    <Route exact path="/settings/security-auth" component={SecurityAuth} />
                    <Route exact path="/changebtnvalue" component={changebtnvalue} />

                    <Route exact path="/unsetteledbet" component={UnsetteledBet} />


                    <Route exact path="/current-bets" component={CurrentBets} />
                    <Route exact path="/activity-log" component={ActivityLog} />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/profile" component={loggedIn == true ? AccountMobile : Login} />


                    <Route exact path="/m/reports/accountstatement" component={AccountStatement} />
                    <Route exact path="/m/reports/profitloss" component={ProfitLoss} />
                    <Route exact path="/m/home" component={Dashboard} />
                    <Route exact path="/m/reports/bethistory" component={BetHistory} />
                    <Route exact path="/m/reports/unsetteledbet" component={UnsetteledBet} />
                    <Route exact path="/m/reports/casinoresults" component={CasinoReportHistory} />
                    <Route exact path="/m/setting/changebtnvalue" component={changebtnvalue} />
                    <Route exact path="/m/settings/security-auth" component={SecurityAuth} />
                    <Route exact path="/m/setting/changepassword" component={ChangePassword} />
                    <Route exact path="/m/reports//m/rules" component={BetHistory} />
              
                  </>
                
            
          </Switch>
        </Router>
      </SocketContext.Provider>
    </div>
  );
}

export default App;
