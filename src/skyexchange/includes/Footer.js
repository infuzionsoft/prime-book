import React from 'react'

import { makeStyles, Grid, Link, Tabs, Tab, Box, Typography } from '@material-ui/core';

import { BrowserView, MobileView } from "react-device-detect";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    dashboardWrap: {
        width: '75%',
        margin: 'auto',
        marginTop: '2px',
    },
    AlignCenter: {
        alignItems: 'center'
    },
    borderRight: {
        borderRight: '1px solid #999',
        height: '100%'
    },
    marginBottomLast: {
        marginBottom: '24px !important'
    }
}))

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

const Footer = () => {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    return (
        <div className="dashboard-footer">
            <footer className="footer-dark">
                <p class="text-center">
                    © Copyright 2016. All Rights Reserved. Powered by PRIMEBOOK9.
                </p>
            </footer>
        </div>
    )
}

export default Footer;
