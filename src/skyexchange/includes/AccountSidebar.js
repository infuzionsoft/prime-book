import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Tab, ListItemText, ListItem, Tabs } from '@material-ui/core';
import { useHistory } from 'react-router';
import { Typography, Accordion, AccordionSummary, AccordionDetails } from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import background from '../assets/images/bg-purple.png';

import { MoreVert } from '@material-ui/icons';

import { BrowserView, MobileView, isMobile } from "react-device-detect";

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 2,
        backgroundColor: theme.palette.background.paper,
        display: 'flex',
        height: "100%",
    },
    tabs: {
        borderRight: `1px solid ${theme.palette.divider}`,
        width: '230px',
        marginLeft: 'auto',
        background: '#ffffff'
    },
    table: {
        minWidth: 650,
    },
}));

export default function VerticalTabs() {

    const history = useHistory();
    const lastSegment = window.location.pathname;
    let segment = 0;
    if(lastSegment === '/summary'){
        segment = 1;
    }else if(lastSegment === '/account-statement'){
        segment = 2;
    }else if(lastSegment === '/current-bets'){
        segment = 3;
    }else if(lastSegment === '/activity-log'){
        segment = 4;
    }
    

    const openAccount = (URL,search='') => {
        if(URL==='current-bets'){
            history.push({
                pathname:'/'+URL,
                search:search,
            });
        }else{
            history.push(`/${URL}`);
        }
    }

    const classes = useStyles();
    const [value, setValue] = React.useState(segment);
    const [valuex, setxValue] = React.useState(0);
    const [valuen, setnValue] = React.useState(0);
    const [age, setAge] = React.useState('');
    const [open, setOpen] = React.useState(false);

    const handleChange = (event, newValue) => {
       setValue(newValue);
    };
    function ListItemLink(props) {
        return <ListItem button component="a" {...props} />;
    }

    const gridType = isMobile ? 2 : 3;

    return (
        <>
            <Grid item lg={gridType} xs={12} spacing={2}>
                <div className="account-tabs">
                    <Accordion defaultExpanded={true} style={{ marginTop: 10, marginRight: 23 }}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" className="bet-slip-bar">
                            <Typography>Others</Typography>
                        </AccordionSummary>
                        <AccordionDetails className="d-none bet-slip-block" display="inline-block" style={{ height: "100%" }}>
                            <Typography className="bet-slip-content">
                                <nav class="casino collapse show">
                                    <ul>
                                        <li class="nav-item"><Link to="/game-list/darts" class="nav-link"><span>Darts</span></Link></li>
                                        <li class="nav-item"><Link to="/casino/race/t20" class="nav-link"><span>Race 20-20</span></Link></li>
                                        <li class="nav-item"><Link to="/casino/queen" class="nav-link"><span>Casino Queen</span></Link></li>
                                        <li class="nav-item"><Link to="/casino/dt-list" class="nav-link"><span>Dragon Tiger</span></Link></li>
                                        <li class="nav-item"><Link to="/casino/sportscasinolist" class="nav-link"  ><span class="newlacunch-menu">Sports Casino</span></Link></li>
                                        <li class="nav-item"><Link to="/casino/ablist" class="nav-link"><span>Andar Bahar</span></Link></li>

                                        <li class="nav-item"><a href="/casino/bollywood-table" class="nav-link"><span>Bollywood Casino</span></a></li>
                                        <li class="nav-item"><a href="/casino/war" class="nav-link"><span>Casino War</span></a></li>
                                        <li class="nav-item"><a href="/casino/worlilist" class="nav-link"><span>Worli</span></a></li>
                                        <li class="nav-item"><a href="/casino/lottery" class="nav-link"><span>Lottery</span></a></li>
                                        <li class="nav-item"><a href="/casino/3cardj" class="nav-link"><span>3 Cards Judgement</span></a></li>
                                        <li class="nav-item"><a href="/binary-list" class="nav-link"><span>Binary</span></a></li>
                                        <li class="nav-item"><a href="/virtualsports" class="nav-link"><span>Virtual Sports</span></a></li>
                                        <li class="nav-item "><a href="/livecasino" class="nav-link"><span class="newlacunch-menu">Live Casino</span></a></li>
                                        <li class="nav-item"><a href="/slotgame" class="nav-link"><span>Slot Game</span></a></li>
                                        <li class="nav-item"><a href="/cricket-casino" class="nav-link"><span>Cricket Casino</span></a></li>
                                    </ul>
                                </nav>
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                </div>
            </Grid>
        </>
    );
}





