import React from 'react'
import '../assets/css/style.css'
import '../assets/css/Responsive.css'

import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, Box, Typography, Table, TableHead, TableRow, TableCell, TableBody, Slide, InputBase, Dialog, IconButton, Button, BottomNavigation, BottomNavigationAction, Modal, Backdrop, Fade, Grid, Menu, MenuItem, List, ListItem, ListItemText, Divider, FormControlLabel, Checkbox } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { useHistory, useParams } from 'react-router';
import CloseIcon from '@material-ui/icons/Close';
import Marquee from "react-fast-marquee";

import SearchIcon from '@material-ui/icons/Search';
import Person from '@material-ui/icons/Person';
import Setting from '@material-ui/icons/Settings';
import Replay from '@material-ui/icons/Replay';
import CheckBox from '@material-ui/icons/CheckBoxOutlineBlank';
import ArrowDropDown from '@material-ui/icons/ArrowDropDown';
import ExitToApp from '@material-ui/icons/ExitToApp';
import Edit from '@material-ui/icons/Edit';
import Clear from '@material-ui/icons/Clear';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';



import { BrowserView, MobileView, isMobile } from "react-device-detect";

// Import Images
import LOGO from '../assets/images/logo1.png';
import Login from '../assets/images/download.svg';
import Verify from '../assets/images/verifycode.gr';
import Live from '../assets/images/live.svg';
import Account from '../assets/images/account.svg';
import Multi from '../assets/images/multi.svg';
import HomeIcon from '../assets/images/home.svg';
import Clock from '../assets/images/clock.svg';
import Sport from '../assets/images/sport-trophy.svg';
import LoginBg from '../assets/images/bg-login.jpg';
import Transparent from '../assets/images/transparent.gif';
import Loader from '../assets/images/loading40.gif';
import Mic from '../assets/images/mic.svg';
import Appconfig from "../config/config";
import axios from "axios";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearchPlus } from '@fortawesome/free-solid-svg-icons'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons'
import { faHome } from '@fortawesome/free-solid-svg-icons'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { faCaretDown } from '@fortawesome/free-solid-svg-icons'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { faLandmark } from '@fortawesome/free-solid-svg-icons'
import { faAndroid } from '@fortawesome/free-brands-svg-icons'
import { AlertTitle } from '@material-ui/lab';

function eventopen(){
    let dataId = document.getElementById('search-events').getAttribute("data-id");    
    if(dataId == 0){
        document.getElementById('search-events').setAttribute("data-id", "1");
        document.getElementById('search-events').style.width="300px";
        
    }
    else if (dataId == 1) {
        document.getElementById('search-events').setAttribute("data-id", "0");
        document.getElementById('search-events').style.width="0";
    }
}
function searchOpen(iconActive,iconDisable) {
    
    let dataMobileInput = document.getElementById('search-input-header');
    dataMobileInput.classList.toggle("search_input-hover");
    document.getElementById(iconActive).style.display = "none";
    document.getElementById(iconDisable).style.display = "none";
    document.getElementById(iconActive).style.display = "block";
    document.getElementById('iconin-form').classList.add("search-icon-font");
    document.getElementById('iconin-form1').classList.add("search-icon-font");

    
}
function userDropdown() {
    let dropdownID = document.getElementById('dropdown-change').getAttribute("data-dropdown");
    if (dropdownID == 0 ) {
        document.getElementById('dropdown-change').classList.remove("d-none-drop");
        document.getElementById('dropdown-change').setAttribute("data-dropdown", "1");
        // document.getElementById('dropdown-change').setAttribute("class", "d-block-drop");
        document.getElementById("dropdown-change").className += " d-block-drop";


    }
    if (dropdownID == 1 ) {
        document.getElementById('dropdown-change').classList.remove("d-block-drop");
        document.getElementById('dropdown-change').setAttribute("data-dropdown", "0");
        document.getElementById("dropdown-change").className += " d-none-drop";

        // document.getElementById('dropdown-change').setAttribute("class", "d-none-drop");
    }
}
const useStyles = makeStyles((theme) => ({

    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        display: 'block',
    },
    search: {
        position: 'relative',
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: '270px',
        },
        backgroundColor: '#ffffff',
        borderRadius: '6px'
    },
    searchIcon: {
        padding: theme.spacing(0, .5),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff',
        borderRadius: '6px',
        color: '#000000',
        fontSize: '14px !important'
    },
    inputRoot: {
        color: '#000000',
        width: '100%'
    },
    inputInput: {
        padding: theme.spacing(.5, .5, .5, 0),
        paddingLeft: `calc(1em + ${theme.spacing(2)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        fontSize: '14px'
    },
    sectionDesktop: {
        display: 'flex',
    },
    navButton: {
        fontSize: '12px',
        fontWeight: '600',
        padding: '2px 16px',
        textTransform: 'none',
        marginLeft: '8px',
    },
    navBtnImg: {
        marginLeft: '6px'
    },
    navInput: {
        border: 'none',
        borderRadius: '4px',
        marginLeft: '6px',
        fontSize: '12px',
        paddingLeft: '8px',
        width: '122px',

    },
    marginLeftAuto: {
        marginLeft: 'auto'
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
}));
function displayStakeForm() {
    document.getElementById("open-stakeform").style.display = "block";
    document.getElementById("hide-stakebuttons").style.display = "none";
}
function hideStakeForm() {
    document.getElementById("hide-stakebuttons").style.display = "block";
    document.getElementById("open-stakeform").style.display = "none";
}
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="right" ref={ref} {...props} />;
});
const TransitionSetting = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="left" ref={ref} {...props} />;
});
function TableRowLink(props) {
    return <TableRow button component="Link" {...props} />;
}
function createData(name1, value, name2, play, link, calories, fat, carbs, protein) {
    return { name1, value, name2, play, link, calories, fat, carbs, protein };
}

function loader_default() {
    document.getElementById("poker_loading").style.display = "block";

}


function loader_remove() {
    document.getElementById("poker_loading").style.display = "none";

}




function showDetailListing(block_id) {
    document.getElementById("open-bets-table").style.display = "none";

    document.getElementById(block_id).style.display = "block";

}

function closeBackDetail(closeblock) {
    document.getElementById(closeblock).style.display = "none";

    document.getElementById("open-bets-table").style.display = "block";
}

// const OpenBetListDetails = () => (
//     <>
//     </>
// )
const Header = (props) => {
    const params = useParams();
    const isLoggedIn = window.sessionStorage.getItem("loggedIn") && window.sessionStorage.getItem("loggedIn") != "false" ? true : false;
    // const [showBetList, setShowBetList] = React.useState(false)
    // const showDetailListing = () => setShowBetList(true)
    const userInfo = JSON.parse(window.sessionStorage.getItem("userData"));
    const [betopen, betsetOpen] = React.useState(false);
    const [betSlipList, setBetSlipList] = React.useState([]);
    const [openList, setOpenList] = React.useState([]);
    const [tvUrl, setTvUrl] = React.useState("");
    const [showtv, setShowtv] = React.useState(false);

    const handleBetClickOpen = () => {
        betsetOpen(true);
    };

    const handleBetClose = () => {
        betsetOpen(false);
    };
    const [settingXSOpen, settingsetOpen] = React.useState(false);

    const handleSettingClickOpen = () => {
        settingsetOpen(true);
    };

    const handleSettingClose = () => {
        settingsetOpen(false);
    };

    const classes = useStyles();
    const lastSegment = window.location.pathname;
    let segValue = 2;
    let menuName = "";
    if (lastSegment === '/cricket' || lastSegment === '/soccer' || lastSegment === '/tennis') {
        segValue = 0;
    } else if (lastSegment === '/in-play') {
        segValue = 1;
        menuName = 'in-play';
    } else if (lastSegment === '/multi-market') {
        segValue = 3;
        menuName = 'multi-market';
    }

    if (lastSegment === '/cricket') {
        menuName = 'cricket';
    } else if (lastSegment === '/soccer') {
        menuName = 'soccer';
    } else if (lastSegment === '/tennis') {
        menuName = 'tennis';
    } else if (lastSegment === '/indian-premier-league') {
        menuName = 'indian-premier-league';
    } else if (lastSegment === '/financial-market') {
        menuName = 'financial-market';
    } else if (lastSegment === '/horse-racing') {
        menuName = 'horse-racing';
    } else if (lastSegment === '/result') {
        menuName = 'result';
    }

   

    const [value, setValue] = React.useState(segValue);
    const [open, setOpen] = React.useState(false);
    const [open1, setOpen1] = React.useState(false);
    const [login, setLogin] = React.useState(true);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [anchorEl2, setAnchorEl2] = React.useState(null);
    const [anchorEl3, setAnchorEl3] = React.useState(null);
    const [state, setState] = React.useState({
        checkedA: true,
        checkedB: true,
        checkedF: true,
        checkedG: true,
    });

    const handleChange = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
    };
    const history = useHistory();

    const handleOpen = () => {
        setOpen(true);
    };

    const handleOpen1 = () => {
        setOpen1(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleClose1 = () => {
        setOpen1(false);
    };


    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const menuClose = () => {
        setAnchorEl(null);
    };

    const menuClose2 = () => {
        setAnchorEl2(null);
        sessionStorage.clear();
        sessionStorage.setItem("loggedIn", false);
        history.push("/");
    };

    const menuClose3 = () => {
        setAnchorEl3(null);
    };

    const accountClick = (event) => {
        setAnchorEl2(event.currentTarget);
    }

    const settingOpen = (event) => {
        setAnchorEl3(event.currentTarget);
    }

    const navInput = classes.navInput + " xs-none";
    const searchClass = classes.search + " xs-none";
    const loginBtn = classes.navButton + " red-btn login-index";
    const signupBtn = classes.navButton + " dark-btn btn-signup";

    const bottomPage = (urlSegment) => {
        history.push(`/${urlSegment}`);
    }
    const openAccount = (URL, search = '') => {


        if (URL === 'current-bets') {
            history.push({
                pathname: '/' + URL,
                search: search,
            });
        } else {
            history.push(`/${URL}`);
        }
    }
    const rows = [
        createData('Daring king', '(1) 4 -2 (1)', 'maxime cressy', 'In-Play', 'link', 237, 9.0, 37, 4.3),
        createData('Daring king', '(1) 4 -2 (1)', 'maxime cressy', 'In-Play', 'link', 237, 9.0, 37, 4.3),
        createData('Daring king', '(1) 4 -2 (1)', 'maxime cressy', 'In-Play', 'link', 237, 9.0, 37, 4.3),
        createData('Daring king', '(1) 4 -2 (1)', 'maxime cressy', 'In-Play', 'link', 237, 9.0, 37, 4.3),
        createData('Daring king', '(1) 4 -2 (1)', 'maxime cressy', 'In-Play', 'link', 237, 9.0, 37, 4.3),
        createData('Daring king', '(1) 4 -2 (1)', 'maxime cressy', 'In-Play', 'link', 237, 9.0, 37, 4.3),
    ];
   

    const displayType = isLoggedIn && isMobile ? 'inline-block' : 'flex';

    const [MainBalance, setMainBalance] = React.useState(0);
    const [exposure, setExposure] = React.useState(0);
    let pthh = MainBalance - exposure;
    const [showBetInfo, setShowBetInfo] = React.useState(false);

    React.useEffect(() => {
        if (isLoggedIn) {
            getBalance();
            getBetSlip();
            getChips();
            getOpenBets();
        }
        getTvUrl(params.event_id)
    }, [props.isBetPlaced]);
    const [chipList, setchipList] = React.useState([]);
    const handleInputChange = (event) => {
        let index = event.target.getAttribute('data-id');
        // const newChipList = chipList.map((chip, index) => {

        //     if(index == tempIndex)
        //     {
        //         console.log('chipList1',chip);
        //         chipList[index]["chip_value"] =  parseFloat(event.target.value)

        //     }
        // });

        chipList[index]["chip_value"] = parseFloat(event.target.value);


        setchipList([
            ...chipList,
        ]);
        console.log('chipList', chipList);
    }
    function getBalance() {

        var data = JSON.stringify({
            user_id: userInfo._id,
        });

        var config = {
            method: "post",
            url: `${Appconfig.apiUrl}ledger/getUserBalance`,
            headers: {
                "Content-Type": "application/json",
            },
            data: data,
        };

        axios(config)
            .then(function (response) {
                if (response.data.result == 0) {
                } else {
                    console.log(response.data.resultData);
                    setExposure(response.data.resultData.exposure);
                    setMainBalance(response.data.resultData.balance);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    function getBetSlip() {

        var data = JSON.stringify({
            user_id: userInfo._id,
            event_id: props.eventId,
        });

        var config = {
            method: "post",
            url: `${Appconfig.apiUrl}betting/getBettingsByUserAndEventId`,
            headers: {
                "Content-Type": "application/json",
            },
            data: data,
        };

        axios(config)
            .then(function (response) {
                setBetSlipList(response.data.resultData);
                console.log('betslip list ', response.data.resultData);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    function getChips() {


        var data = JSON.stringify({
            "user_id": userInfo._id
        });

        var config = {
            method: 'post',
            url: `${Appconfig.apiUrl}chips/getChips`,
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };

        axios(config)
            .then(function (response) {
                setchipList(response.data);
                let tmpChipList = response.data;
                const newChipList = tmpChipList.map((chipList, index) => {
                    tmpChipList[index]["id"] = index + 1;
                });
                loader_remove();

                setchipList(tmpChipList);

            })
            .catch(function (error) {
                console.log(error);
            });
    }
    function updateChips(chipList) {



        let newdata = [];

        for (var i = 0; i < chipList.length; i++) {
            newdata[i] = { "_id": chipList[i]._id, "chip_value": document.getElementById('stakeEdit_8' + chipList[i]._id).value };
        }


        var data = JSON.stringify({
            "data": newdata
        });

        var config = {
            method: 'post',
            url: `${Appconfig.apiUrl}chips/updateMultipleChips`,
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };

        axios(config)
            .then(function (response) {
                console.log('UPDATED', response.data);
                if (response.data.ok) {
                    hideStakeForm();
                    getChips();
                    // setEditOpens(false);
                    // notify('chips has been updated successfully', 'success')
                }
                else {
                    // notify('error occured', 'error')
                }
            })
            .catch(function (error) {

                console.log(error);
            });
    }

    let expocheck = Math.sign(exposure);

    // console.log('chipList',chipList);

    function getOpenBets() {

        var data = JSON.stringify({
            "user_id": userInfo._id
        });

        var config = {
            method: 'post',
            url: `${Appconfig.apiUrl}reports/openBets`,
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };

        axios(config)
            .then(function (response) {
                // console.log("responseeee", response.data.resultData);
                setOpenList(response.data.resultData);
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    function filterBackBets(bettings) {
        console.log('bettings', bettings);
        let backBets = bettings.filter(element => element.is_back == "1");
        console.log('backBets', backBets);
        return backBets
    }
    function dropdownMenu() {
        document.getElementById("userdropdown").classList.toggle("user-Dropdown");
    
    }

    function filterLayBets(bettings) {
        console.log('bettings', bettings);
        let backBets = bettings.filter(element => element.is_back == "0");
        console.log('backBets', backBets);
        return backBets
    }

    async function getTvUrl(eventId) {
        await fetch(`${Appconfig.marketSarket}livetvurl/${eventId}`)
            .then(response => response.json())
            .then(data => setTvUrl(data.livetv));

    }
    var texttt = "Sydney Thunder WBBL vs Adelaide Strikers WBBL - STW 20 Over Runs Adv ";
    var text22 = "STW 20 Over Runs Adv";
    function logout_session() {
        sessionStorage.removeItem('loggedIn');
        history.push('/dashboard');
    }
    return (
        <>
            {showtv ? <div id="streamingBox" className="tv-fix">
                <iframe src={tvUrl} style={{ width: "99vw" }}></iframe>
            </div> : ""}
            <meta name="viewport" content="width=device-width, initial-scale=1" />
               
                <div className="header logged-out">
                {isMobile ?
                        
                    <div data-v-7355abad="" class="container-fluid mobile-alll-header">
                        <div data-v-7355abad="" class="row row5 pt-1 pb-1">
                            <div data-v-7355abad="" class="logo col-6" style={{marginTop: "-4px"}}>
                                <a data-v-7355abad="" href="/m/home" class="router-link-exact-active router-link-active">
                                    <FontAwesomeIcon className="iconin-form" icon={faHome} style={{fontSize: "17px"}}/>
                                    <img data-v-7355abad="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/themes/primebook9.com/mobile/logo.png" alt="Exchange" class="img-fluid logo" />
                                </a>
                            </div>
                            <div data-v-7355abad="" class="col-6 text-right bal-expo">
                                <p data-v-7355abad="" class="mb-0">
                                    <FontAwesomeIcon className="iconin-form" icon={faLandmark} />
                                    <b data-v-7355abad="">4029.32</b>
                                </p>
                                <span data-v-7c3f5da5="" style={{marginRight: "4px"}}>
                                    <u data-v-7c3f5da5="">Exp:0</u>
                                </span>
                                <div data-v-7c3f5da5="" class="dropdown d-inline-block show" style={{marginTop: "4px"}}>
                                    <a data-v-7c3f5da5="" href="#"  data-toggle="dropdown" class="dropdown-toggle" aria-expanded="true" onClick={dropdownMenu}>
                                        <u data-v-7c3f5da5="">user1155<FontAwesomeIcon className="iconin-form" icon={faCaretDown} /></u>
                                    </a>
                                    <div data-v-7c3f5da5="" id="userdropdown" class="dropdown-menu show" x-placement="top-start" style={{position: "absolute", willChange: "transform", top: "0px", left: "0px", transform: "translate3d(-112px, -20px, 0px)"}}>
                                        <Link data-v-7c3f5da5="" to="/m/home" class="dropdown-item">Home</Link> 
                                        <Link data-v-7c3f5da5="" to="/m/reports/accountstatement" class="dropdown-item">Account Statement</Link> 
                                        <Link data-v-7c3f5da5="" to="/m/reports/profitloss" class="dropdown-item">Profit Loss Report</Link> 
                                        <Link data-v-7c3f5da5="" to="/m/reports/bethistory" class="dropdown-item">Bet History</Link> 
                                        <Link data-v-7c3f5da5="" to="/m/reports/unsetteledbet" class="dropdown-item">Unsetteled Bet</Link> 
                                        <Link data-v-7c3f5da5="" to="/m/reports/casinoresults" class="dropdown-item">Casino Report History</Link> 
                                        <Link data-v-7c3f5da5="" to="/m/setting/changebtnvalue" class="dropdown-item">Set Button Values</Link> 
                                        <Link data-v-7c3f5da5="" to="/m/settings/security-auth" class="dropdown-item">Security Auth Verification</Link> 
                                        <Link data-v-7c3f5da5="" to="/m/setting/changepassword" class="dropdown-item">Change Password</Link> 
                                        <Link data-v-7c3f5da5="" to="#" class="dropdown-item">
                                            Balance
                                            <div data-v-7c3f5da5="" class="custom-control custom-checkbox float-right">
                                                <input data-v-7c3f5da5="" type="checkbox" id="customCheck" class="custom-control-input"/> 
                                                <label data-v-7c3f5da5="" for="customCheck" class="custom-control-label"></label>
                                            </div>
                                        </Link>
                                        <Link data-v-7c3f5da5="" to="#" class="dropdown-item">
                                            Exposure
                                            <div data-v-7c3f5da5="" class="custom-control custom-checkbox float-right">
                                                <input data-v-7c3f5da5="" type="checkbox" id="customCheck1" class="custom-control-input"/> 
                                                <label data-v-7c3f5da5="" for="customCheck1" class="custom-control-label"></label>
                                            </div>
                                        </Link>
                                        <Link data-v-7c3f5da5="" to="/m/rules" class="dropdown-item">Rules</Link> 
                                        <Link data-v-7c3f5da5="" to="/#" class="dropdown-item mt-2 text-danger">
                                            <b data-v-7c3f5da5=""  onClick={logout_session}>Logout</b>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-v-7c3f5da5="" class="row row5 header-bottom">
	                        <div data-v-7c3f5da5="" class="col-12">
		                        <div data-v-7c3f5da5="" class="search-box-container">
			                        <div data-v-7c3f5da5="" class="search-box float-left">
				                        <input data-v-7c3f5da5="" type="text" id="search-input-header" class="search_input"/>
				                        <a data-v-7c3f5da5="" href="javascript:void(0)" id="faSearch" style={{display: "none"}} Class="search_icon" onClick={() => searchOpen('faTimes','faSearch')}>
                                            <i data-v-7c3f5da5="" id="iconin-form" class="">
                                            <FontAwesomeIcon  icon={faTimes} />
                                            </i>
                                        </a>
                                        <a data-v-7c3f5da5="" href="javascript:void(0)" id="faTimes" style={{fontSize: "14px"}} Class="search_icon" onClick={() => searchOpen('faSearch','faTimes')}>
                                            <i data-v-7c3f5da5="" id="iconin-form1" class="">
                                            <FontAwesomeIcon  icon={faSearch} />
                                            </i>
                                        </a>
                                    </div>
		                        </div>
                                <marquee data-v-7c3f5da5="" scrollamount="3" class="searchOpen">Welcome to Our Exchange.</marquee>
	                        </div>
                        </div>
                    </div>                         
                        
                    :

                    <AppBar position="static" className="autogap-header">
                        <Toolbar className="autogap-header-gap" style={{ 'display': displayType, justifyContent: 'space-between' }}>
                            <div style={{ 'display': 'flex', alignItems: 'center' }}>
                                {isMobile && isLoggedIn ? null :
                                    <Typography className={classes.title} variant="h6">
                                        <Link to="/">
                                            <img alt="skyexchange" className="logo header-logo-left" src={LOGO} />
                                        </Link>
                                        
                                    </Typography>
                                }
                            </div>
                        
                                
                                <div className={classes.sectionDesktop}>
                                    <div className="wholerightend">
                                        <div className="click">
                                            <input type="search" id="search-events" data-id="0" placeholder="All Events" />
                                            <i className="search-plus-icon" onClick={eventopen}>
                                                <FontAwesomeIcon className="iconin-form" icon={faSearchPlus} />
                                            </i>                      
                                        </div>
                                        <div className="downloadApp-text">
                                            <div className="rules-div-header"><Link to="#"><strong>Rules </strong></Link></div>
                                            <div className="download-div-header"><Link to="#"><strong>Download Apk <FontAwesomeIcon className="iconin-form" icon={faAndroid} /></strong></Link></div>
                                        </div>
                                        <div className="balance-expo">
                                            <div className="balance-div-header">Balance: <strong>4029.32</strong></div>
                                            <div className="exposure-div-header"><Link to="/#">Exposure: <strong>0</strong></Link></div>
                                        </div>
                                        <div className="dropdown-user">
                                            <span onClick={userDropdown}>user1155<FontAwesomeIcon className="iconin-form" icon={faChevronDown} /></span>
                                            <ul className="dropdown-open" id="dropdown-change" data-dropdown="0">
                                                <li>
                                                    <Link to="/reports/account-statement" className="">Account Statement</Link>
                                                </li>
                                                <li>
                                                    <Link to="/profitloss" className="">Profit Loss Report</Link>
                                                </li>
                                                <li>
                                                    <Link to="/bethistory" className="">Bet History</Link>
                                                </li>
                                                <li>
                                                    <Link to="/unsetteledbet" className="">Unsetteled Bet</Link>
                                                </li>
                                                <li>
                                                    <Link to="/casinoresults" className="">Casino Report History</Link>
                                                </li>
                                                <li>
                                                    <Link to="/changebtnvalue" className="">Set Button Values</Link>
                                                </li>
                                                <li>
                                                    <Link to="/settings/security-auth" className="">Security Auth Verification</Link>
                                                </li>
                                                <li>
                                                    <Link to="/changepassword" className="">Change Password</Link></li>
                                                <li>
                                                    <hr />
                                                </li>
                                                <li>
                                                    <Link to="/#" onClick={logout_session}>signout</Link>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="marqueeDiv">
                                            <marquee>Welcome to Our Exchange.</marquee>
                                        </div>
                                    </div>
                                </div>                      
                        </Toolbar>
                        <div className="clear"></div>
                    </AppBar>
                    }
                   
                </div>

            <BrowserView>
                <div className="navbar onlynavbar">
                    <AppBar position="static">
                        <Toolbar variant="dense" className="navMenu menu-first-cont">
                            <Typography className={menuName === "" ? 'active' : null} variant="p" color="inherit">
                                <Link to="/">
                                    Home
                                </Link>
                            </Typography>
                            <Typography className={menuName === "cricket" ? 'active' : null} variant="p" color="inherit">
                                <Link to="/cricket">
                                    Cricket
                                </Link>
                            </Typography>
                            <Typography className={menuName === "tennis" ? 'active' : null} variant="p" color="inherit">
                                <Link to="/tennis">
                                    Tennis
                                </Link>
                            </Typography>
                            <Typography className={menuName === "soccer" ? 'active' : null} variant="p" color="inherit">
                                <Link to="/soccer">
                                    Football
                                </Link>
                            </Typography>
                            <Typography className={menuName === "indian-premier-league" ? 'active' : null} variant="p" color="inherit">
                                <Link to="/indian-premier-league">
                                    Table Tennis
                                </Link>
                            </Typography>
                            <Typography className={menuName === "horse-racing" ? 'active' : null} variant="p" color="inherit">
                                <Link to="/horse-racing">
                                    Badminton
                                </Link>
                            </Typography>
                            <Typography className={menuName === "result" ? 'active' : null} variant="p" color="inherit">
                                <Link to="/result">
                                    Basketball
                                </Link>
                            </Typography>
                            <Typography className={menuName === "financial-market" ? 'active' : null} variant="p" color="inherit">
                                <Link to="/financial-market">
                                    Volleyball
                                </Link>
                            </Typography>
                            <Typography className={menuName === "financial-market" ? 'active' : null} variant="p" color="inherit">
                                <Link to="/financial-market">
                                    Ice Hockey
                                </Link>
                            </Typography>
                            <Typography className={menuName === "financial-market" ? 'active' : null} variant="p" color="inherit">
                                <Link to="/financial-market">
                                    Baccarat
                                </Link>
                            </Typography>
                            <Typography className={menuName === "financial-market" ? 'active' : null} variant="p" color="inherit">
                                <Link to="/financial-market">
                                    32 Cards
                                </Link>
                            </Typography>
                            <Typography className={menuName === "financial-market" ? 'active' : null} variant="p" color="inherit">
                                <Link to="/financial-market">
                                    Teenpatti
                                </Link>
                            </Typography>
                            <Typography className={menuName === "financial-market" ? 'active' : null} variant="p" color="inherit">
                                <Link to="/financial-market">
                                    Poker
                                </Link>
                            </Typography>
                            <Typography className={menuName === "financial-market" ? 'active' : null} variant="p" color="inherit">
                                <Link to="/financial-market">
                                    Lucky 7
                                </Link>
                            </Typography>
                        </Toolbar>
                    </AppBar>
                </div>
            </BrowserView>
            <MobileView>
                <div className="bottom-nav">
                    <BottomNavigation
                        value={value}
                        onChange={(event, newValue) => {
                            setValue(newValue);
                        }}
                        showLabels
                        className={classes.root}
                    >
                        <BottomNavigationAction onClick={() => bottomPage('cricket')} className="sportlable" label="Sports" icon={<img alt="skyexchange" src={Sport} />} />
                        <BottomNavigationAction onClick={() => bottomPage('in-play')} label="In-Play" icon={<img alt="skyexchange" src={Clock} />} />
                        <BottomNavigationAction onClick={() => bottomPage('')} label="Home" className="home" icon={<img alt="skyexchange" src={HomeIcon} />} />
                        <BottomNavigationAction style={{ letterSpacing: '.4px' }} onClick={() => bottomPage('multi-market')} label="Multi M..." icon={<img alt="skyexchange" src={Multi} />} />
                        <BottomNavigationAction onClick={() => bottomPage('profile')} label="Account" icon={<img alt="skyexchange" src={Account} />} />
                    </BottomNavigation>
                </div>
            </MobileView>

            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className="login-modal">
                        <Grid container>
                            <Grid item xs={6}>
                                <img className="login-modal-img" src={LoginBg} />
                            </Grid>
                            <Grid item xs={6}>
                                <div className="right-login">
                                    <h3>Please login to continue</h3>
                                    <div>
                                        <input placeholder="Username" />
                                    </div>
                                    <div>
                                        <input placeholder="Password" type="password" />
                                    </div>
                                    <div className="validation-input">
                                        <input type="text" placeholder="Validation Code" maxLength="4" className={classes.navInput} />
                                        <img alt="skyexchange" src={Verify} />
                                    </div>
                                    <Button variant="contained" className="w-100 popup-login-btn">Login<img alt="skyexchange" src={Transparent} className={classes.navBtnImg} /></Button>
                                </div>
                            </Grid>
                        </Grid>
                    </div>
                </Fade>
            </Modal>

            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open1}
                onClose={handleClose1}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open1}>
                    <div className={`news-modal`}>
                        <div className="news-head">
                            <h5>Announcement</h5>
                            <Link onClick={handleClose1}>
                                <Clear />
                            </Link>
                        </div>
                        <div className="news-body">
                            <div className="news-block">
                                <div className="news-date">
                                    <span>10</span>
                                    <span className="inner-date">
                                        <span>Sep</span>
                                        <span>2021</span>
                                    </span>
                                </div>
                                <p>Event :- St Lucia Kings v Jamaica Tallawahs .... Market :- JT 6 Over Runs 75-76 (IST 04:51:37 - 04:51:44) ; JT 10 Over Runs 102-104 (IST 04:51:35 - 04:51:52) ; JT 20 Over Runs 191-193 (IST 04:51:35 - 04:51:38) , 192-194 (IST 04:51:39 - 04:51:46) Bets Voided Because of "Wrong Commentary" .....Sorry for the Inconvenience Caused</p>
                            </div>
                            <div className="news-block">
                                <div className="news-date">
                                    <span>10</span>
                                    <span className="inner-date">
                                        <span>Sep</span>
                                        <span>2021</span>
                                    </span>
                                </div>
                                <p>Event :- St Lucia Kings v Jamaica Tallawahs .... Market :- JT 6 Over Runs 75-76 (IST 04:51:37 - 04:51:44) ; JT 10 Over Runs 102-104 (IST 04:51:35 - 04:51:52) ; JT 20 Over Runs 191-193 (IST 04:51:35 - 04:51:38) , 192-194 (IST 04:51:39 - 04:51:46) Bets Voided Because of "Wrong Commentary" .....Sorry for the Inconvenience Caused</p>
                            </div>
                            <div className="news-block">
                                <div className="news-date">
                                    <span>10</span>
                                    <span className="inner-date">
                                        <span>Sep</span>
                                        <span>2021</span>
                                    </span>
                                </div>
                                <p>Event :- St Lucia Kings v Jamaica Tallawahs .... Market :- JT 6 Over Runs 75-76 (IST 04:51:37 - 04:51:44) ; JT 10 Over Runs 102-104 (IST 04:51:35 - 04:51:52) ; JT 20 Over Runs 191-193 (IST 04:51:35 - 04:51:38) , 192-194 (IST 04:51:39 - 04:51:46) Bets Voided Because of "Wrong Commentary" .....Sorry for the Inconvenience Caused</p>
                            </div>
                        </div>
                        <div className="news-footer">
                            <ul className="pages">
                                <li id="prev">
                                    <a href="javascript:void(0);" className="ui-link disable">Prev</a>
                                </li>
                                <li id="pageNumber">
                                    <a href="javascript:void(0);" className="ui-link select">1</a>
                                </li>
                                <li id="next">
                                    <a href="javascript:void(0);" className="ui-link disable">Next</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </Fade>
            </Modal>

            <Menu
                id="setting-list"
                anchorEl={anchorEl3}
                keepMounted
                open={Boolean(anchorEl3)}
                onClose={menuClose3}
                anchorOrigin={{
                    vertical: 'bottom',
                }}
            >
                <ListItem className="default-setting setting-first-list">
                    <ListItemText primary="Default stake" secondary={
                        <>
                            <input value="950" />
                        </>
                    } />
                </ListItem>
                <Divider />
                <ListItem className="default-setting setting-stack">
                    <ListItemText primary="Default stake" secondary={
                        <>
                            <Grid container spaceing={2}>
                                <Grid item md={9}>
                                    <Grid container spaceing={2}>

                                        {chipList.map((row) => (
                                            <Grid item md={3}>
                                                <a>{row.chip_value}</a>
                                            </Grid>
                                        ))}
                                    </Grid>
                                </Grid>
                                <Grid item md={3}>
                                    <a className="edit-settings">Edit <Edit /></a>
                                </Grid>
                            </Grid>
                        </>
                    } />
                </ListItem>
                <Divider />
                <ListItem className="default-setting odd-list">
                    <ListItemText primary="Odds" secondary={
                        <>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={state.checkedA}
                                        onChange={handleChange}
                                        name="checkedA"
                                        color="primary"
                                    />
                                }
                                label="Highlight when odds change"
                            />
                        </>
                    } />
                </ListItem>
                <Divider />
                <ListItem className="default-setting odd-list">
                    <ListItemText primary="Odds" secondary={
                        <>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={state.checkedB}
                                        onChange={handleChange}
                                        name="checkedB"
                                        color="primary"
                                    />
                                }
                                label="Accept Any Odds"
                            />
                        </>
                    } />
                </ListItem>
                <Divider />
                <ListItem className="default-setting odd-list">
                    <ListItemText primary="Odds" secondary={
                        <>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={state.checkedB}
                                        onChange={handleChange}
                                        name="checkedB"
                                        color="primary"
                                    />
                                }
                                label="Accept Any Odds"
                            />
                        </>
                    } />
                </ListItem>
                <Divider />
                <ListItem className="default-setting odd-list">
                    <ListItemText primary="Odds" secondary={
                        <>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={state.checkedB}
                                        onChange={handleChange}
                                        name="checkedB"
                                        color="primary"
                                    />
                                }
                                label="Accept Any Price"
                            />
                        </>
                    } />
                </ListItem>
                <Divider />
                <ListItem className="default-setting setting-button">
                    <Button className="close-btn" variant="contained" >
                        Cancel
                    </Button>
                    <Button className="close-btn save-btn" variant="contained">
                        Save
                    </Button>
                </ListItem>
                <Divider />
            </Menu>
            <Dialog fullScreen open={betopen} onClose={handleBetClose} TransitionComponent={Transition} className="bet-popup">
                <AppBar className='header'>
                    <Box className="xs-bets-popup" display="flex" justifyContent="space-between">
                        <Box className="header-name">
                            <img src={Transparent} alt="bets" />
                            Open Bets
                        </Box>
                        <Box>
                            <IconButton edge="start" color="inherit" onClick={handleBetClose} aria-label="close">
                                <CloseIcon />
                            </IconButton>
                        </Box>
                    </Box>
                </AppBar>
                <List className="bet-body-popup">
                    <Table className={classes.table} aria-label="simple table">
                        <TableBody className="tbl_body">
                            <div id="open-bets-table">
                                {openList.length > 0 && openList.map((row) => (
                                    <TableRow to={row.link} p={1} key={row.name1} className="odd_even_clr" button>
                                        <TableCell className="table_first_row" colSpan="5">
                                            <div className="text_left-in-play">
                                                <FiberManualRecordIcon className="circle-size" />
                                                <Link to="#" style={{ color: '#2789CE' }} onClick={() => showDetailListing('details-container-open' + row._id)}>

                                                    <Box display="contents">
                                                        <span> {`${row.remarks.substring(0, 32)}...`} </span>
                                                        {/* <span className="in-play"> v </span>
                                                    <span>{row.name2}</span> */}
                                                    </Box>
                                                </Link>
                                            </div>
                                        </TableCell>
                                        <TableCell className="arrow-icon" style={{ width: "30px" }}>
                                            <Link className="inplay-last-icon"></Link>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </div>
                            <TableRow>
                                {openList.length > 0 && openList.map((row) => (
                                    <div className="details-container-open-bets" id={`details-container-open${row._id}`} style={{ display: 'none' }}>
                                        <div id="detailOpenHead" className="detail-open-head">
                                            <Link id="detailBack" className="detail-back" to="#" onClick={() => closeBackDetail('details-container-open' + row._id)}></Link>
                                            <ul>
                                                <li id="eventName">{`${row.remarks.substring(0, 36)}...`}</li>
                                            </ul>
                                        </div><h3 id="openheadHeader"> Matched </h3><div id="txnList" className="slip-list" style={{ 'display': 'block' }}>




                                            {
                                                filterBackBets(row.bettings).length > 0 ?
                                                    <>
                                                        <dl id="header_back" className="bets-head" style={{ 'display': 'block' }}>
                                                            <dt>
                                                                <h4>Back</h4>
                                                            </dt>
                                                            <dd className="col-odds"><span>Odds</span></dd>
                                                            <dd className="col-stake"><span>Stake</span></dd>
                                                            <dd className="col-profit"><span>Profit</span></dd>
                                                        </dl>

                                                        {filterBackBets(row.bettings).length > 0 && filterBackBets(row.bettings).map((rowinner) => (
                                                            rowinner.is_back == 1 ?
                                                                <><dd id="betInfo" className={`col-ref backk ${showBetInfo ? 'show-bet-info' : 'hide-bet-info'}`}>
                                                                    <ul>
                                                                        <li id="betId">Ref {rowinner._id.substr(rowinner._id.length - 6)}</li>
                                                                        <li id="placeDate">
                                                                            {moment(rowinner.createdAt).format('DD-MM-YYYY  hh:mm ')}
                                                                        </li>
                                                                    </ul>
                                                                </dd>
                                                                    {console.log('working', rowinner)}
                                                                    <dl id="txn_692282101" className="bets-back" style={{ 'display': 'block' }}>
                                                                        <dt>
                                                                            <span id="sideType" className="back">Yes</span>
                                                                            <h4 id="selectionName">{`${rowinner.place_name.substring(0, 14)}...`}</h4>
                                                                        </dt>
                                                                        <dd id="odds" className="col-odds"><span>{rowinner.price_val}</span></dd>
                                                                        <dd id="stake" className="col-stake" title=" 100"><span> {rowinner.stake}</span></dd>
                                                                        <dd id="pl" className="col-profit"><span> {rowinner.profit.toFixed(2)}</span></dd>
                                                                        <div className="clear"></div>
                                                                    </dl></> : ''
                                                        ))}
                                                    </> : null
                                            }

                                            {
                                                filterLayBets(row.bettings).length > 0 ?
                                                    <>
                                                        <dl id="header_lay" className="bets-head" style={{ 'display': 'block' }}>
                                                            <dt>
                                                                <h4>Lay</h4>
                                                            </dt>
                                                            <dd className="col-odds"><span>Odds</span></dd>
                                                            <dd className="col-stake"><span>Stake</span></dd>
                                                            <dd className="col-profit"><span>Liability</span></dd>
                                                        </dl>

                                                        {filterLayBets(row.bettings).length > 0 && filterLayBets(row.bettings).map((rowinner) => (
                                                            rowinner.is_back == 0 ?
                                                                <><dd id="betInfo" className={`col-ref layy ${showBetInfo ? 'show-bet-info' : 'hide-bet-info'}`} >
                                                                    <ul>
                                                                        <li id="betId">Ref {rowinner._id.substr(rowinner._id.length - 6)}</li>
                                                                        <li id="placeDate">{moment(rowinner.createdAt).format('DD-MM-YYYY  hh:mm ')}</li>
                                                                    </ul>
                                                                </dd>
                                                                    <dl id="txn_692282101" className="bets-lay" style={{ 'display': 'block' }}>
                                                                        <dt>
                                                                            <span id="sideType" className="lay">Yes</span>
                                                                            <h4 id="selectionName">{`${rowinner.place_name.substring(0, 14)}...`}</h4>
                                                                        </dt>
                                                                        <dd id="odds" className="col-odds"><span>{rowinner.price_val}</span></dd>
                                                                        <dd id="stake" className="col-stake" title=" 100"><span> {rowinner.stake}</span></dd>
                                                                        <dd id="pl" className="col-profit"><span> {rowinner.profit.toFixed(2)}</span></dd>
                                                                        <div className="clear"></div>
                                                                    </dl></> : ''
                                                        ))}
                                                    </> : null
                                            }


                                        </div><ul id="openBetOption" className="check-list" style={{ 'display': 'flex' }}>
                                            <li><Link id="showBetInfo" className="" to="#" onClick={() => {
                                                setShowBetInfo(!showBetInfo)
                                            }
                                            }>Bet Info</Link></li>
                                            <li name="txnOption" style={{ 'display': 'none' }}><Link id="averageOdds" className="" to="#">Average Odds</Link></li>
                                        </ul>

                                    </div>
                                ))}
                            </TableRow>
                        </TableBody>

                    </Table>


                </List>
            </Dialog>

            <Dialog fullScreen open={settingXSOpen} onClose={handleSettingClose} TransitionComponent={TransitionSetting} className="set-popup">
                <AppBar className='header'>
                    <Box className="xs-setting-popup" display="flex" justifyContent="space-between">
                        <Box className="header-name">
                            <Setting />Setting
                        </Box>
                        <Box>
                            <IconButton edge="start" color="inherit" onClick={handleSettingClose} aria-label="close">
                                <CloseIcon />
                            </IconButton>
                        </Box>
                    </Box>
                </AppBar>
                <List className="popup-setting-body">
                    <Box fontWeight="fontWeightBold" fontSize={14} className="header-set">
                        <Typography variant="span">Stack</Typography>
                    </Box>
                    <ListItem className="default-setting setting-first-list">
                        <ListItemText primary="Default stake" secondary={
                            <>
                                <input type="number" className="userCoin" value="915" />
                            </>
                        } />
                    </ListItem>
                    <Divider />
                    <ListItem className="default-setting setting-stack hello">
                        <ListItemText primary="Quick stakes" secondary={
                            <>
                                <Grid container spaceing={2} id="hide-stakebuttons">
                                    <Grid item md={9} xs={12}>
                                        <Grid container spaceing={2}>

                                            {chipList.map((row) => (
                                                <Grid item md={3} xs={3}>
                                                    <label>
                                                        <input type="checkbox" />
                                                        <a>{row.chip_value}</a>
                                                    </label>
                                                </Grid>
                                            ))}
                                        </Grid>
                                    </Grid>
                                    <Grid item md={3} xs={12}>
                                        <Link className="edit-settings" onClick={displayStakeForm}>Edit Stakes<Edit /></Link>
                                    </Grid>
                                </Grid>
                                <div className="grid-text-boxes" id="open-stakeform">
                                    <dl id="editCustomizeStakeList" className="setting-block stake-custom-edit stake-setting">
                                        <dt>Quick Stakes</dt>

                                        {chipList.map((row, index) => (
                                            <dd><div className="ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset">
                                                <input id={`stakeEdit_8${row._id}`} type="text" onChange={handleInputChange} pattern="[0-9]*" data-id={index} value={row.chip_value} /></div>
                                            </dd>
                                        ))}
                                        <dd className="col-stake_edit"><Link id="ok" onClick={() => updateChips(chipList)} className="btn-send ui-link" to="#">OK</Link></dd>
                                    </dl>
                                </div>
                            </>
                        } />
                    </ListItem>
                    <Divider />
                    <Box fontWeight="fontWeightBold" fontSize={14} className="header-set">
                        <Typography variant="span">Odds</Typography>
                    </Box>
                    <ListItem className="default-setting odd-list">
                        <ListItemText primary="" secondary={
                            <>
                                <label className="switch">
                                    Highlight when odds change
                                    <input type="checkbox" checked />
                                    <div className="slider round">
                                        <span className="on"></span>
                                        <span className="off"></span>
                                    </div>
                                </label>
                            </>
                        } />
                    </ListItem>
                    <Divider />
                    <Box fontWeight="fontWeightBold" fontSize={14} className="header-set">
                        <Typography variant="span">FancyBet</Typography>
                    </Box>
                    <ListItem className="default-setting odd-list">
                        <ListItemText primary="" secondary={
                            <>
                                <label className="switch">
                                    Accept Any Odds
                                    <input type="checkbox" />
                                    <div className="slider round">
                                        <span className="on"></span>
                                        <span className="off"></span>
                                    </div>
                                </label>
                            </>
                        } />
                    </ListItem>
                    <Divider />
                    <Box fontWeight="fontWeightBold" fontSize={14} className="header-set">
                        <Typography variant="span">Sportsbook</Typography>
                    </Box>
                    <ListItem className="default-setting odd-list">
                        <ListItemText primary="" secondary={
                            <>
                                <label className="switch">
                                    Accept Any Odds
                                    <input type="checkbox" />
                                    <div className="slider round">
                                        <span className="on"></span>
                                        <span className="off"></span>
                                    </div>
                                </label>
                            </>
                        } />
                    </ListItem>
                    <Divider />
                    <Box fontWeight="fontWeightBold" fontSize={14} className="header-set">
                        <Typography variant="span">Binary</Typography>
                    </Box>
                    <ListItem className="default-setting odd-list">
                        <ListItemText primary="" secondary={
                            <>
                                <label className="switch">
                                    Accept Any Price
                                    <input type="checkbox" />
                                    <div className="slider round">
                                        <span className="on"></span>
                                        <span className="off"></span>
                                    </div>
                                </label>
                            </>
                        } />
                    </ListItem>
                    <Divider />
                    <ListItem className="default-setting setting-button">
                        <Button className="close-btn" variant="contained" >
                            Cancel
                        </Button>
                        <Button className="close-btn save-btn" variant="contained">
                            Save
                        </Button>
                    </ListItem>
                </List>
            </Dialog>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={menuClose}
            >
                <List className={classes.root}>
                    <ListItem className="main-balance balance-list">
                        <ListItemText primary="SABA Balance" secondary={
                            <>
                                <span className="pth">PTH</span>
                                <span className="points">0.53</span>
                                <Divider />
                                <div className="main-footer">
                                    <span>Exposure</span>
                                    <span>0.00</span>
                                </div>
                            </>
                        } />
                        <Divider />
                    </ListItem>
                </List>
                <List className={classes.root}>
                    <ListItem className="balance-list">
                        <ListItemText primary="SABA Balance" secondary={
                            <>
                                <span className="pth">PTH</span>
                                <span className="points">0.53</span>
                            </>
                        } />
                        <Button className="recall-btn">Recall</Button>
                    </ListItem>
                    <ListItem className="balance-list">
                        <ListItemText primary="Casino Balance" secondary={
                            <>
                                <span className="pth">PTH</span>
                                <span className="points">0</span>
                            </>
                        } />
                        <Button className="recall-btn">Recall</Button>
                    </ListItem>
                    <ListItem className="balance-list">
                        <ListItemText primary="BPoker Balance" secondary={
                            <>
                                <span className="pth">PTH</span>
                                <span className="points">0</span>
                            </>
                        } />
                        <Button className="recall-btn">Recall</Button>
                    </ListItem>
                    <ListItem className="balance-list">
                        <ListItemText primary="&nbsp;" secondary={
                            <>
                                <span className="points"></span>
                            </>
                        } />
                        <Button className="recall-btn">Recall All</Button>
                    </ListItem>
                </List>
                <Button variant="contained" onClick={menuClose} className="close-btn">Close</Button>
            </Menu>

        </>
    )

}

export default Header;
