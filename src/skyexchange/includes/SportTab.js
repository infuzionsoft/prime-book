import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles, AppBar, Tabs, Tab, Typography, Box, Button } from '@material-ui/core';
import Drawer from '@material-ui/core/Drawer';
import Search from '@material-ui/icons/Search';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIos from '@material-ui/icons/ArrowBackIos';
import Close from '@material-ui/icons/Close';
import SearchIcon from '@material-ui/icons/Search';
import { useHistory } from 'react-router';

import Transparent from '../assets/images/transparent.gif'
import Live from '../assets/images/live.svg';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-force-tabpanel-${index}`}
            aria-labelledby={`scrollable-force-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `scrollable-force-tab-${index}`,
        'aria-controls': `scrollable-force-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    list: {
        padding: "16px 10px"
    },
    fullList: {
        width: 'auto',
    },
    input: {
        flex: 1,
        width: '70%'
    },
    iconButton: {
        padding: 10,
        width: '10%'
    },
    divider: {
        height: 28,
        margin: 4,
    },
}));


const SportTab = () => {

    const classes = useStyles();
    const lastSegment = window.location.pathname;
    let segValue = 0
    if (lastSegment === '/soccer') {
        segValue = 2;
    } else if (lastSegment === '/tennis') {
        segValue = 3;
    }
    const [value, setValue] = React.useState(segValue);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const history = useHistory();

    const openSport = (urlValue) => {
        history.push(`/${urlValue}`);
    }

    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });

    const toggleDrawer = (anchor, open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setState({ ...state, [anchor]: open });
    };

    const list = (anchor) => (
        <div
            className={clsx(classes.list, {
                [classes.fullList]: anchor === 'top' || anchor === 'bottom',
            })}
            role="presentation"
        >
            {['Inbox'].map((text, index) => (
                <Paper component="form" className={classes.root}>
                    <IconButton type="button" onClick={toggleDrawer(anchor, false)} className={classes.iconButton} aria-label="search">
                        <ArrowBackIos />
                    </IconButton>
                    <InputBase
                        className={classes.input}
                        placeholder="Search Events"
                        inputProps={{ 'aria-label': 'search google maps' }}
                    />
                    <IconButton type="button" className={classes.iconButton} aria-label="search">
                        <Close />
                    </IconButton>
                    <IconButton type="submit" className={classes.iconButton} aria-label="search">
                        <SearchIcon />
                    </IconButton>
                </Paper>
            ))}
        </div>
    );

    return (
        <>
            <div className="sport-xs-tab">
                <AppBar position="static" color="default">
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        variant="scrollable"
                        scrollButtons="on"
                        indicatorColor="primary"
                        textColor="primary"
                        aria-label="scrollable force tabs example"
                    >
                        <Tab label={
                            <>
                                <div className="aaa">Cricket</div>
                                <span className="live-number">
                                    <strong><img alt="skyexchange" src={Live} /></strong>
                                    
                                </span>
                            </>
                        } className="cricket-img" icon={<img src={Transparent} />} onClick={() => openSport('cricket')} {...a11yProps(0)} ></Tab>
                        <Tab label="IPL Winner" className="cricket-img" icon={<img src={Transparent} />} onClick={() => openSport('indian-premier-league')} {...a11yProps(1)} />
                        <Tab label={
                            <>
                                <div className="aaa">Soccer</div>
                                <span className="live-number">
                                    <strong><img alt="skyexchange" src={Live} /></strong> 12
                                </span>
                            </>
                        } className="soccer-img" icon={<img src={Transparent} />} onClick={() => openSport('soccer')} {...a11yProps(1)} />
                        <Tab label={
                            <>
                                <div className="aaa">Tennis</div>
                                <span className="live-number">
                                    <strong><img alt="skyexchange" src={Live} /></strong> 12
                                </span>
                            </>
                        } className="tennis-img" icon={<img src={Transparent} />} onClick={() => openSport('tennis')} {...a11yProps(1)} />
                    </Tabs>
                </AppBar>
                {['top'].map((anchor) => (
                    <React.Fragment key={anchor}>
                        <Button className="search-xs" onClick={toggleDrawer('top', true)}><Search /></Button>
                        <Drawer anchor={anchor} open={state[anchor]} onClose={toggleDrawer(anchor, false)}>
                            {list(anchor)}
                        </Drawer>
                    </React.Fragment>
                ))}
            </div>
            <div className="clear-fix"></div>
        </>
    )
}

export default SportTab;