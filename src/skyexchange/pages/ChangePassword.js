import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { List, ListItemText, ListItem, Tabs, Button } from '@material-ui/core';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Grid, AppBar, InputLabel } from '@material-ui/core';
import moment from 'moment';
import Appconfig from "../config/config";
import Footer from "../includes/Footer";
import axios from "axios";

import { isMobile } from "react-device-detect";

import Header from '../includes/Header';
import AccountSidebar from '../includes/AccountSidebar';

const gridType = isMobile ? 10 : 8;
export default function ChangePassword() {
    return (
        <>
       
         {isMobile ? 
            <>
            <meta name="viewport" content="width=1300" />
            <Header /> 
            <Grid className="account-stat" container spacing={0}>
                <Grid item lg={gridType} xs={12} spacing={2}>
                    {/* <div className="helloaccount">
                        Hello
                    </div> */}
                    <div class="report-container">
                        
                        <div class="card" style={{border: "none"}}>
                            <div class="card-header">
                                <h4 class="mb-0">Change Password</h4></div>
                            <div class="card-body container-fluid container-fluid-5">
                                <div class="row row5 mt-2">
                                    <div class="col-12">
                                        <div class="form-group form-group1">
                                            <label>Current Password</label>
                                            <input type="password" class="form-control"/>
                                        </div>
                                        <div class="form-group form-group1">
                                            <label>New Password</label>
                                            <input type="password" class="form-control"/>
                                        </div>
                                        <div class="form-group form-group1">
                                            <label>Confirm New Password</label>
                                            <input type="password" class="form-control"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row5 mt-2">
                                    <div class="col-12">
                                        <button class="btn btn-primary btn-block btn-sm" style={{marginBottom: "40px",fontWeight: "400",fontSize: "1.2rem"}}>Change Password</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                   
                </Grid>
            </Grid>
            </>
            : 
            <>
            <Header /> 
           <div class="row row5">
                <div className="sidebar-whold-cont">
                    <AccountSidebar />
                </div> 
                <div class="col-md-10 report-main-content m-t-5">
	<div class="card">
		<div class="card-header-desktop">
			<h4 class="mb-0">Change Password</h4></div>
		<div class="card-body container-fluid container-fluid-5 change-password">
			<div class="row row5 mt-2">
				<div class="col-4">
					<div class="form-group">
						<label>Current Password</label>
						<input type="password" class="form-control"/>
					</div>
					<div class="form-group">
						<label>New Password</label>
						<input type="password" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Confirm New Password</label>
						<input type="password" class="form-control"/>
					</div>
				</div>
			</div>
			<div class="row row5 mt-2">
				<div class="col-12">
					<button class="btn btn-primary-desktop">Change Password</button>
				</div>
			</div>
		</div>
	</div>
</div>
            </div>
            <Footer />    
            </>}
        </>

    )
}
