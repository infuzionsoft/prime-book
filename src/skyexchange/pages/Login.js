import React, { useState } from 'react';
// import ReactDOM from 'react-dom'
import { Button, Grid, makeStyles, Tabs, Tab, Box, Typography } from '@material-ui/core';
import { Link, useHistory } from 'react-router-dom';

import Email from '@material-ui/icons/Email';
import Telegram from '@material-ui/icons/Telegram';
import WhatsApp from '@material-ui/icons/WhatsApp';
import Instagram from '@material-ui/icons/Instagram';
import Clear from '@material-ui/icons/Clear';
import Skype from '../assets/images/skype.svg';

// Import Images
import LOGO from '../assets/images/logo1.png';
import Verify from '../assets/images/verifycode.gr';
import Transparent from '../assets/images/transparent.gif';
import BetFair from '../assets/images/betfair.png';
import ErrorEx from '../assets/images/error-ex.png';
import axios from 'axios';
import Appconfig from '../config/config';
import { toast } from 'react-toastify';
import { FormControl, Input, FormControlLabel, Checkbox } from '@material-ui/core';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import CaptchaTest from '../includes/captcha'; 
import ReactDOM from 'react-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHandPointDown } from '@fortawesome/free-solid-svg-icons'
import { faSignInAlt } from '@fortawesome/free-solid-svg-icons'
import { faAndroid } from '@fortawesome/free-brands-svg-icons'
const useStyles = makeStyles((theme) => ({
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        display: 'block',
    },
    search: {
        position: 'relative',
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: '270px',
        },
        backgroundColor: '#ffffff',
        borderRadius: '6px'
    },
    searchIcon: {
        padding: theme.spacing(0, .5),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff',
        borderRadius: '6px',
        color: '#000000',
        fontSize: '14px !important'
    },
    inputRoot: {
        color: '#000000',
        width: '100%'
    },
    inputInput: {
        padding: theme.spacing(.5, .5, .5, 0),
        paddingLeft: `calc(1em + ${theme.spacing(2)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        fontSize: '14px'
    },
    sectionDesktop: {
        display: 'flex',
    },
    navButton: {
        fontSize: '12px',
        fontWeight: '600',
        padding: '2px 16px',
        textTransform: 'none',
        marginLeft: '8px',
        color: '#ffffff'
    },
    navBtnImg: {
        marginLeft: '6px'
    },
    navInput: {
        border: 'none',
        borderRadius: '4px',
        marginLeft: '6px',
        fontSize: '12px',
        paddingLeft: '8px',
        width: '122px',

    },
    marginLeftAuto: {
        marginLeft: 'auto'
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
}));

// function TabPanel(props) {
//     const { children, value, index, ...other } = props;

//     return (
//         <div
//             role="tabpanel"
//             hidden={value !== index}
//             id={`simple-tabpanel-${index}`}
//             aria-labelledby={`simple-tab-${index}`}
//             {...other}
//         >
//             {value === index && (
//                 <Box p={3}>
//                     <Typography>{children}</Typography>
//                 </Box>
//             )}
//         </div>
//     );
// }

const Login = (props) => {

    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const history = useHistory();
    const [formData, setformData] = useState({
        user_name: '',
        password: '',
        site_code: Appconfig.sitecodes
        // validation_checker:''
    });



    const [validationError, SetValidationError] = useState("");

    function checkValidation(){

        var format = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
        let errorText;

        // document.getElementById("user_name").style.background = "";        
        // document.getElementById("password").style.background = "";
        // document.getElementById("user_captcha_input").style.background = "";        
        // document.getElementById("erroruserid").style.display = "none";               
        // document.getElementById("errorpassid").style.display = "none";            

         if(formData.user_name==""){
            // document.getElementById("user_name").style.background = "#fbd7d3"; 
            document.getElementById("erroruserid").style.display = "block";               
            errorText= ""
            SetValidationError(errorText);
            return false;
        }else if(formData.password =="" ) {
            // document.getElementById("password").style.background = "#fbd7d3";    
            document.getElementById("errorpassid").style.display = "block";            
            errorText= ""            
            SetValidationError(errorText);                  
            return false;
        }
        // else if(formData.validation_checker =="" ) {
        //     document.getElementById("user_captcha_input").style.background = "#fbd7d3";   
        //     errorText= "Validation code is empty"            
        //     SetValidationError(errorText);                  
        //     return false;
        // }
        // else if(format.test(formData.user_name) == true) {

        //     errorText= "Username is only allow a-z and 0-9."            
        //     SetValidationError(errorText); 

        //     return false;
        // }
         else {
            errorText = ""
            SetValidationError(errorText);
            return true;
        }


//        return false;

    }

    const handleInputChange = (event) => {
        setformData({
            ...formData,
            [event.target.name]: event.target.value
        });
        console.log(formData);
    }
    const handleSubmit = (event) => {
        event.preventDefault();
        if( checkValidation()){
            document.getElementById('login-message').style.display="none";

        var config = {
            method: 'post',
            url: `${Appconfig.apiUrl}users/userAuthenticate`,
            headers: {
                'Content-Type': 'application/json',
            },
            data: JSON.stringify(formData)
        };
        axios(config)
            .then(function (response) {
                console.log(response);
                // storeUserinfo(response.data.resultData);
                if (response.data.result) {
                     //notify(response.data.resultMessage, 'success')
                    storeUserinfo(response.data.resultData);
                   // alert("Success");
                    props.setLoggedIn(true)

                }
                else {
                    // notify(response.data.resultMessage, 'error')
                    //alert("username or password is invalid");
                    var errorText = "";
                    errorText= "The username or password is incorrect1"
                    SetValidationError(errorText);
                    document.getElementById('login-message').style.display="block";


                }
            })
            .catch(function (error) {
                console.log(error);
            });
        
        } 

    }
    
    // function notify(message, type) {
    //     toast.configure();
    //     if (type === 'error') {
    //         toast.error(message, {
    //             toastId: "error"
    //         });
    //     }
    //     else {
    //         toast.success(message, {
    //             toastId: "success"
    //         });
    //     }
    // }
    
    function storeUserinfo(userdata) {
        window.sessionStorage.setItem("loggedIn", true);
        window.sessionStorage.setItem("userData", JSON.stringify(userdata));
        setTimeout(function () {
            history.push('/dashboard');
        }, 2000);
    }
    return (
        <>  
            <Grid container className="login-section">
                <Grid container className="login-inner-section">

                <Grid item xs={12} className="login-cont">
                    <Grid item xs={12}>
                        <div className="login-header">
                            <img src={LOGO} />
                        </div>
                    </Grid>
                    <Grid className="form-login-all" item xs={12}>
                        <ValidatorForm

                            onSubmit={handleSubmit}
                            autoComplete="off"
                        >
                            <div className="right-login login-form">
                                <h4 class="text-center login-headingwhite">LOGIN <i class="fas fa-hand-point-down"></i>
                                <FontAwesomeIcon icon={faHandPointDown} />
                                </h4>
                                {/* <span>{validationError} </span> */}
                                <div role="alert" id="login-message" className="alert alert-danger">
                                    <ul>
                                        <li>{validationError}</li>
                                    </ul>
                                </div>
                                
                                <div className="form-cont-single">
                                    <input className="formstyletext" validators={['required']} autoComplete="off"
                                        errorMessages={['this field is required']} value={formData.user_name} helperText="Username" placeholder="Username" id="user_name" name="user_name" onChange={handleInputChange} />
                                        <i className="icon-cont-form">
                                            <FontAwesomeIcon className="iconin-form" icon={faHandPointDown} />
                                        </i>
                                    <span className="error-ex-cont"><img id="erroruserid" src={ErrorEx} className="errow-ex" /></span>
                                </div>
                                <div className="form-cont-single">
                                    <input className="formstyletext" fullWidth validators={['required']} autoComplete="off"
                                        errorMessages={['this field is required']}  id="password" type="password" value={formData.password} helperText="Password" placeholder="Password" name="password" onChange={handleInputChange} />
                                    <i className="icon-cont-form">
                                        <FontAwesomeIcon className="iconin-form" icon={faHandPointDown} />
                                    </i>
                                    <span className="error-ex-cont"><img id="errorpassid" src={ErrorEx} className="errow-ex" /></span>
                                </div>
                                <div className="custom-captcha">
                                    {/* <CaptchaTest /> */}                        
                                </div>
                                <span className="error-ex-cont"><img id="errorid" src={ErrorEx} className="errow-ex" /></span>
                                <Button type="submit" variant="contained" class="w-100 popup-login-btn">
                                    Login          
                                    <i className="icon-cont-form">
                                        <FontAwesomeIcon className="iconin-form" icon={faSignInAlt} />
                                    </i>                      
                                </Button>
                                
                                <div className="login-privacy-cont">
                                    <small>This site is protected by reCAPTCHA and the Google <Link>Privacy Policy</Link> and <Link>Terms of Service</Link> apply.</small>    
                                </div>
                                <Button type="button" variant="contained" class="w-100 download-app-login popup-login-btn blinking">
                                    <i className="icon-cont-form-android">
                                        <FontAwesomeIcon className="iconin-form" icon={faAndroid} />
                                    </i>                      
                                    <b> Download Apk</b>
                                </Button>

                            </div>
                        </ValidatorForm>
                    </Grid>
                </Grid>
                </Grid>
            </Grid>
        </>
    );
};

export default Login;
