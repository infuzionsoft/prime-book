import React, { useState } from 'react'
import Header from '../includes/Header'
import BetSlip from '../includes/BetSlip';
import LeftSideBar from '../includes/LeftSideBar';

import { Grid, Table, TableRow, TableCell, TableHead, TableBody, Link, makeStyles } from '@material-ui/core'
import { ArrowForwardIos, MoreVert } from '@material-ui/icons';

import { BrowserView, MobileView, isMobile } from 'react-device-detect';


import GreenPin from '../assets/images/green-pin-off.svg';
import MinMax from '../assets/images/min-max.svg';
import Depth from '../assets/images/depth.svg';
import BackballDisabled from '../assets/images/bg-backall-disabled.png'
import transparent from '../assets/images/transparent.gif';
import pin from '../assets/images/pin.png';
import darkPin from '../assets/images/dark-pin-off.svg';
import Time from '../assets/images/time.png';

import ReplayIcon from '@material-ui/icons/Replay';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Loader from '../assets/images/loading40.gif';

const mainMenu = () => {
    return (
        <div>
            <MoreVert /> <ArrowForwardIos />
        </div>
    )
}

function TableRowLink(props) {
    return <TableRow button component="Link" {...props} />;
}
function loader_default(){
    document.getElementById("poker_loading").style.display = "block";  
}
  
function loader_remove(){
    document.getElementById("poker_loading").style.display = "none";
}
const useStyles = makeStyles((theme) => ({
    root: {
        height: '100%',
    },
    tableHeight: {
        height: 'calc(100vh - 120px)',
    },
}));

const MultiMarket = () => {

    const classes = useStyles();
    const [match, setMatch] = useState(true)
    const isLoggedIn = window.sessionStorage.getItem("loggedIn") && window.sessionStorage.getItem("loggedIn") != "false" ? true : false;
    React.useEffect(() => {
        setTimeout(() => {
          loader_remove();
        }, 1500);
      }, []);
    return (
        <>
            <Header />
            {isLoggedIn ? null :
                <div id="poker_loading" class="loading-wrap" style={{ 'marginTop': '50px' }}>
                    <ul class="loading">
                        <li><img src={Loader} /></li>
                        <li>Loading…</li>
                    </ul>
                </div>
            }
            <Grid container>
                <Grid item lg={2} xs={12} className="xs-none">
                    <LeftSideBar />
                </Grid>
                <Grid lg={7} xs={12}>
                    {match ?
                        <>
                            <div className="match-block">
                                <div className="match-block-header">
                                    <h5>Cricket</h5>
                                    <span><img src={Time} /> In-Play</span>
                                </div>
                                <Table className={classes.table}>
                                    <TableHead className="multi-head">
                                        <TableRow className="tbl_head">
                                            <TableCell colSpan="1" className="xs-none">
                                                <Link className="header-pin">
                                                    <img alt="cricket" src={pin} />
                                                </Link>
                                                <span style={{ fontWeight: "bold", fontSize: "16px" }}>Indian Premier League </span>
                                                <span className="winner">Winner</span>
                                                <span className="refresh-icon"><ReplayIcon /></span>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tbl_head">
                                            <TableCell colSpan="1" className="md-none ipl-header">
                                                <Link className="pin-icon">
                                                    <img src={GreenPin} />
                                                </Link>
                                                <span className="ipl-header-text">Ireland v Zimbabwe</span>
                                            </TableCell>
                                            <TableCell style={{ maxWidth: '18px', borderRight: '1px solid #394f5c', textAlign: 'center', paddingBottom: '4px' }}>
                                                <ChevronRight className="right-arrow" />
                                            </TableCell>
                                            <TableCell style={{ maxWidth: '18px', textAlign: 'center', paddingBottom: '4px' }}>
                                                <ReplayIcon />
                                            </TableCell>
                                        </TableRow>
                                    </TableHead>
                                </Table>
                                <Table className={classes.table} style={{ background: '#FFFFFF' }} aria-label="simple table">
                                    {isMobile ?
                                        <TableHead>
                                            <TableRow className="ipl-tbl-head">
                                                <TableCell colSpan="1" className="ipl-xs-head px-0">
                                                    <div style={{ paddingLeft: '5px' }}>
                                                        <Link><img src={Depth} /></Link>
                                                        <div className="matched-match">
                                                            <span>Match Odds</span>
                                                        </div>
                                                    </div>
                                                </TableCell>
                                                <TableCell align="right" className="ipl-middle-head" colSpan="1">Back</TableCell>
                                                <TableCell align="right" className="ipl-middle-head" colSpan="1">Lay</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        :
                                        <TableHead>
                                            <TableRow className="ipl-tbl-head">
                                                <TableCell colSpan="1">
                                                    <span>8 selections </span>
                                                </TableCell>
                                                <TableCell align="left" colSpan="2">
                                                    <span>200.8%</span>
                                                </TableCell>
                                                <TableCell align="right" style={{ backgroundImage: `url(${BackballDisabled})` }} className="ipl-middle-head" colSpan="1">Back</TableCell>
                                                <TableCell align="right" className="ipl-middle-head" colSpan="1">Lay</TableCell>
                                                <TableCell align="right" colSpan="2">
                                                    <span>95.3%</span>
                                                </TableCell>
                                            </TableRow>
                                        </TableHead>
                                    }
                                    <TableBody className="ipl_tbl_body">
                                        <TableRowLink p={1} className="odd_even_clr" button>
                                            <TableCell className="table_first_row" colSpan="1">
                                                <div className="text_left-in-ipl">
                                                    {!isMobile && (
                                                        <img className="icon-predict" alt="chart" src={transparent} />
                                                    )}
                                                    Ireland
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-none" style={{ backgroundColor: '#D7E8F4' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>80</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-none" style={{ backgroundColor: '#B7D5EB' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>80</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-ipl-block" style={{ backgroundColor: '#72BBEF' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>1.71</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-ipl-block" style={{ backgroundColor: '#FAA9BA' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>1.98</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-none" style={{ backgroundColor: '#EFD3D9' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>90</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-none" style={{ backgroundColor: '#F6E6EA' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>85</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                        </TableRowLink>
                                        <TableRowLink p={1} className="odd_even_clr" button>
                                            <TableCell className="table_first_row" colSpan="1">
                                                <div className="text_left-in-ipl">
                                                    {!isMobile && (
                                                        <img className="icon-predict" alt="chart" src={transparent} />
                                                    )}
                                                    Zimbabwe
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-none" style={{ backgroundColor: '#D7E8F4' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>80</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-none" style={{ backgroundColor: '#B7D5EB' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>80</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-ipl-block" style={{ backgroundColor: '#72BBEF' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>1.71</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-ipl-block" style={{ backgroundColor: '#FAA9BA' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>1.98</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-none" style={{ backgroundColor: '#EFD3D9' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>90</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-none" style={{ backgroundColor: '#F6E6EA' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>85</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                        </TableRowLink>
                                    </TableBody>
                                </Table>
                            </div>
                            <div className="match-block">
                                <div className="match-block-header">
                                    <h5>Cricket</h5>
                                    <span><img src={Time} /> In-Play</span>
                                </div>
                                <Table className={classes.table}>
                                    <TableHead className="multi-head">
                                        <TableRow className="tbl_head">
                                            <TableCell colSpan="1" className="xs-none">
                                                <Link className="header-pin">
                                                    <img alt="cricket" src={pin} />
                                                </Link>
                                                <span style={{ fontWeight: "bold", fontSize: "16px" }}>Indian Premier League </span>
                                                <span className="winner">Winner</span>
                                                <span className="refresh-icon"><ReplayIcon /></span>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tbl_head">
                                            <TableCell colSpan="1" className="md-none ipl-header">
                                                <Link className="pin-icon">
                                                    <img src={GreenPin} />
                                                </Link>
                                                <span className="ipl-header-text">Ireland v Zimbabwe</span>
                                            </TableCell>
                                            <TableCell style={{ maxWidth: '18px', borderRight: '1px solid #394f5c', textAlign: 'center', paddingBottom: '4px' }}>
                                                <ChevronRight className="right-arrow" />
                                            </TableCell>
                                            <TableCell style={{ maxWidth: '18px', textAlign: 'center', paddingBottom: '4px' }}>
                                                <ReplayIcon />
                                            </TableCell>
                                        </TableRow>
                                    </TableHead>
                                </Table>
                                <Table className={classes.table} style={{ background: '#FFFFFF' }} aria-label="simple table">
                                    {isMobile ?
                                        <TableHead>
                                            <TableRow className="ipl-tbl-head">
                                                <TableCell colSpan="1" className="ipl-xs-head px-0">
                                                    <div style={{ paddingLeft: '5px' }}>
                                                        <Link><img src={Depth} /></Link>
                                                        <div className="matched-match">
                                                            <span>Match Odds</span>
                                                        </div>
                                                    </div>
                                                </TableCell>
                                                <TableCell align="right" className="ipl-middle-head" colSpan="1">Back</TableCell>
                                                <TableCell align="right" className="ipl-middle-head" colSpan="1">Lay</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        :
                                        <TableHead>
                                            <TableRow className="ipl-tbl-head">
                                                <TableCell colSpan="1">
                                                    <span>8 selections </span>
                                                </TableCell>
                                                <TableCell align="left" colSpan="2">
                                                    <span>200.8%</span>
                                                </TableCell>
                                                <TableCell align="right" style={{ backgroundImage: `url(${BackballDisabled})` }} className="ipl-middle-head" colSpan="1">Back</TableCell>
                                                <TableCell align="right" className="ipl-middle-head" colSpan="1">Lay</TableCell>
                                                <TableCell align="right" colSpan="2">
                                                    <span>95.3%</span>
                                                </TableCell>
                                            </TableRow>
                                        </TableHead>
                                    }
                                    <TableBody className="ipl_tbl_body">
                                        <TableRowLink p={1} className="odd_even_clr" button>
                                            <TableCell className="table_first_row" colSpan="1">
                                                <div className="text_left-in-ipl">
                                                    {!isMobile && (
                                                        <img className="icon-predict" alt="chart" src={transparent} />
                                                    )}
                                                    Ireland
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-none" style={{ backgroundColor: '#D7E8F4' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>80</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-none" style={{ backgroundColor: '#B7D5EB' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>80</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-ipl-block" style={{ backgroundColor: '#72BBEF' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>1.71</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-ipl-block" style={{ backgroundColor: '#FAA9BA' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>1.98</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-none" style={{ backgroundColor: '#EFD3D9' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>90</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-none" style={{ backgroundColor: '#F6E6EA' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>85</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                        </TableRowLink>
                                        <TableRowLink p={1} className="odd_even_clr" button>
                                            <TableCell className="table_first_row" colSpan="1">
                                                <div className="text_left-in-ipl">
                                                    {!isMobile && (
                                                        <img className="icon-predict" alt="chart" src={transparent} />
                                                    )}
                                                    Zimbabwe
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-none" style={{ backgroundColor: '#D7E8F4' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>80</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-none" style={{ backgroundColor: '#B7D5EB' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>80</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-ipl-block" style={{ backgroundColor: '#72BBEF' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>1.71</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-ipl-block" style={{ backgroundColor: '#FAA9BA' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>1.98</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-none" style={{ backgroundColor: '#EFD3D9' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>90</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                            <TableCell align="center" className="disabled_td xs-none" style={{ backgroundColor: '#F6E6EA' }}>
                                                <div style={{ display: 'block' }}>
                                                    <div>85</div>
                                                    <span>72</span>
                                                </div>
                                            </TableCell>
                                        </TableRowLink>
                                    </TableBody>
                                </Table>
                            </div>
                            <br />
                            <br />
                        </>
                        :
                        <>
                            <div className="md-none xs-multi-marketing">
                                <h3>There are currently no followed multi markets.</h3>
                                <p>Please add some markets from events.</p>
                            </div>
                            <div className="multi-marketing xs-none">
                                <h4>Multi Markets</h4>
                                <p>There are currently no followed multi markets.</p>
                            </div>
                        </>
                    }
                </Grid>
                <Grid lg={3} xs={12} className="xs-none">
                    <BetSlip />
                </Grid>
            </Grid>
        </>
    )
}

export default MultiMarket