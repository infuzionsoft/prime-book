import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { List, ListItemText, ListItem, Tabs, Button } from '@material-ui/core';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Grid, AppBar, InputLabel } from '@material-ui/core';
import moment from 'moment';
import Appconfig from "../config/config";
import Footer from "../includes/Footer";
import axios from "axios";

import { isMobile } from "react-device-detect";

import Header from '../includes/Header';
import AccountSidebar from '../includes/AccountSidebar';

const gridType = isMobile ? 10 : 8;
export default function UnsetteledBet() {
    return (
        <>
       
         {isMobile ? 
            <>
            <meta name="viewport" content="width=1300" />
            <Header /> 
            <Grid className="account-stat" container spacing={0}>
                <Grid item lg={gridType} xs={12} spacing={2}>
                    {/* <div className="helloaccount">
                        Hello
                    </div> */}
			<div class="report-container">
	 
	<div class="card" style={{border: "none"}}>
		<div class="card-header">
			<h4 class="mb-0">Un-Setteled Bet</h4></div>
		<div class="card-body container-fluid container-fluid-5 unsetteledbet">
			<div class="row row5">
				<div class="col-12 text-center">
					<div id="match_unmatched_delete" role="radiogroup" tabindex="-1" >
						<div class="custom-control custom-control-inline custom-radio">
							<input id="matched" type="radio" name="match_unmatched_delete" autocomplete="off" value="1" class="custom-control-input"  />
							<label for="matched" class="custom-control-label" style={{color: "#000"}}><span>Matched</span></label>
						</div>
						<div class="custom-control custom-control-inline custom-radio">
							<input id="unmatched" type="radio" name="match_unmatched_delete" autocomplete="off" value="2" class="custom-control-input" />
							<label for="unmatched" class="custom-control-label" style={{color: "#000"}}><span>Un-Matched</span></label>
						</div>
						<div class="custom-control custom-control-inline custom-radio">
							<input id="deleteed" type="radio" name="match_unmatched_delete" autocomplete="off" value="3" class="custom-control-input" />
							<label for="deleteed" class="custom-control-label" style={{color: "#000"}}><span>Deleted</span></label>
						</div>
					</div>
				</div>
			</div>
			<div class="row row5 mt-2">
				<div class="col-12">
					<div class="row row5">
						<div class="col-12">
							<p class="mb-0 text-center">There are no records to show</p>
						</div>
					</div>
				</div>
			</div>
			 
		</div>
	</div>
</div>

                   
                </Grid>
            </Grid>
            </>
            : 
            <>
            <Header /> 
           <div class="row row5">
                <div className="sidebar-whold-cont">
                    <AccountSidebar />
                </div> 
                <div class="col-md-10 report-main-content m-t-5">
	<div class="card">
		<div class="card-header-desktop">
			<h4 class="mb-0">Un-Setteled Bet</h4></div>
		<div class="card-body container-fluid container-fluid-5 unsetteledbet">
			<div class="row row5">
				<div class="col-12">
					<div id="match_unmatched_delete" role="radiogroup" tabindex="-1">
						<div class="custom-control custom-control-inline custom-radio">
							<input id="matched" type="radio" name="match_unmatched_delete" autocomplete="off" value="1" class="custom-control-input"/>
							<label for="matched" class="custom-control-label"><span>Matched</span></label>
						</div>
						<div class="custom-control custom-control-inline custom-radio">
							<input id="unmatched" type="radio" name="match_unmatched_delete" autocomplete="off" value="2" class="custom-control-input"/>
							<label for="unmatched" class="custom-control-label"><span>Un-Matched</span></label>
						</div>
						<div class="custom-control custom-control-inline custom-radio">
							<input id="deleteed" type="radio" name="match_unmatched_delete" autocomplete="off" value="3" class="custom-control-input"/>
							<label for="deleteed" class="custom-control-label"><span>Deleted</span></label>
						</div>
					</div>
				</div>
			</div>
			 
			<div class="row row5 mt-2">
				<div class="col-12">
					<div class="table-responsive">
						<table role="table" aria-busy="false" aria-colcount="10" class="table b-table table-bordered" id="__BVID__59">
							 
							 
							<thead role="rowgroup" class="">
								 
								<tr role="row" class="">
									<th role="columnheader" scope="col" aria-colindex="1" class="text-right">No</th>
									<th role="columnheader" scope="col" aria-colindex="2" class="text-center">Event Name</th>
									<th role="columnheader" scope="col" aria-colindex="3" class="text-center">Nation</th>
									<th role="columnheader" scope="col" aria-colindex="4" class="text-center">Event Type</th>
									<th role="columnheader" scope="col" aria-colindex="5" class="text-center">Market Name</th>
									<th role="columnheader" scope="col" aria-colindex="6" class="text-center">Side</th>
									<th role="columnheader" scope="col" aria-colindex="7" class="text-center">Rate</th>
									<th role="columnheader" scope="col" aria-colindex="8" class="text-right">Amount</th>
									<th role="columnheader" scope="col" aria-colindex="9" class="text-center">Place Date</th>
									<th role="columnheader" scope="col" aria-colindex="10" class="">Match Date</th>
								</tr>
							</thead>
							<tbody role="rowgroup"></tbody>
							 
						</table>
					</div>
				</div>
			</div>
			<div class="row row5 mt-2">
				<div class="col-12">
					 
				</div>
			</div>
		</div>
	</div>
</div>
            </div>
            <Footer />    
            </>}
        </>

    )
}
