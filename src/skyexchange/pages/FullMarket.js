import '../assets/css/style.css';
import React, { useContext } from 'react';
import Header from '../includes/Header';
import BetSlip from '../includes/BetSlip';
import LeftSideBar from '../includes/LeftSideBar';
import { ToastContainer, toast } from "react-toastify";
import { SocketContext } from "../../context/socket";
import { Grid, makeStyles, Table, TableRow, TableCell, TableHead, TableBody, Link, Box, Typography, Accordion, AccordionSummary, AccordionDetails, FormControlLabel, Checkbox, Button, AppBar, Tabs, Tab } from '@material-ui/core';
import PropTypes from 'prop-types';

//IMAGES
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Replay from '@material-ui/icons/Replay';
import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';

import background from '../assets/images/bg-purple.png';
import KV1 from '../assets/images/KV2.png';
import pin from '../assets/images/pin.png';
import Greypin from '../assets/images/grey-pin-off.svg';
import DarkPin from '../assets/images/dark-pin-off.svg';
import transparent from '../assets/images/transparent.gif';
import bookmarkwithtime from '../assets/images/bookmarkwithtime.png';
import MinMax from '../assets/images/min-max.svg';
import Depth from '../assets/images/depth.svg';
// import Time from '../assets/images/time.png';
import Time from '../assets/images/clock-green.png';
import CircularProgress from '@material-ui/core/CircularProgress';

import BackballDisabled from '../assets/images/bg-backall-disabled.png';
import SimplePin from '../assets/images/simple-pin.svg';
import BtnDelete from '../assets/images/btn-delete.svg';

import { useParams, useHistory } from "react-router-dom";
import { MobileView, BrowserView, isMobile } from 'react-device-detect';
import Appconfig from "../config/config"
import axios from 'axios';
import Loader from '../assets/images/loading40.gif';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { faMinus } from '@fortawesome/free-solid-svg-icons'


function createData(name1, value, name2, play, link, calories, fat, carbs, protein) {
    return { name1, value, name2, play, link, calories, fat, carbs, protein };
}
function TableRowLink(props) {
    return <TableRow button component="Link" {...props} />;
}
function loader_default() {
    document.getElementById("poker_loading").style.display = "block";
}
function loader_remove() {
    document.getElementById("poker_loading").style.display = "none";
}
function hideModal() {
    document.getElementById("__BVID__272___BV_modal_outer_").style.display = "none";
}
function showModal() {
    
    document.getElementById("__BVID__272___BV_modal_outer_").style.display = "block";
   
}
function showInfoModal(evt, Name) {
    document.getElementById(Name).style.display = "block";
}
function hideInfoModal(evt, Name) {
    document.getElementById(Name).style.display = "none";
}

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        // width: 500,
        height: "100%",
    },
    tableHeight: {
        height: 'calc(100vh - 120px)',
    },
}));
const FullMarket = () => {
    const history = useHistory();
    const socket = useContext(SocketContext);
    const isLoggedIn = window.sessionStorage.getItem("loggedIn") && window.sessionStorage.getItem("loggedIn") != "false" ? true : false;
    const userInfo = JSON.parse(window.sessionStorage.getItem("userData"));
    const params = useParams();

    const eventId = params.event_id;
    const classes = useStyles();
    const [state, setState] = React.useState({
        checkedB: false,
    });

    const [isBetPlaced, setIsBetPlaced] = React.useState(false);

    const [betShow, setBetShow] = React.useState(false);
    const [EventData, setEventData] = React.useState([]);
    const [StakeValue, setStakeValue] = React.useState(0);
    const [betSlipList, setBetSlipList] = React.useState([]);
    const [placing, setPlacing] = React.useState(false);
    const [lossValue, setLossValue] = React.useState(0);

    const [BetPlaceData, setBetPlaceData] = React.useState({
        event_id: "",
        market_id: "",
        is_back: "",
        price: "",
        is_fancy: "",
        selection_id: "",
        runner_name: "",
    });
    const [ProfitValue, setProfitValue] = React.useState(0);

    const placeStakeValue = (stake) => {
        stake = parseFloat(stake);
        setStakeValue(stake);
        let profit = parseFloat(BetPlaceData.price) * parseFloat(stake) - stake;
        setProfitValue(profit);
    };

    const handleBetInputChange = (e) => {
        setStakeValue(e.target.value);
    }
    const rows = [
        createData('Daring king', '0 - 1 ', 'maxime cressy', 'In play', 'link', 237, 9.0, 37, 4.3),
        createData('Daring king', '1 - 1 ', 'maxime', 'In play', 'link', 237, 9.0, 37, 4.3),
        createData('king', '3 - 1 ', 'cressy', 'In play', 'link', 237, 9.0, 37, 4.3),
        createData('Daring', '2 - 1 ', 'maxime cressy', 'In play', 'link', 237, 9.0, 37, 4.3),
        createData('Forge', '0 - 1 ', 'maxime cressy', 'In play', 'link', 237, 9.0, 37, 4.3),
    ];

    const [tvUrl, setTvUrl] = React.useState("");
    const [scoreUrl, setScoreUrl] = React.useState("");
    const [iframeHeight, setScoreIframeHeight] = React.useState("");
    const [chipList, setchipList] = React.useState([]);


    React.useEffect(() => {
        getChips();
        window.addEventListener("message", (event) => {
            if (event.origin == "https://central.satsport247.com'")
                console.log("event new", event.data.scoreWidgetHeight)
            setScoreIframeHeight(event.data.scoreWidgetHeight)
        }, false);

        // console.log("profit,loss", [ProfitValue, lossValue])
        socket.on('fancy_update', function (data) {

            // console.log('FANCY UPDATE:', data);
            // console.log(data);
            fancyHtml(data);
        });


        socket.on("market_update", (data) => {

            // return false;
            //events   
            // console.log('MARKET UPDATE:', data);

            var matchId = eventId;
            // if (MarketId) {

            // var market = data.marketodds[matchId]

            if (data.marketodds.length > 0) {

                var market = data.marketodds.find(o => o.event_id == matchId);

                if (market) {
                    if (market.market_types.length > 0) {
                        market.market_types.map((market_type, index) => {


                            market_type.runners.map((runner, index) => {



                                if (market_type.status == 'OPEN') {
                                    // document.getElementsByClassName('overlay_matchBoxs_' + market_type.market_id.replace('.', '')).fadeOut();
                                } else {

                                    // document.getElementsByClassName('overlay_matchBoxs_' + market_type.market_id.replace('.', '')).fadeIn();

                                    // document.getElementsByClassName('status_matchBoxs_' + market_type.market_id.replace('.', '')).text(market_type.status);
                                }
                                // console.log('table_row_' + runner.market_id.replace('.', '') + '_' + runner.selection_id[0]);
                                var match_odd_row = document.getElementsByClassName('table_row_' + runner.market_id.replace('.', '') + '_' + runner.selection_id)[0];

                                if (runner.status == 'SUSPENDED') {



                                    if (document.getElementById('availableToLay1_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id)) {
                                        match_odd_row.setAttribute('data-title', "SUSPENDED");
                                        match_odd_row.classList.add("suspended");
                                        if (parseFloat(document.getElementById('availableToBack1_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML) != parseFloat(runner.back_1_price)) {
                                            document.getElementById('availableToBack1_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML = 0;

                                        } else {
                                            document.getElementById('availableToBack1_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML = 0;
                                        }


                                        if (parseFloat(document.getElementById('availableToLay1_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML) != parseFloat(runner.lay_1_price)) {
                                            document.getElementById('availableToLay1_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML = 0;

                                        } else {
                                            document.getElementById('availableToLay1_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML = 0;
                                        }

                                    }

                                } else {

                                    if (document.getElementById('availableToLay1_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id)) {
                                        match_odd_row.setAttribute('data-title', "");
                                        match_odd_row.classList.remove("suspended");

                                        if (parseFloat(document.getElementById('availableToBack1_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML) != parseFloat(runner.back_1_price)) {

                                            document.getElementById('availableToBack1_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML = parseFloat(runner.back_1_price);

                                        } else {
                                            document.getElementById('availableToBack1_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML = parseFloat(runner.back_1_price);
                                        }


                                        if (parseFloat(document.getElementById('availableToLay1_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML) != parseFloat(runner.lay_1_price)) {
                                            document.getElementById('availableToLay1_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML = parseFloat(runner.lay_1_price);

                                        } else {
                                            document.getElementById('availableToLay1_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML = parseFloat(runner.lay_1_price);
                                        }
                                    }
                                    // if (parseFloat(document.getElementById('availableToLay2_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML) != parseFloat(runner.lay_2_price)) {
                                    //     document.getElementById('availableToLay2_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML=runner.lay_2_price;

                                    // } else {
                                    //     document.getElementById('availableToLay2_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML=runner.lay_2_price;
                                    // }

                                    // if (parseFloat(document.getElementById('availableToLay3_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML) != parseFloat(runner.lay_3_price)) {
                                    //     document.getElementById('availableToLay3_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML=runner.lay_3_price;

                                    // } else {
                                    //     document.getElementById('availableToLay3_price_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML=runner.lay_3_price;
                                    // }

                                }


                                /************************Size */

                                // if (parseFloat(document.getElementById('availableToBack3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML) != parseFloat(runner.back_3_size)) {
                                //     document.getElementById('availableToBack3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML=runner.back_3_size;document.getElementById('availableToLay3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).parentElement.classList.add('yellow');

                                // } else {
                                //     document.getElementById('availableToBack3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML=runner.back_3_size;document.getElementById('availableToLay3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).parentElement.classList.remove('yellow');
                                // }


                                // if (parseFloat(document.getElementById('availableToBack2_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML) != parseFloat(runner.back_2_size)) {
                                //     document.getElementById('availableToBack2_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML=runner.back_2_size;document.getElementById('availableToLay3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).parentElement.classList.add('yellow');

                                // } else {
                                //     document.getElementById('availableToBack2_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML=runner.back_2_size;document.getElementById('availableToLay3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).parentElement.classList.remove('yellow');
                                // }
                                if (document.getElementById('availableToBack1_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id)) {
                                    if (parseFloat(document.getElementById('availableToBack1_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML) != parseFloat(runner.back_1_size)) {
                                        document.getElementById('availableToBack1_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML = parseFloat(runner.back_1_size);

                                        // document.getElementById('availableToLay3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).parentElement.classList.add('yellow');

                                    } else {

                                        document.getElementById('availableToBack1_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML = parseFloat(runner.back_1_size);
                                        //  document.getElementById('availableToLay3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).parentElement.classList.remove('yellow');
                                    }


                                    if (parseFloat(document.getElementById('availableToLay1_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML) != parseFloat(runner.lay_1_size)) {
                                        document.getElementById('availableToLay1_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML = parseFloat(runner.lay_1_size);

                                        // document.getElementById('availableToLay3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).parentElement.classList.add('yellow');

                                    } else {

                                        document.getElementById('availableToLay1_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML = parseFloat(runner.lay_1_size);
                                        // document.getElementById('availableToLay3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).parentElement.classList.remove('yellow');
                                    }
                                }
                                // if (parseFloat(document.getElementById('availableToLay2_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML) != parseFloat(runner.lay_2_size)) {
                                //     document.getElementById('availableToLay2_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML=runner.lay_2_size;document.getElementById('availableToLay3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).parentElement.classList.add('yellow');

                                // } else {
                                //     document.getElementById('availableToLay2_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML=runner.lay_2_size;document.getElementById('availableToLay3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).parentElement.classList.remove('yellow');
                                // }

                                // if (parseFloat(document.getElementById('availableToLay3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML) != parseFloat(runner.lay_3_size)) {
                                //     document.getElementById('availableToLay3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML=runner.lay_3_size;document.getElementById('availableToLay3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).parentElement.classList.add('yellow');

                                // } else {
                                //     document.getElementById('availableToLay3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).innerHTML=runner.lay_3_size;document.getElementById('availableToLay3_size_' + runner.market_id.replace('.', '') + '_' + runner.selection_id).parentElement.classList.remove('yellow');
                                // }


                            });
                        });
                    }
                }

            }
        });
        getTvUrl(eventId)
        getScoreUrl(eventId)
        getEventData()
    }, [eventId])




    function fancyHtml(data) {
        // console.log("kaka", data);
        var superiors = ["270", "259", "185", "177", "30"];
        var matchId = eventId;
        if (document.getElementById('fancyAPI')) {
            // document.getElementById('fancyAPI').innerHTML = `<tr class="MuiTableRow-root yelloBG"><td class="MuiTableCell-root MuiTableCell-body table_first_row colorDanger"></td><td class="MuiTableCell-root MuiTableCell-body xs_none MuiTableCell-alignRight"></td><td class="MuiTableCell-root MuiTableCell-body xs_none MuiTableCell-alignRight"></td><td class="MuiTableCell-root MuiTableCell-body xs_none MuiTableCell-alignRight"></td><td class="MuiTableCell-root MuiTableCell-body xs_none MuiTableCell-alignRight"></td><td class="MuiTableCell-root MuiTableCell-body bg_odd MuiTableCell-alignRight">Back</td><td class="MuiTableCell-root MuiTableCell-body bg_even MuiTableCell-alignRight">Lay</td></tr>`;
        }
        if (matchId) {

            if (data.fantacy.length > 0) {


                var fancys = data.fantacy.find(o => o.event_id == matchId);

                if (fancys) {

                    fancys = fancys.fancy_data;


                    if (fancys.length) {

                        for (var j = 0; j < fancys.length; j++) {


                            if (fancys[j].runner_name.includes('ball')) {
                                continue;

                            }
                            if (fancys[j].cron_disable == 'Yes') {
                                // ClearAllSelection(1);
                                // $('.fancy_lay_price_' + fancys[j].selection_id).parent().parent().fadeOut();
                            } else {
                                if (fancys[j].is_active == "No") {
                                    document.getElementsByClassName('fancy_' + fancys[j].selection_id).remove();
                                }
                                if (fancys[j]) {
                                    // console.log("fancys[j]", fancys[j])
                                    var block_market_fancys = fancys[j].block_market;
                                    var block_all_market_fancys = fancys[j].block_all_market;
                                    // var block_market_fancys = [];
                                    // var block_all_market_fancys = [];

                                    var find_fancy_all_block = block_all_market_fancys.filter(element => {

                                        return superiors.includes(element.user_id.toString())
                                    });

                                    if (find_fancy_all_block.length > 0) {
                                        // ClearAllSelection(1);
                                        // $('.fancy_lay_price_' + fancys[j].selection_id).parent().parent().fadeOut();
                                    } else {

                                        var find_fancy_block = block_market_fancys.filter(element => {

                                            return superiors.includes(element.user_id.toString())
                                        });

                                        if (find_fancy_block.length > 0) {
                                            // ClearAllSelection(1);
                                            // $('.fancy_lay_price_' + fancys[j].selection_id).parent().parent().fadeOut();
                                        } else {
                                            // $('.fancy_lay_price_' + fancys[j].selection_id).parent().parent().fadeIn();
                                            var fancyHtml = '';
                                            // console.log("reee", 'fancy_' + fancys[j].selection_id);
                                            if (!document.getElementsByClassName('fancy_' + fancys[j].selection_id)) {
                                                // console.log("nhi hai")
                                                // fancyHtml += '<div class="block_box f_m_' + fancys[j].match_id + ' fancy_' + fancys[j].selection_id + ' f_m_31236" data-id="31236">';

                                                // fancyHtml += '<ul class="sport-high fancyListDiv">';
                                                // fancyHtml += '<li>';
                                                // fancyHtml += '<div class="ses-fan-box">';
                                                // fancyHtml += '<table class="table table-striped  bulk_actions">';
                                                // fancyHtml += '<tbody>';








                                                fancyHtml += `<tr class="MuiTableRow-root fancy_${fancys[j].selection_id}" >`;
                                                fancyHtml += `<td class="MuiTableCell-root MuiTableCell-body table_first_row">`;
                                                fancyHtml += `<div class="text_left">
                            <button onclick=getFancyPosition(${fancys[j].selection_id}) className="book-fancy">Book</button>
                            <p >${fancys[j].runner_name}</p><p>0</p></div>`;
                                                fancyHtml += `</td>`;
                                                fancyHtml += `<td class="MuiTableCell-root MuiTableCell-body xs_none MuiTableCell-alignRight"></td>`;

                                                fancyHtml += `<td class="MuiTableCell-root MuiTableCell-body xs_none MuiTableCell-alignRight"></td>`;
                                                fancyHtml += `<td class="MuiTableCell-root MuiTableCell-body xs_none MuiTableCell-alignRight"></td>`;
                                                fancyHtml += `<td class="MuiTableCell-root MuiTableCell-body xs_none MuiTableCell-alignRight"></td>`;

                                                fancyHtml += `<td class="MuiTableCell-root MuiTableCell-body bg_even MuiTableCell-alignRight" 
                            onclick=
                        handleOpen(
                          ${eventId},
                          "",
                          false,
                          ${fancys[j].lay_price1},
                          true,
                          ${fancys[j].selection_id},
                          ${fancys[j].runner_name},
                          fancy_lay_price_${fancys[j].selection_id}
                        )>`;
                                                fancyHtml += `<div class="fancy-td-cell"><div class="fancy_lay_price_${fancys[j].selection_id}" id="fancy_lay_price_${fancys[j].selection_id}">${parseFloat(fancys[j].lay_price1)}</div><p class="eventP fancy_lay_size_${fancys[j].selection_id}" id="fancy_lay_size_${fancys[j].selection_id}">${parseFloat(fancys[j].lay_size1)}</p></div>`;
                                                fancyHtml += `</td >`;
                                                fancyHtml += `<td class="MuiTableCell-root MuiTableCell-body bg_odd sus-parent MuiTableCell-alignRight" 
                            onclick=
                        handleOpen(
                          ${eventId},
                          "",
                          true,
                          ${fancys[j].back_price1},
                          true,
                          ${fancys[j].selection_id},
                          ${fancys[j].runner_name},
                          fancy_lay_price_${fancys[j].selection_id}
                        )>`;
                                                fancyHtml += `<div class="fancy-td-cell"><div class="fancy_back_price_${fancys[j].selection_id}" id="fancy_back_price_${fancys[j].selection_id}">${parseFloat(fancys[j].back_price1)} </div> <p class="eventP fancy_back_size_${fancys[j].selection_id}" id="fancy_back_size_${fancys[j].selection_id}">${parseFloat(fancys[j].back_size1)}</p></div></td>`;
                                                fancyHtml += `</tr >`;



                                                // fancyHtml += '<tr class="MuiTableRow-root">';
                                                // fancyHtml += '<td class="MuiTableCell-root MuiTableCell-body table_first_row">';
                                                // fancyHtml += '<div  class="fancyhead' + fancys[j].selection_id + '" id="fancy_name' + fancys[j].selection_id + '">' + fancys[j].runner_name + '</div><b class="fancyLia' + fancys[j].selection_id + '"></b><p class="position_btn"><button class="btn btn-xs btn-danger" onclick="getPosition(' + fancys[j].selection_id + ')">Book</button></td>';


                                                // fancyHtml += '<td></td>';
                                                // fancyHtml += '<td></td>';

                                                // fancyHtml += '<td class="fancy_lay" id="fancy_lay_' + fancys[j].selection_id + '" onclick="betfancy(`' + fancys[j].match_id + '`,`' + fancys[j].selection_id + '`,`' + 0 + '`);">';

                                                // fancyHtml += '<button class="lay-cell cell-btn fancy_lay_price_' + fancys[j].selection_id + '" id="LayNO_' + fancys[j].selection_id + '">' + fancys[j].lay_price1 + '</button>';

                                                // fancyHtml += '<button id="NoValume_' + fancys[j].selection_id + '" class="disab-btn fancy_lay_size_' + fancys[j].selection_id + '">' + fancys[j].lay_size1 + '</button></td>';

                                                // fancyHtml += '<td class="fancy_back" onclick="betfancy(`' + fancys[j].match_id + '`,`' + fancys[j].selection_id + '`,`' + 1 + '`);">';

                                                // fancyHtml += '<button class="back-cell cell-btn fancy_back_price_' + fancys[j].selection_id + '" id="BackYes_' + fancys[j].selection_id + '">' + fancys[j].back_price1 + '</button>';

                                                // fancyHtml += '<button id="YesValume_' + fancys[j].selection_id + '" class="disab-btn fancy_back_size_' + fancys[j].selection_id + '">' + fancys[j].back_size1 + '</button>';
                                                // fancyHtml += '</td>';

                                                // fancyHtml += '<td>';
                                                // fancyHtml += '</td>';
                                                // fancyHtml += '<td>';
                                                // fancyHtml += '</td>';
                                                // fancyHtml += '</tr>';
                                                // fancyHtml += '</tbody>';
                                                // fancyHtml += '</table>';
                                                // fancyHtml += '</div>';
                                                // fancyHtml += '</li>';
                                                // fancyHtml += '</ul></div>';
                                                // console.log('fancy html', fancyHtml);
                                                if (document.getElementById('fancyAPI')) {
                                                    document.getElementById('fancyAPI').innerHTML += fancyHtml;
                                                }

                                            }

                                            // if (fancys[j].back_price1 == 'Ball') {
                                            //     $('.fancy_lay_price_' + fancys[j].selection_id).parent().attr('disabled', 'disabled');
                                            //     $('.fancy_back_price_' + fancys[j].selection_id).parent().attr('disabled', 'disabled');
                                            //     $('.fancy_lay_price_' + fancys[j].selection_id).text(fancys[j].lay_price1);
                                            //     $('.fancy_back_price_' + fancys[j].selection_id).text(fancys[j].back_price1);
                                            //     $('.fancy_lay_size_' + fancys[j].selection_id).text(fancys[j].lay_size1);
                                            //     $('.fancy_back_size_' + fancys[j].selection_id).text(fancys[j].back_size1);
                                            // }
                                            var newtr = document.getElementsByClassName('fancy_' + fancys[j].selection_id)[0];
                                            if (document.getElementById('fancy_lay_price_' + fancys[j].selection_id)) {
                                                if (fancys[j].game_status == 'Ball Running') {
                                                    // console.log("FDf", document.getElementsByClassName('fancy_' + fancys[j].selection_id));
                                                    // $('.fancy_lay_price_' + fancys[j].selection_id).parent().attr('disabled', 'disabled');
                                                    // $('.fancy_back_price_' + fancys[j].selection_id).parent().attr('disabled', 'disabled');
                                                    newtr.setAttribute('data-title', "BALL RUNNING");
                                                    newtr.classList.add("suspended");

                                                    document.getElementById('fancy_lay_price_' + fancys[j].selection_id).innerHTML = "-";
                                                    document.getElementById('fancy_back_price_' + fancys[j].selection_id).innerHTML = '-';
                                                    // document.getElementById('fancy_lay_size_' + fancys[j].selection_id).innerHTML = 'Ball Running';
                                                    //  document.getElementById('fancy_back_size_' + fancys[j].selection_id).innerHTML = 'Ball Running';

                                                } else if (fancys[j].game_status == 'SUSPENDED') {
                                                    newtr.setAttribute('data-title', "SUSPENDED");
                                                    newtr.classList.add("suspended");
                                                    document.getElementById('fancy_lay_price_' + fancys[j].selection_id).innerHTML = "-";
                                                    document.getElementById('fancy_back_price_' + fancys[j].selection_id).innerHTML = '-';
                                                    document.getElementById('fancy_lay_size_' + fancys[j].selection_id).innerHTML = '0';
                                                    document.getElementById('fancy_back_size_' + fancys[j].selection_id).innerHTML = '0';

                                                } else if (fancys[j].back_price1 == 0) {
                                                    newtr.classList.remove("suspended");
                                                    document.getElementById('fancy_lay_price_' + fancys[j].selection_id).innerHTML = '-';
                                                    document.getElementById('fancy_back_price_' + fancys[j].selection_id).innerHTML = '-';
                                                    document.getElementById('fancy_lay_size_' + fancys[j].selection_id).innerHTML = '0';
                                                    document.getElementById('fancy_back_size_' + fancys[j].selection_id).innerHTML = '0';

                                                } else {
                                                    newtr.classList.remove("suspended");
                                                    // console.log('fancy_lay_price_',document.getElementsByClassName('fancy_lay_price_' + fancys[j].selection_id).innerHTML);
                                                    document.getElementById('fancy_lay_price_' + fancys[j].selection_id).innerHTML = parseFloat(fancys[j].lay_price1);
                                                    document.getElementById('fancy_back_price_' + fancys[j].selection_id).innerHTML = parseFloat(fancys[j].back_price1);
                                                    document.getElementById('fancy_lay_size_' + fancys[j].selection_id).innerHTML = parseFloat(fancys[j].lay_size1);
                                                    document.getElementById('fancy_back_size_' + fancys[j].selection_id).innerHTML = parseFloat(fancys[j].back_size1);

                                                }
                                            }
                                        }
                                    }

                                } else {

                                    var fancyHtml = '';

                                    if (!document.getElementsByClassName('fancy_' + fancys[j].selection_id)) {
                                        // fancyHtml += '<div class="block_box f_m_' + fancys[j].match_id + ' fancy_' + fancys[j].selection_id + ' f_m_31236" data-id="31236">';

                                        // fancyHtml += '<ul class="sport-high fancyListDiv">';
                                        // fancyHtml += '<li>';
                                        // fancyHtml += '<div class="ses-fan-box">';
                                        // fancyHtml += '<table class="table table-striped  bulk_actions">';
                                        // fancyHtml += '<tbody>';








                                        fancyHtml += `<tr class="MuiTableRow-root fancy_${fancys[j].selection_id}">`;
                                        fancyHtml += `<td class="MuiTableCell-root MuiTableCell-body table_first_row">`;
                                        fancyHtml += `<div class="text_left">
                        <button onclick=getFancyPosition(${fancys[j].selection_id}) className="book-fancy">Book</button>
                        <p >${fancys[j].runner_name}</p><p>0</p></div>`;
                                        fancyHtml += `</td>`;
                                        fancyHtml += `<td class="MuiTableCell-root MuiTableCell-body xs_none MuiTableCell-alignRight"></td>`;

                                        fancyHtml += `<td class="MuiTableCell-root MuiTableCell-body xs_none MuiTableCell-alignRight"></td>`;
                                        fancyHtml += `<td class="MuiTableCell-root MuiTableCell-body xs_none MuiTableCell-alignRight"></td>`;
                                        fancyHtml += `<td class="MuiTableCell-root MuiTableCell-body xs_none MuiTableCell-alignRight"></td>`;

                                        fancyHtml += `<td class="MuiTableCell-root MuiTableCell-body bg_even MuiTableCell-alignRight" onclick=
                        handleOpen(
                          ${eventId},
                          "",
                          false,
                          ${fancys[j].lay_price1},
                          true,
                          ${fancys[j].selection_id},
                          ${fancys[j].runner_name},
                          fancy_lay_price_${fancys[j].selection_id}
                        )>`;
                                        fancyHtml += `<div class="fancy-td-cell"><div class="fancy_lay_price_${fancys[j].selection_id}" id="fancy_lay_price_${fancys[j].selection_id}">${parseFloat(fancys[j].lay_price1)}</div><p class="eventP fancy_lay_size_${fancys[j].selection_id}" id="fancy_lay_size_${fancys[j].selection_id}">${parseFloat(fancys[j].lay_size1)}</p></div>`;
                                        fancyHtml += `</td >`;
                                        fancyHtml += `<td class="MuiTableCell-root MuiTableCell-body bg_odd sus-parent MuiTableCell-alignRight" onclick=
                        handleOpen(
                          ${eventId},
                          "",
                          true,
                          ${fancys[j].back_price1},
                          true,
                          ${fancys[j].selection_id},
                          ${fancys[j].runner_name},
                          fancy_lay_price_${fancys[j].selection_id}
                        )>`;
                                        fancyHtml += `<div class="fancy-td-cell"><div class="fancy_back_price_${fancys[j].selection_id}" id="fancy_back_price_${fancys[j].selection_id}" >${parseFloat(fancys[j].back_price1)} </div> <p class="eventP fancy_back_size_${fancys[j].selection_id}" id="fancy_back_size_${fancys[j].selection_id}">${parseFloat(fancys[j].back_size1)}</p></div></td>`;

                                        fancyHtml += `</tr >`;



                                        // fancyHtml += '<tr class="MuiTableRow-root">';
                                        // fancyHtml += '<td class="MuiTableCell-root MuiTableCell-body table_first_row">';
                                        // fancyHtml += '<div  class="fancyhead' + fancys[j].selection_id + '" id="fancy_name' + fancys[j].selection_id + '">' + fancys[j].runner_name + '</div><b class="fancyLia' + fancys[j].selection_id + '"></b><p class="position_btn"><button class="btn btn-xs btn-danger" onclick="getPosition(' + fancys[j].selection_id + ')">Book</button></td>';


                                        // fancyHtml += '<td></td>';
                                        // fancyHtml += '<td></td>';

                                        // fancyHtml += '<td class="fancy_lay" id="fancy_lay_' + fancys[j].selection_id + '" onclick="betfancy(`' + fancys[j].match_id + '`,`' + fancys[j].selection_id + '`,`' + 0 + '`);">';

                                        // fancyHtml += '<button class="lay-cell cell-btn fancy_lay_price_' + fancys[j].selection_id + '" id="LayNO_' + fancys[j].selection_id + '">' + fancys[j].lay_price1 + '</button>';

                                        // fancyHtml += '<button id="NoValume_' + fancys[j].selection_id + '" class="disab-btn fancy_lay_size_' + fancys[j].selection_id + '">' + fancys[j].lay_size1 + '</button></td>';

                                        // fancyHtml += '<td class="fancy_back" onclick="betfancy(`' + fancys[j].match_id + '`,`' + fancys[j].selection_id + '`,`' + 1 + '`);">';

                                        // fancyHtml += '<button class="back-cell cell-btn fancy_back_price_' + fancys[j].selection_id + '" id="BackYes_' + fancys[j].selection_id + '">' + fancys[j].back_price1 + '</button>';

                                        // fancyHtml += '<button id="YesValume_' + fancys[j].selection_id + '" class="disab-btn fancy_back_size_' + fancys[j].selection_id + '">' + fancys[j].back_size1 + '</button>';
                                        // fancyHtml += '</td>';

                                        // fancyHtml += '<td>';
                                        // fancyHtml += '</td>';
                                        // fancyHtml += '<td>';
                                        // fancyHtml += '</td>';
                                        // fancyHtml += '</tr>';
                                        // fancyHtml += '</tbody>';
                                        // fancyHtml += '</table>';
                                        // fancyHtml += '</div>';
                                        // fancyHtml += '</li>';
                                        // fancyHtml += '</ul></div>';
                                        // console.log('fancy html', fancyHtml);
                                        if (document.getElementById('fancyAPI')) {
                                            document.getElementById('fancyAPI').innerHTML += fancyHtml;
                                        }
                                    }

                                    if (fancys[j].game_status == 'Ball Running') {
                                        // console.log('getfdsaf', document.getElementsByClassName('fancy_lay_price_' + fancys[j].selection_id));
                                        // $('.fancy_lay_price_' + fancys[j].selection_id).parent().attr('disabled', 'disabled');
                                        // $('.fancy_back_price_' + fancys[j].selection_id).parent().attr('disabled', 'disabled');
                                        document.getElementById('fancy_' + fancys[j].selection_id).setAttribute('data-title', "BALL RUNNING");
                                        document.getElementById('fancy_' + fancys[j].selection_id).classList.add("suspended");

                                        document.getElementsByClassName('fancy_lay_price_' + fancys[j].selection_id).innerHTML = "-";
                                        document.getElementsByClassName('fancy_back_price_' + fancys[j].selection_id).innerHTML = '-';
                                        document.getElementsByClassName('fancy_lay_size_' + fancys[j].selection_id).innerHTML = 'Ball Running';
                                        document.getElementsByClassName('fancy_back_size_' + fancys[j].selection_id).innerHTML = 'Ball Running';
                                    } else {
                                        document.getElementById('fancy_' + fancys[j].selection_id).classList.remove("suspended");
                                        document.getElementsByClassName('fancy_lay_price_' + fancys[j].selection_id).innerHTML = parseFloat(fancys[j].lay_price1);
                                        document.getElementsByClassName('fancy_back_price_' + fancys[j].selection_id).innerHTML = parseFloat(fancys[j].back_price1);
                                        document.getElementsByClassName('fancy_lay_size_' + fancys[j].selection_id).innerHTML = parseFloat(fancys[j].lay_size1);
                                        document.getElementsByClassName('fancy_back_size_' + fancys[j].selection_id).innerHTML = parseFloat(fancys[j].back_size1);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    const handleChange = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
    };
    const handleBetClick = () => {
        setBetShow(!betShow);
    }
    const [value, setValue] = React.useState(0);

    const fancyChange = (event, newValue) => {
        setValue(newValue);
    };

    function getEventData() {
        const event_id = params.event_id;
        var data;
        let url;



        if (userInfo) {
            url = "getDashboardDataByeventId"
            data = JSON.stringify({
                user_id: "6119ef3c2d2b398df5e863f1",
                event_id: "31062893",
            });
        } else {
            url = "getDashboardDataByEventIdWithoutUserID"
            data = JSON.stringify({
                event_id: "31062893",
            });
        }

        var config = {
            method: "post",
            url: `${Appconfig.apiUrl}eventsDashboard/${url}`,
            headers: {
                "Content-Type": "application/json",
            },
            data: data,
        };

        axios(config)
            .then(function (response) {
                // console.log("setEventData", response.data.resultData);                
                setEventData(response.data.resultData);
                loader_remove();
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    // console.log("setEventData", setEventData);
    // Stake Dynamic Start
    function getChips() {


        var data = JSON.stringify({
            "user_id": "6119ef3c2d2b398df5e863f1"
        });

        var config = {
            method: 'post',
            url: `${Appconfig.apiUrl}chips/getChips`,
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };

        axios(config)
            .then(function (response) {
                setchipList(response.data);
                let tmpChipList = response.data;
                const newChipList = tmpChipList.map((chipList, index) => {
                    tmpChipList[index]["id"] = index + 1;
                });
                loader_remove();

                setchipList(tmpChipList);

            })
            .catch(function (error) {
                console.log(error);
            });
    }
// Stake Dynamic end
    function betPlace() {
        if (userInfo) {
            if (userInfo.user_type == "User") {

                setPlacing(true);
                loader_default();
                var data = JSON.stringify({
                    user_id: userInfo._id,
                    match_id: BetPlaceData.event_id,
                    selection_id: BetPlaceData.selection_id,
                    is_back: BetPlaceData.is_back,
                    stake: StakeValue,
                    price_val: BetPlaceData.price,
                    market_id: BetPlaceData.market_id,
                    is_fancy: BetPlaceData.is_fancy == 1 ? "Yes" : "No",
                    market_name: "Match odds",
                    profit: ProfitValue,
                    loss: lossValue,
                });
                console.log("betting for fancy", data);

                var config = {
                    method: "post",
                    url: `${Appconfig.apiUrl}betting/addBetting`,
                    headers: {
                        "Content-Type": "application/json",
                    },
                    data: data,
                };
                // alert("success");
                axios(config)
                    .then(function (response) {
                        // setOpen(false);
                        setBetPlaceData({
                            event_id: "",
                            market_id: "",
                            is_back: "",
                            price: "",
                            is_fancy: "",
                            selection_id: "",
                            runner_name: "",
                        });

                        setProfitValue(0);
                        setStakeValue(0);
                        hideAllBetBox()
                        if (response.data.result == 0) {
                            notify(response.data.resultMessage, "error");
                        } else {



                            if (response.data.resultData[0].is_back == 1) {

                                document.getElementById("sideType").innerHTML = "Yes";
                                document.getElementById("selectionName").innerHTML = response.data.resultData[0].place_name;
                                document.getElementById("stake_notify").innerHTML = response.data.resultData[0].stake;
                                document.getElementById("price_notify_value").innerHTML = response.data.resultData[0].price_val;
                                var element = document.getElementById("sideType");
                                element.classList.add("blue-type");
                            }
                            if (response.data.resultData[0].is_back == 0) {

                                document.getElementById("sideType").innerHTML = "No";
                                document.getElementById("selectionName").innerHTML = response.data.resultData[0].place_name;
                                document.getElementById("stake_notify").innerHTML = response.data.resultData[0].stake;
                                document.getElementById("price_notify_value").innerHTML = response.data.resultData[0].price_val;
                                var element = document.getElementById("sideType");
                                element.classList.add("pink-type");

                            }
                            loader_remove();
                            document.getElementById("msgBox").style.display = "flex";
                            setTimeout(() => {
                                document.getElementById("msgBox").style.display = "none";
                            }, 3000);
                            
                        }
                        // alert(response.data.result);
                        setIsBetPlaced(!isBetPlaced);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        }
        else {
            history.push("/login")
        }
    }
    function notify(message, type) {
        toast.configure();
        if (type === 'error') {
            toast.error(message, {
                toastId: "error"
            });
        }
        else {
            toast.success(message, {
                toastId: "success",
            });
        }
    }


    async function getTvUrl(eventId) {
        await fetch(`${Appconfig.marketSarket}livetvurl/${eventId}`)
            .then(response => response.json())
            .then(data => setTvUrl(data.livetv));

    }
    async function getScoreUrl(eventId) {
        await fetch(`${Appconfig.marketSarket}animurl/${eventId}`)
            .then(response => response.json())
            .then(data => setScoreUrl(data.animation));
    }

    const hideAllBetBox = () => {
        const boxes = document.getElementsByClassName("pbb");
        for (var box of boxes) {
            box.style.setProperty("display", "none", "important")
        }
    }


    const handleOpen = (
        event_id,
        market_id,
        is_back,
        price,
        is_fancy,
        selection_id,
        runner_name,
        htmlId
    ) => {

        //alert(htmlId);
        hideAllBetBox();
        const userInfo = JSON.parse(window.sessionStorage.getItem("userData"));

        // console.log('click',`availableToBack1_price_${market_id.replace('.','')}_${selection_id}`);
        const priceNew = document.getElementById(htmlId).innerHTML;

        // console.log(`place-bet-block_${market_id.replace('.', '')}_${selection_id}`)
        // document.getElementsByClassName("pbb").style.setProperty("display", "none", "important");
        if (!is_fancy) {
            document.getElementsByClassName(`place-bet-block_${market_id.replace('.', '')}_${selection_id}`)[0].style.setProperty("display", "block", "important");
        } else {
            document.getElementsByClassName(`place-fancybet-block_${event_id}_${selection_id}`)[0].style.setProperty("display", "block", "important");
        }

        setBetPlaceData({
            event_id: event_id,
            market_id: market_id,
            is_back: is_back ? 1 : 0,
            price: priceNew,
            is_fancy: is_fancy,
            selection_id: selection_id,
            runner_name: runner_name,
        });
        // setOpen(true);

        // console.log("Bet Place Data", BetPlaceData);

    };
    const counter = 6;
    function openTabs(evt, Name ,Tab) {
        var i, tabcontent, tablinks , tab;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
          tabcontent[i].style.display = "none";
        }
        tab = document.getElementsByClassName("nav-item");
        for (i = 0; i < tab.length; i++) {
          tab[i].style.borderTop = "none";
        }
        document.getElementById(Name).style.display = "block";
        
        document.getElementById(Tab).style.borderTop = "2px solid white";
        // evt.currentTarget.className += "active";
      }
      function openLinks(evt, Name ) {
      
        var i, tabcontent, tablinks , tab;
        
        tab = document.getElementsByClassName("nav-item-fancy");
        for (i = 0; i < tab.length; i++) {
          tab[i].classList.remove("active")
        }
        document.getElementById(Name).classList.add("active");
        
        // evt.currentTarget.className += "active";
      }
    return (
        <>
            <Header isBetPlaced={isBetPlaced} />
            <div className={`game-page ${classes.root}`}>
                <Grid container spacing={0} style={{ backgroundColor: '#F0ECE1' }}>
                    {isLoggedIn ? null :
                        <div id="poker_loading" class="loading-wrap" style={{ 'marginTop': '50px' }}>
                            <ul class="loading">
                                <li><img src={Loader} /></li>
                                <li>Loading…</li>
                            </ul>
                        </div>
                    }
                    {isMobile ?
                        <>
                        <ul data-v-51245d04="" className="nav nav-tabs game-nav-bar full-market-tabs-mobile">
                            <li data-v-51245d04="" class="nav-item text-center full-market-active" id="Tab1">
                                <Link data-v-51245d04="" data-toggle="tab" href="#game1" class="nav-link nav-link tablinks " onClick={() => openTabs('event', 'oddsdtab' ,'Tab1')}>
                                    <div data-v-51245d04="">ODDS</div>
                                </Link>
                            </li>
                            <li data-v-51245d04="" class="nav-item text-center" id="Tab2">
                                <Link data-v-51245d04="" data-toggle="tab" href="#game2" class="nav-link nav-link tablinks" onClick={() => openTabs('event', 'matchedbet' , 'Tab2')}>
                                    <div data-v-51245d04="">MATCHED BET (0)</div>
                                </Link>
                            </li>
                        </ul>
                            <div id="oddsdtab" class="tabcontent activate">
                            <Grid item lg={7} xs={12} spacing={2} className={`cricket-section ${classes.tableHeight}`}>
                        <Grid container spacing={2} style={{ padding : "8px"}} >
                            <Grid item lg={12} xs={12} className="md-none xp-0">
                                <div className="match-block-header">
                                    <h5>Bangladesh v Pakistan</h5>
                                    <span>11/26/2021 9:30:00 AM</span>
                                </div>
                            </Grid>
                            <Grid item lg={12} xs={12} style={{ background: '#e0e6e6' }} className="cricket-banner xp-0">
                                <iframe src={scoreUrl} title="match-score" style={{ height: iframeHeight + "px", width: "100%" }} />
                            </Grid>
                            <Grid item lg={12} className="p-0" xs={12}>

                                {/* Match Odds Custom Section Start */}




                                <div id="marketBetsWrap" class="bets-wrap">
                                    <div id="minMaxBox" class="limit_info-popup" style={{ display: 'none' }}>
                                        <a class="close" onclick="javascript:$j('#minMaxBox').fadeOut();">Close</a>
                                        <dl>
                                            <dt id="minMaxDt" style={{ display: 'none' }}>Min / Max</dt>
                                            <dt id="maxDt">Max</dt>
                                            <dd id="minMaxInfo">1600</dd>
                                        </dl>
                                    </div>
                                    {EventData.length > 0 && EventData.map((event, index) =>
                                        event.marketTypes.map((market, index) =>
                                            market.marketRunners.length > 0 && market.market_name == "Match Odds" ?
                                                market.marketRunners.map((runner, i) => (
                                                    <>

                                                        <dl key={i * 14} className={`odd_even_clr table_row_${market.market_id.replace('.', '')}_${runner.selection_id}  ${runner.status === "suspended" ? "suspended" : ""}`} data-title={runner.status === "suspended" ? "SUSPENDED" : ""} id="selection_4708889" class="bets-selections" style={{ display: 'flex' }} key={i * 14} className={`odd_even_clr table_row_${market.market_id.replace('.', '')}_${runner.selection_id}  ${runner.status === "suspended" ? "suspended" : ""}`} data-title={runner.status === "suspended" ? "SUSPENDED" : ""} button>
                                                            <dt>

                                                                <div id="horseInfo" class="horse-info" style={{ display: 'none' }}>
                                                                    <ul id="horseBox" class="horse-box">
                                                                        <li id="clothNumberAlpha"></li>
                                                                        <li id="stallDraw"></li>
                                                                    </ul>
                                                                    <div id="uniform" class="uniform"><img src="" /></div>
                                                                    <ul id="horseName" class="horse-name">
                                                                        <li id="runnerName">{runner.runner_name}</li>
                                                                        <li id="jockyName"></li>
                                                                    </ul>
                                                                </div>

                                                                <h4 id="runnerName">{runner.runner_name}</h4>
                                                                <ul id="winLoss">
                                                                    <li id="withoutBet" class="win" style={{ display: 'none' }}></li>
                                                                    <li id="lossWithoutBet" class="win" style={{ display: 'none' }}></li>
                                                                    <li id="withBet" class="win" style={{ display: 'none' }}></li>
                                                                    <li id="lossWithBet" class="win" style={{ display: 'none' }}></li>
                                                                    <li id="zeroProfit" class="win" style={{ display: 'none' }}></li>
                                                                    <li id="zeroLiability" class="win" style={{ display: 'none' }}></li>
                                                                    <li id="zeroProfitWithBet" class="win" style={{ display: 'none' }}></li>
                                                                    <li id="zeroLiabilityWithBet" class="win" style={{ display: 'none' }}></li>
                                                                </ul>
                                                            </dt>
                                                            <dd id="suspend" class="suspend" style={{ display: 'none' }}><p>Suspend
                                                            </p></dd>
                                                            <dd id="closed" class="suspend" style={{ display: 'none' }}><p>Closed
                                                            </p></dd>
                                                            <dd id="nonRunner" class="suspend" style={{ display: 'none' }}><p>Non Runner
                                                            </p></dd>
                                                            <dd id="back_1">
                                                                <Link onClick={() => handleOpen(
                                                                    event.event_id,
                                                                    market.market_id,
                                                                    true,
                                                                    runner.back_1_price,
                                                                    false,
                                                                    runner.selection_id,
                                                                    runner.runner_name,
                                                                    `availableToBack1_price_${market.market_id.replace('.', '')}_${runner.selection_id}`
                                                                )
                                                                } className={`back-1 ${runner.status === "SUSPENDED" ? "suspended" : ""}`} to="#"><span className="backprice-match" id={`availableToBack1_price_${market.market_id.replace('.', '')}_${runner.selection_id}`}>{parseFloat(runner.back_1_price)}</span><span> {parseFloat(runner.back_1_size)}</span></Link>
                                                            </dd>
                                                            <dd id="lay_1">
                                                                <Link onClick={() => handleOpen(
                                                                    event.event_id,
                                                                    market.market_id,
                                                                    false,
                                                                    runner.lay_1_price,
                                                                    false,
                                                                    runner.selection_id,
                                                                    runner.runner_name,
                                                                    `availableToLay1_price_${market.market_id.replace('.', '')}_${runner.selection_id}`
                                                                )
                                                                } className={`lay-1 ${runner.status === "SUSPENDED" ? "suspended" : ""}`} to="#" fullmarketodds={parseFloat(runner.lay_1_price)}><span className="layprice-match" id={`availableToLay1_price_${market.market_id.replace('.', '')}_${runner.selection_id}`}>{parseFloat(runner.lay_1_price)}</span><span> {parseFloat(runner.lay_1_size)}</span></Link>
                                                            </dd>
                                                        </dl>

                                                        <div p={1} key={runner.selection_id} className={`pbb place-bet-block_${market.market_id.replace('.', '')}_${runner.selection_id}`} button>
                                                            <div className={`xs-bet-slip ${BetPlaceData.is_back ? "bcolor" : "lcolor"}`} colSpan="7">
                                                                <div className="bet-list" >
                                                                    <div>
                                                                        <p>&nbsp;</p>
                                                                        <Button className="typed" variant="contained">{BetPlaceData.price}</Button>
                                                                    </div>
                                                                    <div className="input-num">
                                                                        <p>Min Bet</p>
                                                                        <div>
                                                                            <a href="#" onClick={() => placeStakeValue(StakeValue - 1)} style={{ borderTopLeftRadius: '1.6vw', borderBottomLeftRadius: '1.6vw' }}>
                                                                                <RemoveIcon />
                                                                            </a>
                                                                            <span className={!StakeValue ? "typeing" : ""}>{StakeValue ? StakeValue : ""}</span>
                                                                            <a href="#" onClick={() => placeStakeValue(StakeValue + 1)} style={{ borderTopRightRadius: '1.6vw', borderBottomRightRadius: '1.6vw' }}>
                                                                                <AddIcon />
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="coin-list" >
                                                                    {chipList.slice(0, 6).map((row) => (                                                                   
                                                                        <Link href="#" onClick={() => placeStakeValue(row.chip_value)}>{row.chip_value}</Link>
                                                                    ))}                                                                
                                                                </div>                                                                        
                                                                <div className="keyboard-wrap">
                                                                    <div className="btn-tel">
                                                                        <Link to="/#" onClick={() => placeStakeValue(String(StakeValue).concat("1"))}>1</Link>
                                                                        <Link to="/#" onClick={() => placeStakeValue(String(StakeValue).concat("2"))}>2</Link>
                                                                        <Link to="/#" onClick={() => placeStakeValue(String(StakeValue).concat("3"))}>3</Link>
                                                                        <Link to="/#" onClick={() => placeStakeValue(String(StakeValue).concat("4"))}>4</Link>
                                                                        <Link to="/#" onClick={() => placeStakeValue(String(StakeValue).concat("5"))}>5</Link>
                                                                        <Link to="/#" onClick={() => placeStakeValue(String(StakeValue).concat("6"))}>6</Link>
                                                                        <Link to="/#" onClick={() => placeStakeValue(String(StakeValue).concat("7"))}>7</Link>
                                                                        <Link to="/#" onClick={() => placeStakeValue(String(StakeValue).concat("8"))}>8</Link>
                                                                        <Link to="/#" onClick={() => placeStakeValue(String(StakeValue).concat("9"))}>9</Link>
                                                                        <Link to="/#" onClick={() => placeStakeValue(String(StakeValue).concat("0"))}>0</Link>
                                                                        <Link to="/#" onClick={() => placeStakeValue(String(StakeValue).concat("00"))}>00</Link>
                                                                        <Link to="/#">.</Link>
                                                                    </div>
                                                                    <a href="#" onClick={() => placeStakeValue(String(StakeValue).substring(0, String(StakeValue).length - 1))}><img src={BtnDelete} /></a>
                                                                </div>
                                                                <div className="btn-list">
                                                                    <a className="cancelbtn" href="#" onClick={hideAllBetBox}>Cancel</a>
                                                                    <Link className={!StakeValue ? "placebtn disable" : "placebtn"} to="/#" onClick={() => betPlace()}>Place Bet</Link>
                                                                    {/* <Button
                variant="contained"
                className="eventSingleSuccessBtn"
                onClick={() => betPlace()}
                disabled={placing}
                endIcon={placing ? <CircularProgress size={20} style={{ color: "red" }} /> : ""}
                >
                {placing ? "PLACING...." : "PLACE BET"}
            </Button> */}
                                                                </div>
                                                                <div className={`acceptodds ${BetPlaceData.is_back ? "b-dark-color" : "l-dark-color"}`}>
                                                                    <label>
                                                                        <input type="checkbox" />
                                                                        <a>Accept Any Odds</a>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </>)) : ""))}
                                </div>


                                {/* Match Odds Custom Section End */}


                                {/* Book Marker Custom Section Start */}

                                {/*<div id="bookMakerMarket_30998640_130561" className="bets-sec-all bets-wrap bets-bookmaker" style={{ display: 'block' }}>*/}
                                   {/*  <h4>
                                        <span>Bookmaker Market</span>
                                        <a className="info-circle-full"><FontAwesomeIcon className="iconin-form" icon={faInfoCircle} /></a>
                                        <div id="bookMakerMinMax" class="fancy_info-popup">
                                            <dl>
                                                <dt>Min / Max</dt>
                                                <dd id="minMax"> 1.00 /  800.00</dd>
                                            </dl>
                                            <dl>
                                                <dt id="rebateName" style={{ display: 'none' }}>Rebate</dt>
                                                <dd id="rebate" style={{ display: 'none' }}></dd>
                                            </dl>
                                            <a href="" id="closeBookMakerInfo" class="close">Close</a>
                                        </div>
                                    </h4> */}
                                    {/* <dl class="bets-selections-head back-lay-bookmarker">
                                        <dt className="blank-matchodds-bg">Min:100 Max:200000</dt>
                                        <dd>Back</dd>
                                        <dd>Lay</dd>
                                    </dl> */}
                                    {/*<div class="table-header">
                                        <div class="float-left country-name box-6 min-max">
                                            <b>Min:100 Max:200000</b>
                                        </div>
                                        <div class="back box-1 float-left text-center">
                                            <b>BACK</b>
                                        </div>
                                        <div class="lay box-1 float-left text-center">
                                            <b>LAY</b>
                                        </div>
                                    </div>*/}
                                    {/* {EventData.length > 0 && EventData.map((event, index) =>
                                        event.marketTypes.map((market, index) =>
                                            market.marketRunners.length > 0 && market.market_name == "Bookmaker" ? ( */}
        
                                            {/* ) : ""))} */}
                                {/*</div>*/}



                                {/* Book Marker Custom Section End */}

                                 {/* Match Odds Section Start */}


<div data-v-1d41ced0="">
    <div class="market-title mt-1">
        MATCH_ODDS
        <p class="float-right mb-0 mt-0" onClick={() => showInfoModal('event', 'matchodds')}>
            <FontAwesomeIcon className="iconin-form" icon={faInfoCircle} />
        </p>
    </div>
    <div class="main-market">
        <div class="table-header">
            <div class="float-left country-name box-6 min-max">
                <b>
                    Max:
                    <span>10000</span>
                </b>
            </div>
            <div class="back box-1 float-left text-center">
                <b>BACK</b>
            </div>
            <div class="lay box-1 float-left text-center">
                <b>LAY</b>
            </div>
        </div>
        <div data-title="OPEN" class="table-body-odds">
            <div data-title="ACTIVE" class="table-row">
                <div class="float-left country-name box-4">
                    <span class="team-name">
                        <b>Australia</b>
                    </span>
                    <p>
                        <span class="float-left" style = {{color:"black"}}>0</span>
                    </p>
                </div>
                <div class="box-1 back1 float-left back-1 hidden-portrait text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">1.33</span>
                        <span class="d-block">140.9</span>
                    </button>
                </div>
                <div class="box-1 back2 float-left back-2 hidden-portrait text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">1.34</span> 
                        <span class="d-block">689.5</span>
                    </button>
                </div>
                <div class="box-1 back float-left back lock text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">1.35</span>
                        <span class="d-block">471.6</span>
                    </button>
                </div>
                <div class="box-1 lay float-left text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">1.36</span> 
                        <span class="d-block">3 K</span>
                    </button>
                </div>
                <div class="box-1 lay-2 float-left hidden-portrait text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">1.37</span> 
                        <span class="d-block">3.8 K</span>
                    </button>
                </div>
                <div class="box-1 lay-1 float-left hidden-portrait text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">1.38</span> 
                        <span class="d-block">1.8 K</span>
                    </button>
                </div>
            </div>
            <div data-title="ACTIVE" class="table-row">
                <div class="float-left country-name box-4">
                    <span class="team-name">
                        <b>England</b>
                    </span>
                    <p>
                        <span class="float-left" style = {{color:"black"}}>0</span>
                    </p>
                </div>
                <div class="box-1 back1 float-left back-1 hidden-portrait text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">8.2</span> 
                        <span class="d-block">211.4</span>
                    </button>
                    </div>
                <div class="box-1 back2 float-left back-2 hidden-portrait text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">8.4</span> 
                        <span class="d-block">131.7</span>
                    </button>
                </div>
                <div class="box-1 back float-left back lock text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">8.6</span>
                        <span class="d-block">50.3</span>
                    </button>
                </div>
                <div class="box-1 lay float-left text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">8.8</span> 
                        <span class="d-block">2.1 K</span>
                    </button>
                </div>
                <div class="box-1 lay-2 float-left hidden-portrait text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">9</span> 
                        <span class="d-block">1.5 K</span>
                    </button>
                </div>
                <div class="box-1 lay-1 float-left hidden-portrait text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">9.2</span> 
                        <span class="d-block">2.0</span>
                    </button>
                </div>
            </div>
            <div data-title="ACTIVE" class="table-row">
                <div class="float-left country-name box-4">
                    <span class="team-name">
                        <b>The Draw</b>
                    </span>
                    <p>
                        <span class="float-left" style = {{color:"black"}}>0</span>
                    </p>
                </div>
                <div class="box-1 back1 float-left back-1 hidden-portrait text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">6.2</span> 
                        <span class="d-block">980.8</span>
                    </button>
                </div>
                <div class="box-1 back2 float-left back-2 hidden-portrait text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">6.4</span> 
                            <span class="d-block">1 K</span>
                        </button>
                    </div>
                <div class="box-1 back float-left back lock text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">6.6</span>
                        <span class="d-block">701.0</span>
                    </button>
                </div>
                <div class="box-1 lay float-left text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">6.8</span> 
                        <span class="d-block">93.4</span>
                    </button>
                </div>
                <div class="box-1 lay-2 float-left hidden-portrait text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">7</span> 
                        <span class="d-block">132.2</span>
                    </button>
                    </div>
                <div class="box-1 lay-1 float-left hidden-portrait text-center">
                    <button onClick={showModal}>
                        <span class="odd d-block">7.2</span> 
                        <span class="d-block">3.6</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div>
       
    </div>
</div>



                                 {/* Match Odds Section End */}



                                
                                 {/* Book Marker Custom Section start */}
                                
<div  data-v-1d41ced0="" >
	<div class="market-title mt-1">
        Bookmaker
        <p class="float-right mb-0 mt-0" onClick={() => showInfoModal('event', 'bookmaker')}>
            <FontAwesomeIcon className="iconin-form" icon={faInfoCircle} />
        </p>
    </div> 
  	<div class="bookmaker-market">
  		<div class="table-header">
  			<div class="float-left country-name box-6 min-max">
  				<b>Min:100 Max:200000</b>
  			</div> 
  			<div class="back box-1 float-left text-center">
  				<b>BACK</b>
  			</div> 
  			<div class="lay box-1 float-left text-center">
  				<b>LAY</b>
  			</div>
  		</div> 
  		<div class="table-body">
  			<div data-title="CLOSED" class="table-row suspended">
  				<div class="float-left country-name box-4">
  					<span class="team-name">
  						<b>Sri Lanka</b>
  					</span> 
  					<p>
  						<span class="float-left" style = {{ color: "black"}}>0</span>
  					</p>
  				</div> 
  				<div class="box-1 back1 float-left back-1  text-center">
  					<button onClick={showModal}>
  						<span class="odd d-block">0</span> 
  						<span class="d-block">0.0</span>
  					</button>
  				</div> 
  				<div class="box-1 back2 float-left back-2  text-center">
  					<button onClick={showModal}>
  						<span class="odd d-block">0</span> 
  						<span class="d-block">0.0</span>
  					</button>
  				</div> 
  				<div class="box-1 back float-left back lock text-center">
  					<button onClick={showModal}>
  						<span class="odd d-block">0</span> 
  						<span class="d-block">0.0</span>
  					</button>
  				</div> 
  				<div class="box-1 lay float-left text-center">
  					<button onClick={showModal}>
  						<span class="odd d-block">0</span> 
  						<span class="d-block">0.0</span>
  					</button>
  				</div> 
  				<div class="box-1 lay-2 float-left  text-center">
  					<button onClick={showModal}>
  						<span class="odd d-block">0</span> 
  						<span class="d-block">0.0</span>
  					</button>
  				</div> 
  				<div class="box-1 lay-1 float-left  text-center">
  					<button onClick={showModal}>
  					<span class="odd d-block">0</span> 
  				<span class="d-block">0.0</span>
  					</button>
  				</div>
  			</div>
  				<div data-title="CLOSED" class="table-row suspended">
  					<div class="float-left country-name box-4">
  						<span class="team-name">
  							<b>West Indies</b>
  						</span> 
  						<p>
  							<span class="float-left" style = {{ color: "black"}}>0</span>
  						</p>
  					</div> 
  					<div class="box-1 back1 float-left back-1  text-center">
  						<button onClick={showModal}>
  							<span class="odd d-block">0</span> 
  							<span class="d-block">0.0</span>
  						</button>
  					</div> 
  					<div class="box-1 back2 float-left back-2  text-center">
  						<button onClick={showModal}>
  							<span class="odd d-block">0</span> 
  							<span class="d-block">0.0</span>
  						</button>
  					</div> 
  					<div class="box-1 back float-left back lock text-center">
  						<button onClick={showModal}>
  							<span class="odd d-block">0</span> 
  							<span class="d-block">0.0</span>
  						</button>
  					</div> 
  					<div class="box-1 lay float-left text-center">
  						<button onClick={showModal}>
  							<span class="odd d-block">0</span> 
  							<span class="d-block">0.0</span>
  						</button>
  					</div> 
  					<div class="box-1 lay-2 float-left  text-center">
  						<button onClick={showModal}>
  							<span class="odd d-block">0</span> 
  							<span class="d-block">0.0</span>
  						</button>
  					</div> 
  					<div class="box-1 lay-1 float-left  text-center">
  						<button onClick={showModal}>
  							<span class="odd d-block">0</span> 
  							<span class="d-block">0.0</span>
  						</button>
  					</div>
  				</div>
  				<div data-title="CLOSED" class="table-row suspended">
  					<div class="float-left country-name box-4">
  						<span class="team-name">
  							<b>The Draw</b>
  						</span> 
  						<p>
  							<span class="float-left" style = {{ color: "black"}}>0</span>
  						</p>
  					</div> 
  					<div class="box-1 back1 float-left back-1  text-center">
  						<button onClick={showModal}>
  							<span class="odd d-block">0</span> 
  							<span class="d-block">0.0</span>
  						</button>
  					</div> 
  					<div class="box-1 back2 float-left back-2  text-center">
  						<button onClick={showModal}>
  							<span class="odd d-block">0</span> 
  							<span class="d-block">0.0</span>
  						</button>
  					</div> 
  					<div class="box-1 back float-left back lock text-center">
  						<button onClick={showModal}>
  							<span class="odd d-block">0</span> 
  							<span class="d-block">0.0</span>
  						</button>
  					</div> 
  					<div class="box-1 lay float-left text-center">
  						<button onClick={showModal}>
  							<span class="odd d-block">0</span> 
  							<span class="d-block">0.0</span>
  						</button>
  					</div> 
  					<div class="box-1 lay-2 float-left  text-center">
  						<button onClick={showModal}>
  							<span class="odd d-block">0</span> 
  							<span class="d-block">0.0</span>
  						</button>
  					</div> 
  					<div class="box-1 lay-1 float-left  text-center">
  						<button onClick={showModal}>
  							<span class="odd d-block">0</span> 
  							<span class="d-block">0.0</span>
  						</button>
  					</div>
  				</div>
  			</div>
  		</div> 
  		<div class="table-remark text-right remark">
        India Vs New Zealand Match Bet Started In Our Exchange
    </div> 
    <div>
    	
    </div>
</div>
                                 {/* Book Marker Custom Section End */}

                                 {/* Fancy Market  Section Start */}
<div data-v-1d41ced0="" class="fancy-markets">
    <ul data-v-1d41ced0="" class="nav nav-tabs mt-2 fancy-nav">
        <li data-v-1d41ced0="" class="nav-item nav-item-fancy active" id="fancy-tab-1">
            <Link data-v-1d41ced0="" data-toggle="tab" href="javascript:void(0)"  class="nav-link" onClick={() => openLinks('event', 'fancy-tab-1')}>
                Fancy
            </Link>
        </li>
        <li data-v-1d41ced0="" className="nav-item nav-item-fancy" id="fancy-tab-2">
            <Link data-v-1d41ced0="" data-toggle="tab" href="javascript:void(0)"  class="nav-link" onClick={() => openLinks('event', 'fancy-tab-2' )}>
                Fancy 1
            </Link>
        </li>
        <li data-v-1d41ced0="" class="nav-item  nav-item-fancy" id="fancy-tab-3">
            <Link data-v-1d41ced0="" data-toggle="tab" href="javascript:void(0)"  class="nav-link" onClick={() => openLinks('event', 'fancy-tab-3' )}>
                Meter
            </Link>
        </li>
        <li data-v-1d41ced0="" class="nav-item nav-item-fancy" id="fancy-tab-4">
            <Link data-v-1d41ced0="" data-toggle="tab" href="javascript:void(0)"  class="nav-link" onClick={() => openLinks('event', 'fancy-tab-4' )}>
                Khado
            </Link>
        </li>
        <li data-v-1d41ced0="" class="nav-item nav-item-fancy" id="fancy-tab-5">
            <Link data-v-1d41ced0="" data-toggle="tab" href="javascript:void(0)"  class="nav-link" onClick={() => openLinks('event', 'fancy-tab-5' )}>
                Odd Even
            </Link>
        </li>
        <li data-v-1d41ced0="" class="nav-item nav-item-fancy" id="fancy-tab-6">
            <Link data-v-1d41ced0="" data-toggle="tab" href="javascript:void(0)" class="nav-link" onClick={() => openLinks('event', 'fancy-tab-6' )}>
                Wicket
            </Link>
        </li>
        <li data-v-1d41ced0="" class="nav-item nav-item-fancy" id="fancy-tab-7">
            <Link data-v-1d41ced0="" data-toggle="tab" href="javascript:void(0)" class="nav-link"  onClick={() => openLinks('event', 'fancy-tab-7' )}> 
                Four
            </Link>
        </li>
        <li data-v-1d41ced0="" class="nav-item nav-item-fancy" id="fancy-tab-8">
            <Link data-v-1d41ced0="" data-toggle="tab" href="javascript:void(0)"  class="nav-link"  onClick={() => openLinks('event', 'fancy-tab-8' )}>
                Six
            </Link>
        </li>
        <li data-v-1d41ced0="" class="nav-item nav-item-fancy" id="fancy-tab-9">
            <Link data-v-1d41ced0="" data-toggle="tab" href="javascript:void(0)" class="nav-link"  onClick={() => openLinks('event', 'fancy-tab-9' )}>
                Cricket Casino
            </Link>
        </li>
    </ul>
    <div data-v-1d41ced0="" class="tab-content">
        <div data-v-1d41ced0="" class="fancy-market">
            <div data-v-1d41ced0="">
                <div class="table-header">
                    <div class="market-title float-left country-name box-4">
                        <span>Session Market</span>
                        <p class="float-right mb-0 mt-0" onClick={() => showInfoModal('event', 'sessionmarket')}>
                            <FontAwesomeIcon className="iconin-form" icon={faInfoCircle} />
                        </p>
                    </div>
                    <div class="box-1 float-left lay text-center">
                        <b>No</b>
                    </div>
                    <div class="back box-1 float-left back text-center">
                        <b>Yes</b>
                    </div>
                </div>
                <div class="table-body">
                    <div class="fancy-tripple">
                        <div data-title="" class="table-row">
                            <div class="float-left country-name box-4">
                                <span>
                                    <b>75 over run ENG 2</b>
                                </span>
                                <div class="float-right">
                                    <div class="info-block">
                                        <a href="" data-toggle="collapse"
                                            data-target="#min-max-info473" aria-expanded="false" class="info-icon collapsed">
                                            <i class="fas fa-info-circle m-l-10"></i>
                                        </a>
                                        <div id="min-max-info473" class="min-max-info collapse">
                                            <span>
                                                <b>Min:</b>
                                                100
                                            </span> 
                                            <span>
                                                <b>Max:</b>
                                                50000
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <p>
                                    <span class="float-left" style = {{color:"black"}}>0</span>
                                </p>
                            </div>
                            <div class="box-1 lay float-left text-center">
                                <button>
                                    <span class="odd d-block">232</span>
                                    <span class="d-block">100</span>
                                </button>
                            </div>
                            <div class="box-1 back float-left text-center">
                                <button>
                                    <span class="odd d-block">233</span>
                                    <span class="d-block">100</span>
                                </button>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="fancy-tripple">
                        <div data-title="" class="table-row">
                            <div class="float-left country-name box-4">
                                <span>
                                    <b>Fall of 3rd wkt ENG 2</b>
                                </span>
                                <div class="float-right">
                                    <div class="info-block">
                                        <a href="" data-toggle="collapse" data-target="#min-max-info386" aria-expanded="false" class="info-icon collapsed"><i class="fas fa-info-circle m-l-10"></i></a>
                                        <div id="min-max-info386" class="min-max-info collapse">
                                            <span>
                                                <b>Min:</b>
                                                100
                                            </span> 
                                            <span>
                                                <b>Max:</b>
                                                50000
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <p>
                                    <span class="float-left" style = {{color:"black"}}>0</span>
                                </p>
                            </div>
                            <div class="box-1 lay float-left text-center">
                                <button>
                                    <span class="odd d-block">255</span>
                                    <span class="d-block">110</span>
                                </button>
                            </div>
                            <div class="box-1 back float-left text-center">
                                <button>
                                    <span class="odd d-block">255</span>
                                    <span class="d-block">90</span>
                                </button>
                            </div>
                        </div>
                        
                    </div>
                   
                    <div class="fancy-tripple">
                        <div data-title="" class="table-row">
                            <div class="float-left country-name box-4">
                                <span>
                                    <b>D Malan run 2</b>
                                </span>
                                <div class="float-right">
                                    <div class="info-block">
                                        <a href="" data-toggle="collapse" data-target="#min-max-info364" aria-expanded="false" class="info-icon collapsed"><i class="fas fa-info-circle m-l-10"></i></a>
                                        <div id="min-max-info364" class="min-max-info collapse">
                                            <span>
                                                <b>Min:</b>
                                                100
                                            </span> 
                                            <span>
                                                <b>Max:</b>
                                                50000
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <p>
                                    <span class="float-left" style = {{color:"black"}}>0</span>
                                </p>
                            </div>
                            <div class="box-1 lay float-left text-center">
                                <button>
                                    <span class="odd d-block">103</span>
                                    <span class="d-block">110</span>
                                </button>
                            </div>
                            <div class="box-1 back float-left text-center">
                                <button>
                                    <span class="odd d-block">103</span>
                                    <span class="d-block">90</span>
                                </button>
                            </div>
                        </div>
                      
                    </div>
                    
                    <div class="fancy-tripple">
                        <div data-title="" class="table-row">
                            <div class="float-left country-name box-4">
                                <span>
                                    <b>J Root run 2</b>
                                </span>
                                <div class="float-right">
                                    <div class="info-block">
                                        <a href="" data-toggle="collapse" data-target="#min-max-info387" aria-expanded="false" class="info-icon collapsed"><i class="fas fa-info-circle m-l-10"></i></a>
                                        <div id="min-max-info387" class="min-max-info collapse">
                                            <span>
                                                <b>Min:</b>
                                                100
                                            </span> 
                                            <span>
                                                <b>Max:</b>
                                                50000
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <p>
                                    <span class="float-left" style = {{color:"black"}}>0</span>
                                </p>
                            </div>
                            <div class="box-1 lay float-left text-center">
                                <button>
                                    <span class="odd d-block">122</span>
                                    <span class="d-block">100</span>
                                </button>
                                </div>
                            <div class="box-1 back float-left text-center">
                                <button>
                                    <span class="odd d-block">122</span>
                                    <span class="d-block">80</span>
                                </button>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="fancy-tripple">
                        <div data-title="" class="table-row">
                            <div class="float-left country-name box-4">
                                <span>
                                    <b>D Malan boundaries 2</b>
                                </span>
                                <div class="float-right">
                                    <div class="info-block">
                                        <a href="" data-toggle="collapse" data-target="#min-max-info365" aria-expanded="false" class="info-icon collapsed"><i class="fas fa-info-circle m-l-10"></i></a>
                                        <div id="min-max-info365" class="min-max-info collapse">
                                            <span>
                                                <b>Min:</b>
                                                100
                                            </span> 
                                            <span>
                                                <b>Max:</b>
                                                10000
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <p>
                                    <span class="float-left" style = {{color:"black"}}>0</span>
                                </p>
                            </div>
                            <div class="box-1 lay float-left text-center">
                                <button>
                                    <span class="odd d-block">13</span>
                                    <span class="d-block">100</span>
                                </button>
                            </div>
                            <div class="box-1 back float-left text-center">
                                <button>
                                    <span class="odd d-block">14</span>
                                    <span class="d-block">100</span>
                                </button>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="fancy-tripple">
                        <div data-title="" class="table-row">
                            <div class="float-left country-name box-4">
                                <span>
                                    <b>J Root boundaries 2</b>
                                </span>
                                <div class="float-right">
                                    <div class="info-block">
                                        <a href="" data-toggle="collapse" data-target="#min-max-info388" aria-expanded="false" class="info-icon collapsed"><i class="fas fa-info-circle m-l-10"></i></a>
                                        <div id="min-max-info388" class="min-max-info collapse">
                                            <span>
                                                <b>Min:</b>
                                                100
                                            </span> 
                                            <span>
                                                <b>Max:</b>
                                                10000
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <p>
                                    <span class="float-left" style = {{color:"black"}}>0</span>
                                </p>
                            </div>
                            <div class="box-1 lay float-left text-center">
                                <button>
                                    <span class="odd d-block">14</span>
                                    <span class="d-block">100</span>
                                </button>
                            </div>
                            <div class="box-1 back float-left text-center">
                                <button>
                                    <span class="odd d-block">15</span>
                                    <span class="d-block">100</span>
                                </button>
                            </div>
                        </div>
                       
                    </div>
                   
                </div>
                
                <div>
                   
                </div>
            </div>
            
        </div>
    </div>
</div>

                                 {/* Fancy Market  Section  End */}



                                {/* Book Marker Section Table */}

                                {/*}
                                {EventData.length > 0 && EventData.map((event, index) =>
                                    event.marketTypes.map((market, index) =>
                                        market.marketRunners.length > 0 && market.market_name == "Bookmaker" ? (
                                            <Table className={classes.table} aria-label="simple table tableee">
                                                <TableHead>
                                                    <TableRow className="special_bet" >
                                                        <TableCell colSpan="7" className="lg-none">
                                                            <Link style={{ width: "300px", height: "10px" }} className="header-pin">
                                                                {isMobile ?
                                                                    <img alt="cricket" src={Greypin} />
                                                                    :
                                                                    <img alt="cricket" src={pin} />
                                                                }
                                                            </Link>
                                                            <span style={{ fontWeight: "bold", fontSize: "15px" }}>{market.market_name} Market </span>
                                                            <span style={{ color: "#a4adb3" }}> | Zero Commission</span>
                                                            <Link className="header-info-ico"></Link>
                                                        </TableCell>
                                                        <TableCell colSpan="8" className="xs-none">
                                                            <Box display='flex' justifyContent="space-between">
                                                                <Box display='flex'>
                                                                    <Link className="bookmark-last-icon"></Link>
                                                                    <Typography variant="span" style={{ fontWeight: "bold", fontSize: "15px" }}>{market.market_name} Market </Typography>
                                                                    <Typography variant="span" style={{ color: "#a4adb3" }}> | Zero Commission</Typography>
                                                                </Box>
                                                                <Box>
                                                                    <Typography variant="span" className="fancy-info">Min</Typography>
                                                                    <Typography variant="span" className="bookmarket-header-text">500</Typography>
                                                                    <Typography variant="span" className="fancy-info">Max</Typography>
                                                                    <Typography variant="span" className="bookmarket-header-text">40,000.00</Typography>
                                                                </Box>
                                                            </Box>
                                                        </TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody className="fullmarket_tbl_body">
                                                    <TableRow>
                                                        <TableCell colSpan="3"></TableCell>
                                                        <TableCell><strong>Back</strong></TableCell>
                                                        <TableCell><strong>Lay</strong></TableCell>
                                                        <TableCell colSpan="2" className="xs-none"></TableCell>
                                                    </TableRow>
                                                    {market.marketRunners.map((runner, i) => (
                                                        <>
                                                            <TableRow p={1} key={i * 15} className={`table_row_${market.market_id.replace('.', '')}_${runner.selection_id}  ${runner.status === "suspended" ? "suspended" : ""}`} data-title={runner.status === "suspended" ? "SUSPENDED" : ""} button>
                                                                <TableCell className="table_first_row" colSpan="1">
                                                                    <div style={{ fontWeight: 'bold', }}>
                                                                        <span> {event.event_name} </span>
                                                                    </div>
                                                                </TableCell>
                                                                <TableCell align="right" onClick={handleBetClick} colSpan="3" className="fancy-suspend">
                                                                    <dl className="back-gradient back-block">
                                                                        <span className="xs-none" onClick={() =>
                                                                            handleOpen(
                                                                                event.event_id,
                                                                                market.market_id,
                                                                                true,
                                                                                runner.back_3_price,
                                                                                false,
                                                                                runner.selection_id,
                                                                                runner.runner_name,
                                                                                `availableToBack3_price_${market.market_id.replace('.', '')}_${runner.selection_id}`
                                                                            )
                                                                        } id={`availableToBack3_price_${market.market_id.replace('.', '')}_${runner.selection_id}`}>{runner.back_3_price}</span>
                                                                        <span onClick={() =>
                                                                            handleOpen(
                                                                                event.event_id,
                                                                                market.market_id,
                                                                                true,
                                                                                runner.back_2_price,
                                                                                false,
                                                                                runner.selection_id,
                                                                                runner.runner_name,
                                                                                `availableToBack2_price_${market.market_id.replace('.', '')}_${runner.selection_id}`
                                                                            )
                                                                        } className="xs-none" id={`availableToBack2_price_${market.market_id.replace('.', '')}_${runner.selection_id}`}>{runner.back_2_price}</span>
                                                                        <span onClick={() =>
                                                                            handleOpen(
                                                                                event.event_id,
                                                                                market.market_id,
                                                                                true,
                                                                                runner.back_1_price,
                                                                                false,
                                                                                runner.selection_id,
                                                                                runner.runner_name,
                                                                                `availableToBack1_price_${market.market_id.replace('.', '')}_${runner.selection_id}`
                                                                            )
                                                                        }><div className="back-box" id={`availableToBack1_price_${market.market_id.replace('.', '')}_${runner.selection_id}`}>{runner.back_1_price}</div></span>
                                                                    </dl>
                                                                </TableCell>
                                                                <TableCell align="right" colSpan="3" className="fancy-suspend">
                                                                    <dl className="lay-gradient lay-block">
                                                                        <span onClick={() =>
                                                                            handleOpen(
                                                                                event.event_id,
                                                                                market.market_id,
                                                                                false,
                                                                                runner.lay_1_price,
                                                                                false,
                                                                                runner.selection_id,
                                                                                runner.runner_name,
                                                                                `availableToLay1_price_${market.market_id.replace('.', '')}_${runner.selection_id}`
                                                                            )
                                                                        }><div className="lay-box" id={`availableToLay1_price_${market.market_id.replace('.', '')}_${runner.selection_id}`}>{runner.lay_1_price}</div></span>
                                                                        <span onClick={() =>
                                                                            handleOpen(
                                                                                event.event_id,
                                                                                market.market_id,
                                                                                false,
                                                                                runner.lay_2_price,
                                                                                false,
                                                                                runner.selection_id,
                                                                                runner.runner_name,
                                                                                `availableToLay2_price_${market.market_id.replace('.', '')}_${runner.selection_id}`
                                                                            )
                                                                        } className="xs-none" id={`availableToLay2_price_${market.market_id.replace('.', '')}_${runner.selection_id}`}>{runner.lay_2_price}</span>
                                                                        <span onClick={() =>
                                                                            handleOpen(
                                                                                event.event_id,
                                                                                market.market_id,
                                                                                false,
                                                                                runner.lay_3_price,
                                                                                false,
                                                                                runner.selection_id,
                                                                                runner.runner_name,
                                                                                `availableToLay3_price_${market.market_id.replace('.', '')}_${runner.selection_id}`
                                                                            )
                                                                        } className="xs-none" id={`availableToLay3_price_${market.market_id.replace('.', '')}_${runner.selection_id}`}>{runner.lay_3_price}</span>
                                                                    </dl>
                                                                </TableCell>
                                                            </TableRow>
                                                            {isMobile ?

                                                                <TableRow p={1} key={runner.selection_id} className={`pbb place-bet-block_${market.market_id.replace('.', '')}_${runner.selection_id}`} button>
                                                                    <TableCell className={`xs-bet-slip ${BetPlaceData.is_back ? "bcolor" : "lcolor"}`} colSpan="7">
                                                                        <div className="bet-list" >
                                                                            <div>
                                                                                <p>&nbsp;</p>
                                                                                <Button className="typed" variant="contained">{BetPlaceData.price}</Button>
                                                                            </div>
                                                                            <div className="input-num">
                                                                                <p>Min Bet</p>
                                                                                <div>
                                                                                    <a href="#" onClick={() => placeStakeValue(StakeValue - 1)} style={{ borderTopLeftRadius: '1.6vw', borderBottomLeftRadius: '1.6vw' }}>
                                                                                        <RemoveIcon />
                                                                                    </a>
                                                                                    <span className={!StakeValue ? "typeing" : ""}>{StakeValue ? StakeValue : ""}</span>
                                                                                    <a href="#" onClick={() => placeStakeValue(StakeValue + 1)} style={{ borderTopRightRadius: '1.6vw', borderBottomRightRadius: '1.6vw' }}>
                                                                                        <AddIcon />
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="coin-list" >
                                                                            <a href="#" onClick={() => placeStakeValue(4)}>4</a>
                                                                            <a href="#" onClick={() => placeStakeValue(30)}>30</a>
                                                                            <a href="#" onClick={() => placeStakeValue(50)}>50</a>
                                                                            <a href="#" onClick={() => placeStakeValue(200)}>200</a>
                                                                            <a href="#" onClick={() => placeStakeValue(500)}>500</a>
                                                                            <a href="#" onClick={() => placeStakeValue(1000)}>1000</a>
                                                                        </div>
                                                                        <div className="keyboard-wrap">
                                                                            <div className="btn-tel">
                                                                                <a href="#" onClick={() => placeStakeValue(String(StakeValue).concat("1"))}>1</a>
                                                                                <a href="#" onClick={() => placeStakeValue(String(StakeValue).concat("2"))}>2</a>
                                                                                <a href="#" onClick={() => placeStakeValue(String(StakeValue).concat("3"))}>3</a>
                                                                                <a href="#" onClick={() => placeStakeValue(String(StakeValue).concat("4"))}>4</a>
                                                                                <a href="#" onClick={() => placeStakeValue(String(StakeValue).concat("5"))}>5</a>
                                                                                <a href="#" onClick={() => placeStakeValue(String(StakeValue).concat("6"))}>6</a>
                                                                                <a href="#" onClick={() => placeStakeValue(String(StakeValue).concat("7"))}>7</a>
                                                                                <a href="#" onClick={() => placeStakeValue(String(StakeValue).concat("8"))}>8</a>
                                                                                <a href="#" onClick={() => placeStakeValue(String(StakeValue).concat("9"))}>9</a>
                                                                                <a href="#" onClick={() => placeStakeValue(String(StakeValue).concat("0"))}>0</a>
                                                                                <a href="#" onClick={() => placeStakeValue(String(StakeValue).concat("00"))}>00</a>
                                                                                <a href="#">.</a>
                                                                            </div>
                                                                            <a href="#" onClick={() => placeStakeValue(String(StakeValue).substring(0, String(StakeValue).length - 1))}><img src={BtnDelete} /></a>
                                                                        </div>
                                                                        <div className="btn-list">
                                                                            <a className="cancelbtn" href="#" onClick={hideAllBetBox}>Cancel</a>
                                                                            <a className={!StakeValue ? "placebtn disable" : "placebtn"} href="#" onClick={() => betPlace()}>Place Bet</a>
                                                                        </div>
                                                                        <div className={`acceptodds ${BetPlaceData.is_back ? "b-dark-color" : "l-dark-color"}`}>
                                                                            <label>
                                                                                <input type="checkbox" />
                                                                                <a>Accept Any Odds</a>
                                                                            </label>
                                                                        </div>
                                                                    </TableCell>
                                                                </TableRow>

                                                                :
                                                                <>
                                                                    <TableRow p={1} key={runner.selection_id} className={`place-bet-block  place-bet-block_${market.market_id.replace('.', '')}_${runner.selection_id}`} button>
                                                                        <TableCell></TableCell>
                                                                        <TableCell colSpan="6" className="place-bet-td">
                                                                            <FormControlLabel
                                                                                control={
                                                                                    <Checkbox
                                                                                        checked={state.checkedB}
                                                                                        onChange={handleChange}
                                                                                        name="checkedB"
                                                                                        color="primary"
                                                                                    />
                                                                                }
                                                                                label="Accept Any Odds"
                                                                                className="checkbox"
                                                                            />
                                                                            <Button variant="controlled" className="site-btn cancel-btn">Cancel</Button>
                                                                            <span className="odds">83</span>
                                                                            <input value={StakeValue} />
                                                                            <Button variant="controlled" className="site-btn place-bet-btn">Place Bets</Button>
                                                                        </TableCell>
                                                                    </TableRow>
                                                                    <TableRowLink p={1} key={runner.selection_id} className="place-bet-sec-block" button>
                                                                        <TableCell colSpan="7" className="place-bet-td">
                                                                            <Button variant="controlled" className="site-btn bet-value" onClick={() => placeStakeValue(4)}>4</Button>
                                                                            <Button variant="controlled" className="site-btn bet-value" onClick={() => placeStakeValue(30)}>30</Button>
                                                                            <Button variant="controlled" className="site-btn bet-value" onClick={() => placeStakeValue(50)}>50</Button>
                                                                            <Button variant="controlled" className="site-btn bet-value" onClick={() => placeStakeValue(200)}>200</Button>
                                                                            <Button variant="controlled" className="site-btn bet-value" onClick={() => placeStakeValue(500)}>500</Button>
                                                                            <Button variant="controlled" className="site-btn bet-value" onClick={() => placeStakeValue(1000)}>1000</Button>
                                                                        </TableCell>
                                                                    </TableRowLink>
                                                                </>
                                                            }
                                                        </>
                                                    ))}
                                                </TableBody>
                                            </Table>
                                        ) : ""))}   */}
                            </Grid>
                            <Grid item lg={12} spacing={2} className="xs-none">
                                <Grid container spacing={2}>
                                    <Grid item lg={6} className="xs-none">
                                        <Accordion defaultExpanded={true} style={{ marginTop: 10 }}>
                                            <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" className="bet-slip-bar" style={{ backgroundImage: `url(${background})` }}>
                                                <Link className="bookmark-last-icon-table"></Link>
                                                <Typography>Over/Under 2.5 Goals</Typography>
                                            </AccordionSummary>
                                            <AccordionDetails className="d-none p-0" display="inline-block" style={{ height: "100%" }}>
                                                <Table className={classes.table} aria-label="simple table">
                                                    <TableHead>
                                                        <TableRow className="tbl_head xs-none">
                                                            <TableCell colSpan="1">Matched: PTE 0</TableCell>
                                                            <TableCell align="center">Back</TableCell>
                                                            <TableCell align="center">Lay</TableCell>
                                                        </TableRow>
                                                    </TableHead>
                                                    <TableBody className="bookmark-full-market">
                                                        {rows.map((row, index) => (
                                                            <TableRowLink to={row.link} p={1} key={index} className="odd_even_clr" button>
                                                                <TableCell className="table_first_row" colSpan="1">
                                                                    <div className="text_left-in-bookmark">
                                                                        <img className="icon-predict" alt="chart" src={transparent} />
                                                                        {row.name1}
                                                                    </div>
                                                                </TableCell>
                                                                <TableCell align="center" className="xs-ipl-block" style={{ backgroundColor: '#72BBEF' }}>
                                                                    <div style={{ display: 'block' }}>
                                                                        <Typography variant='span'>{row.calories}</Typography>
                                                                    </div>
                                                                </TableCell>
                                                                <TableCell align="center" className="xs-ipl-block" style={{ backgroundColor: '#FAA9BA' }}>
                                                                    <div style={{ display: 'block' }}>
                                                                        <Typography variant='span'>{row.fat}</Typography>
                                                                    </div>
                                                                </TableCell>
                                                            </TableRowLink>
                                                        ))}
                                                        <TableRow className="tbl_head xs-none">
                                                            <TableCell colSpan="1"></TableCell>
                                                            <TableCell colSpan="2" align="right"><Link to="#">View Full Market</Link></TableCell>
                                                        </TableRow>
                                                    </TableBody>
                                                </Table>
                                            </AccordionDetails>
                                        </Accordion>
                                    </Grid>
                                    <Grid item lg={6} className="xs-none">
                                        <Accordion defaultExpanded={true} style={{ marginTop: 10 }}>
                                            <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" className="bet-slip-bar" style={{ backgroundImage: `url(${background})` }}>
                                                <Link className="bookmark-last-icon-table"></Link>
                                                <Typography>Over/Under 2.5 Goals</Typography>
                                            </AccordionSummary>
                                            <AccordionDetails className="d-none p-0" display="inline-block" style={{ height: "100%" }}>
                                                <Table className={classes.table} aria-label="simple table">
                                                    <TableHead>
                                                        <TableRow className="tbl_head xs-none">
                                                            <TableCell colSpan="1">Matched: PTE 0</TableCell>
                                                            <TableCell align="center">Back</TableCell>
                                                            <TableCell align="center">Lay</TableCell>
                                                        </TableRow>
                                                    </TableHead>
                                                    <TableBody className="bookmark-full-market">
                                                        {rows.map((row, index) => (
                                                            <TableRowLink to={row.link} p={1} key={index * 11} className="odd_even_clr" button>
                                                                <TableCell className="table_first_row" colSpan="1">
                                                                    <div className="text_left-in-bookmark">
                                                                        <img className="icon-predict" alt="chart" src={transparent} />
                                                                        {row.name1}
                                                                    </div>
                                                                </TableCell>
                                                                <TableCell align="center" className="xs-ipl-block" style={{ backgroundColor: '#72BBEF' }}>
                                                                    <div style={{ display: 'block' }}>
                                                                        <Typography variant='span'>{row.calories}</Typography>
                                                                    </div>
                                                                </TableCell>
                                                                <TableCell align="center" className="xs-ipl-block" style={{ backgroundColor: '#FAA9BA' }}>
                                                                    <div style={{ display: 'block' }}>
                                                                        <Typography variant='span'>{row.fat}</Typography>
                                                                    </div>
                                                                </TableCell>
                                                            </TableRowLink>
                                                        ))}
                                                        <TableRow className="tbl_head xs-none">
                                                            <TableCell colSpan="1"></TableCell>
                                                            <TableCell colSpan="2" align="right"><Link to="#">View Full Market</Link></TableCell>
                                                        </TableRow>
                                                    </TableBody>
                                                </Table>
                                            </AccordionDetails>
                                        </Accordion>
                                    </Grid>
                                    <Grid item lg={6} spacing={2} className="xs-none">
                                        <Accordion defaultExpanded={true} style={{ marginTop: 10 }}>
                                            <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" className="bet-slip-bar" style={{ backgroundImage: `url(${background})` }}>
                                                <Link className="bookmark-last-icon-table"></Link>
                                                <Typography>Over/Under 2.5 Goals</Typography>
                                            </AccordionSummary>
                                            <AccordionDetails className="d-none p-0" display="inline-block" style={{ height: "100%" }}>
                                                <Table className={classes.table} aria-label="simple table">
                                                    <TableHead>
                                                        <TableRow className="tbl_head xs-none">
                                                            <TableCell colSpan="1">Matched: PTE 0</TableCell>
                                                            <TableCell align="center">Back</TableCell>
                                                            <TableCell align="center">Lay</TableCell>
                                                        </TableRow>
                                                    </TableHead>
                                                    <TableBody className="bookmark-full-market">
                                                        {rows.map((row, index) => (
                                                            <TableRowLink to={row.link} p={1} key={index * 12} className="odd_even_clr" button>
                                                                <TableCell className="table_first_row" colSpan="1">
                                                                    <div className="text_left-in-bookmark">
                                                                        <img className="icon-predict" alt="chart" src={transparent} />
                                                                        {row.name1}
                                                                    </div>
                                                                </TableCell>
                                                                <TableCell align="center" className="xs-ipl-block" style={{ backgroundColor: '#72BBEF' }}>
                                                                    <div style={{ display: 'block' }}>
                                                                        <Typography variant='span'>{row.calories}</Typography>
                                                                    </div>
                                                                </TableCell>
                                                                <TableCell align="center" className="xs-ipl-block" style={{ backgroundColor: '#FAA9BA' }}>
                                                                    <div style={{ display: 'block' }}>
                                                                        <Typography variant='span'>{row.fat}</Typography>
                                                                    </div>
                                                                </TableCell>
                                                            </TableRowLink>
                                                        ))}
                                                        <TableRow className="tbl_head xs-none">
                                                            <TableCell colSpan="1"></TableCell>
                                                            <TableCell colSpan="2" align="right"><Link to="#">View Full Market</Link></TableCell>
                                                        </TableRow>
                                                    </TableBody>
                                                </Table>
                                            </AccordionDetails>
                                        </Accordion>
                                    </Grid>
                                </Grid>
                            </Grid>
                            {/* Child Grid item End */}
                        </Grid>
                        {/* Child Grid Container End */}
                       
                            </Grid>
                            </div>
                            <div id="matchedbet" class="tabcontent">
                                <h3>Match Bets</h3>
                            </div>
                        {/* Models Start */}

                            <div data-v-1d41ced0="" id="__BVID__272___BV_modal_outer_" style={{display: "none",position: "absolute", zIndex: "1040"}}>
                            <div id="__BVID__272" role="dialog" aria-labelledby="__BVID__272___BV_modal_title_" aria-describedby="__BVID__272___BV_modal_body_" class="modal fade show place-bet-modal" aria-modal="true" style={{display: "block"}}>
                                <div class="modal-dialog modal-md" style={{marginTop: "58px"}}><span tabindex="0"></span>
                                    <div role="document" id="__BVID__272___BV_modal_content_" tabindex="-1" class="modal-content">
                                        <header id="__BVID__272___BV_modal_header_" class="modal-header" >
                                            <h5 id="__BVID__272___BV_modal_title_" class="modal-title" style={{marginTop :"0"}}>Placebet</h5>
                                            <button type="button" aria-label="Close" class="close" onClick={hideModal} >×</button>
                                        </header>
                                        <div id="__BVID__272___BV_modal_body_" class="modal-body">
                                            <div data-v-1d41ced0="">
                                                <div data-v-1d41ced0="" class="place-bet pt-2 pb-2 back">
                                                    <div data-v-1d41ced0="" class="container-fluid container-fluid-5">
                                                        <div data-v-1d41ced0="" class="row row5">
                                                            <div data-v-1d41ced0="" class="col-5 text-bold"> Northern Brave
                                                                
                                                            </div>
                                                            <div data-v-1d41ced0="" class="col-7 text-right">
                                                                <div data-v-1d41ced0="" class="float-right">
                                                                    <button data-v-1d41ced0="" class="stakeactionminus btn">
                                                                        <span data-v-1d41ced0="" class="fa fa-minus">
                                                                        <FontAwesomeIcon  icon={faMinus} />
                                                                        </span>
                                                                    </button>
                                                                    <input data-v-1d41ced0="" type="text" placeholder="15" class="stakeinput"/>
                                                                    <button data-v-1d41ced0="" class="stakeactionminus btn">
                                                                        <span data-v-1d41ced0="" class="fa fa-plus">
                                                                        <FontAwesomeIcon  icon={faPlus} />
                                                                        </span>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-1d41ced0="" class="row row5 mt-2">
                                                            <div data-v-1d41ced0="" class="col-4">
                                                                <input data-v-1d41ced0="" type="number" placeholder="00" class="stakeinput w-100"/>
                                                            </div>
                                                            <div data-v-1d41ced0="" class="col-4">
                                                                <button data-v-1d41ced0="" class="btn btn-primary btn-block">
                                                                    Submit </button>
                                                            </div>
                                                            <div data-v-1d41ced0="" style={{height: "28px"}} class="col-4 text-center"><span data-v-1d41ced0="">0</span></div>
                                                        </div>
                                                        <div data-v-1d41ced0="" class="row row5 mt-2">
                                                            <div data-v-1d41ced0="" class="col-4">
                                                                <button data-v-1d41ced0="" class="btn btn-secondary btn-block mb-2">100</button>
                                                            </div>
                                                            <div data-v-1d41ced0="" class="col-4">
                                                                <button data-v-1d41ced0="" class="btn btn-secondary btn-block mb-2">500</button>
                                                            </div>
                                                            <div data-v-1d41ced0="" class="col-4">
                                                                <button data-v-1d41ced0="" class="btn btn-secondary btn-block mb-2">1000</button>
                                                            </div>
                                                            <div data-v-1d41ced0="" class="col-4">
                                                                <button data-v-1d41ced0="" class="btn btn-secondary btn-block mb-2">250</button>
                                                            </div>
                                                            <div data-v-1d41ced0="" class="col-4">
                                                                <button data-v-1d41ced0="" class="btn btn-secondary btn-block mb-2">500</button>
                                                            </div>
                                                            <div data-v-1d41ced0="" class="col-4">
                                                                <button data-v-1d41ced0="" class="btn btn-secondary btn-block mb-2">1000</button>
                                                            </div>
                                                            <div data-v-1d41ced0="" class="col-4">
                                                                <button data-v-1d41ced0="" class="btn btn-secondary btn-block mb-2">200</button>
                                                            </div>
                                                            <div data-v-1d41ced0="" class="col-4">
                                                                <button data-v-1d41ced0="" class="btn btn-secondary btn-block mb-2">500</button>
                                                            </div>
                                                            <div data-v-1d41ced0="" class="col-4">
                                                                <button data-v-1d41ced0="" class="btn btn-secondary btn-block mb-2">1000</button>
                                                            </div>
                                                            <div data-v-1d41ced0="" class="col-4">
                                                                <button data-v-1d41ced0="" class="btn btn-secondary btn-block mb-2">250</button>
                                                            </div>
                                                        </div>
                                                        <div data-v-1d41ced0="" class="row row5 mt-2" style={{height: "18px"}}>
                                                            <div data-v-1d41ced0="" class="col-4"><span data-v-1d41ced0="">Northern Brave</span></div>
                                                            <div data-v-1d41ced0="" class="col-4 text-center text-success"><b data-v-1d41ced0=""><span data-v-1d41ced0=""  style={{color: "black"}}>0</span></b></div>
                                                            <div data-v-1d41ced0="" class="col-4 text-right"><span data-v-1d41ced0="" class="text-danger"><b data-v-1d41ced0="">0.00</b></span>
                                                                
                                                            </div>
                                                        </div>
                                                        <div data-v-1d41ced0="" class="row row5 mt-2"  style={{height: "18px"}}>
                                                            <div data-v-1d41ced0="" class="col-4"><span data-v-1d41ced0="">Canterbury</span></div>
                                                            <div data-v-1d41ced0="" class="col-4 text-center text-success"><b data-v-1d41ced0=""><span data-v-1d41ced0=""  style={{color: "black"}}>0</span></b></div>
                                                            <div data-v-1d41ced0="" class="col-4 text-right">
                                                                <span data-v-1d41ced0="" class="text-danger"><b data-v-1d41ced0="">0.00</b></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div><span tabindex="0"></span></div>
                            </div>
                            <div id="__BVID__272___BV_modal_backdrop_" class="modal-backdrop"></div>
                        </div>

                        {/* Modals End */}
                        {/* Bookmaker Info Modals Start */}
                        <div id="bookmaker" style={{display: "none", position: "absolute", zIndex: "1040"}}>
                            <div id="__BVID__34" role="dialog" aria-labelledby="__BVID__34___BV_modal_title_" aria-describedby="__BVID__34___BV_modal_body_" class="modal fade show" aria-modal="true" style={{display: "block", paddingLeft: "0px"}}>
                                <div class="modal-dialog modal-xl" style={{marginTop: "58px"}}><span tabindex="0"></span>
                                    <div role="document" id="__BVID__34___BV_modal_content_" tabindex="-1" class="modal-content">
                                        <header id="__BVID__34___BV_modal_header_" class="modal-header">
                                            <h5 id="__BVID__34___BV_modal_title_" class="modal-title">Bookmaker Rules</h5>
                                            <button type="button" aria-label="Close" class="close" onClick={() => hideInfoModal('event', 'bookmaker')}>×</button>
                                        </header>
                                        <div id="__BVID__34___BV_modal_body_" class="modal-body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                        <tr>
                                                            <td><span class="text-danger">Due to any reason any team will be getting advantage or disadvantage we are not concerned.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Company reserves the right to suspend/void any id/bets if the same is found to be illegitimate. For example incase of vpn/robot-use/multiple entry from same IP/ multiple bets at the same time (Punching) and others. Note : only winning bets will be voided.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">We will simply compare both teams 25 overs score higher score team will be declared winner in ODI (25 over comparison)</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">We will simply compare both teams 10 overs higher score team will be declared winner in T20 matches (10 over comparison)</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Any query about the result or rates should be contacted within 7 days of the specific event, the same will not be considered valid post 7 days from the event.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">If two team ends up with equal points, then result will be given based on the official point table</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Tennis:- Advance fancy market</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">If the second set is not completed all bets regarding this market will be voided</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">If a player retires after completion of second set, then the market will be settled as three sets</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Virtual Cricket</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">At any situation if the video gets interrupted/stopped then the same cannot be continued due to any technical issues bookmaker market will be voided</span></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        
                                    </div><span tabindex="0"></span></div>
                            </div>
                            <div id="__BVID__34___BV_modal_backdrop_" class="modal-backdrop"></div>
                        </div>
                        {/* bookmaker Info Models End */}
                        
                        {/* Matchodds Info Modals Start */}
                            
                        <div id="matchodds" style={{display: "none", position: "absolute", zIndex: "1040"}}>
	                        <div id="__BVID__42" role="dialog" aria-labelledby="__BVID__42___BV_modal_title_" aria-describedby="__BVID__42___BV_modal_body_" class="modal fade show" aria-modal="true" style={{display: "block"}}>
                                <div class="modal-dialog modal-xl" style={{marginTop: "58px"}}><span tabindex="0"></span>
                                    <div role="document" id="__BVID__42___BV_modal_content_" tabindex="-1" class="modal-content">
                                        <header id="__BVID__42___BV_modal_header_" class="modal-header">
                                            <h5 id="__BVID__42___BV_modal_title_" class="modal-title">Match Rules</h5>
                                            <button type="button" aria-label="Close" class="close" onClick={() => hideInfoModal('event', 'matchodds')}>×</button>
                                        </header>
                                        <div id="__BVID__42___BV_modal_body_" class="modal-body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <tr class="norecordsfound">
                                                        <td class="text-center">No records Found</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
				
			                        </div><span tabindex="0"></span></div>
                                </div>
                            <div id="__BVID__42___BV_modal_backdrop_" class="modal-backdrop"></div>
                        </div>



                        {/* Matchodds Info Modals End */}

                        {/* Sessionmarket Info Modals Start */}
                        <div id="sessionmarket" style={{display: "none" ,position: "absolute", zIndex: "1040"}}>
                            <div id="__BVID__68" role="dialog" aria-labelledby="__BVID__68___BV_modal_title_" aria-describedby="__BVID__68___BV_modal_body_" class="modal fade show" aria-modal="true" style={{display: "block", paddingLeft: "0px"}}>
                                <div class="modal-dialog modal-xl" style={{marginTop: "58px"}}><span tabindex="0"></span>
                                    <div role="document" id="__BVID__68___BV_modal_content_" tabindex="-1" class="modal-content">
                                        <header id="__BVID__68___BV_modal_header_" class="modal-header">
                                            <h5 id="__BVID__68___BV_modal_title_" class="modal-title">Fancy Rules</h5>
                                            <button type="button" aria-label="Close" class="close" onClick={() => hideInfoModal('event', 'sessionmarket')}>×</button>
                                        </header>
                                        <div id="__BVID__68___BV_modal_body_" class="modal-body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                        <tr>
                                                            <td><span class="text-danger">1. All fancy bets will be validated when match has been tied.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">2. All advance fancy will be suspended before toss or weather condition.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">3. In case technical error or any circumstances any fancy is suspended and does not resume result will be given all 
                            previous bets will be valid (based on haar/jeet).</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">4. If any case wrong rate has been given in fancy that particular bets will be cancelled.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">5. In any circumstances management decision will be final related to all exchange items. Our scorecard will be considered as valid if there is any mismatch in online portal</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">6. In case customer make bets in wrong fancy we are not liable to delete, no changes will be made and bets will be consider as confirm bet.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">7. Due to any technical error market is open and result has came all bets after result will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">8. Manual bets are not accepted in our exchange</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">9.Our exchange will provide 5 second delay in our tv.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">10. Company reserves the right to suspend/void any id/bets if the same is found to be illegitimate. For example incase of vpn/robot-use/multiple entry from same IP/ multiple bets at same time (Punching) and others. Note : only winning bets will be voided, For example: If we find such entries (above mentioned) from any id and their bets are (200000 lay in a 6 over session for the rate 40 and 200000 back for the rate of 48) and the actual score is 38, bets of 40 lay will be voided and the bets for 48 back will be considered valid.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">11. Company reserves the right to void any bets (only winning bets) of any event at any point of the match if the company believes there is any cheating/wrong doing in that particular event by the players (either batsman/bowler)</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">12. Once our exchange give username and password it is your responsibility to change a 
                            password.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">13. Penalty runs will not be counted in any fancy.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">14. Warning:- live scores and other data on this site is sourced from third party feeds and may be subject to time delays and/or be inaccurate. If you rely on this data to place bets, you do so at your own risk. Our exchange does not accept responsibility for loss suffered as a result of reliance on this data.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">15.Traders will be block the user ID if find any misinterpret activities, No queries accept regarding.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">16. Our exchange is not responsible for misuse of client id.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Test</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">1 Session:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">1.1 Complete session valid in test.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">1.2 Session is not completed for ex:- India 60 over run session Ind is running in case India team declares or all-out at 55 over next 5 over session will be continue in England inning.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">1.3 1st day 1st session run minimum 25 over  will be played then result is given otherwise 1st day 1st session will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">1.4 1st day 2nd session run minimum 25 over  will be played then result is given otherwise 1st day 2nd session will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">1.5 1st day total run minimum 80 over  will be played then result is given otherwise 1st day total run will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">1.6 Test match both advance session is valid.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">2 Test lambi/ Inning run:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">2.1 Mandatory 70 over played in test lambi paari/ Innings run. If any team all-out or declaration lambi paari/ innings run is valid.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">2.2 In case due to weather situation match has been stopped all lambi trades will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">2.3 In test both lambi paari / inning run is valid in advance fancy.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">3 Test batsman:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">3.1 In case batsmen is injured he/she is made 34 runs the result will be given 34 runs.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">3.2 Batsman 50/100 run if batsman is injured or declaration the result will be given on particular run.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">3.3 In next men out fancy if player is injured particular fancy will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">3.4 In advance fancy opening batsmen is only valid if same batsmen came in opening the fancy will be valid in case one batsmen is changed that particular player fancy will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">3.5 Test match both advance fancy batsmen run is valid.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">4 Test partnership:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">4.1 In partnership one batsman is injured partnership is continued in next batsman.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">4.2 Partnership and player runs due to weather condition or match abandoned the result will be given as per score.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">4.3 Advance partnership is valid in case both players are different or same.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">4.4 Test match both advance fancy partnership is valid.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">5 Other fancy advance (test):-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">5.1 Four, sixes, wide, wicket,  extra run, total run, highest over and top batsmen is valid only if 300 overs has been played or the match has been won by any team otherwise all these fancy will be deleted. Additionally all events are valid only for 1st innings( this is applicable to all individual team events also)</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">2 Odi rule:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Session:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Match 1st over run advance fancy only 1st innings run will be counted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Complete session is valid in case due to rain or match abandoned particular session will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">For example:- 35 over run team a is playing any case team A  is all-out in 33 over team a has made 150 run the session result is validated on particular run.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Advance session is valid in only 1st innings.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">50 over runs:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">In case 50 over is not completed all bet will be deleted due to weather or any condition.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Advance 50 over runs is valid in only 1st innings.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Odi batsman runs:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">In case batsman is injured he/she is made 34 runs the result will be given 34 runs.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">In next men out fancy if player is injured particular fancy will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">In advance fancy opening batsmen is only valid if same batsmen came in opening the fancy will be valid in case one batsmen is changed that particular player fancy will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Odi partnership runs:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">In partnership one batsman is injured partnership is continued in next batsman.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Advance partnership is valid in case both players are different or same.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Both team advance partnerships are valid in particular match.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Other fancy:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Four, sixes, wide, wicket, extra run, total run, highest over ,top batsman,maiden over,caught-out,no-ball,run-out,fifty and century are valid only match has been completed in case due to rain over has been reduced all other fancy will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">T20:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Session:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Match 1st over run advance fancy only 1st innings run will be counted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Complete session is valid in case due to rain or match abandoned particular session will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">For example :- 15 over run team a is playing any case team a  is all-out in 13 over team A has made 100 run the session result is validated on particular run.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Advance session is valid in only 1st innings.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">20 over runs:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Advance 20 over run is valid only in 1st innings. 20 over run will not be considered as valid if 20 overs is not completed due to any situation</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">T20 batsman runs:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">In case batsman is injured he/she is made 34 runs the result will be given 34 runs.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">In next men out fancy if player is injured particular fancy will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">In advance fancy opening batsmen is only valid if same batsmen came in opening the fancy will be valid in case one batsmen is changed that particular player fancy will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">T20 partnership runs:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">In partnership one batsman is injured partnership is continued in next batsman.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Advance partnership is valid in case both players are different or same.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Both team advance partnerships are valid in particular match.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">1st 2 &amp; 3 Wickets runs:- T20/ODI</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Advance event is valid in only 1st Innings.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">If over reduced due to rain or weather condition or match abandoned the result will be given as per score.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Other fancy:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">T-20 ,one day and test match in case current innings player and partnership are running in between match has been called off or abandoned that situation all current player and partnership results are valid.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Four, sixes, wide, wicket, extra run, total run, highest over and top batsman,maiden over,caught-out,no-ball,run-out,fifty and century are valid only match has been completed in case due to rain over has been reduced all other fancy will be deleted.
                        1st 6 over dot ball and  20 over dot ball fancy only valid is 1st innings.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">1st wicket lost to any team balls meaning that any team 1st wicket fall down in how many balls that particular fancy at least minimum one ball have to be played otherwise bets will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">1st wicket lost to any team fancy valid both innings.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">How many balls for 50 runs any team meaning that any team achieved 50 runs in how many balls that particular fancy at least one ball have to be played otherwise that fancy bets will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">How many balls for 50 runs fancy any team only first inning valid.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">1st 6 inning boundaries runs any team fancy will be counting only according to run scored fours and sixes at least 6 over must be played otherwise that fancy will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">1st inning 6 over boundaries runs any team run like wide ,no-ball ,leg-byes ,byes and over throw runs are not counted this fancy.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">How many balls face any batsman meaning that any batsman how many balls he/she played that particular fancy at least one ball have to be played otherwise that fancy bets will be deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">How many balls face by any batsman both innings valid.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Lowest scoring over will be considered valid only if the over is completed fully (all six deliveries has to be bowled)</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Concussion in Test:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">All bets of one over session will be deleted in test scenario, in case session is incomplete. For example innings declared or match suspended to bad light or any other conditions.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">1. All bets will be considered as valid if a player has been replaced under concussion substitute, result will be given for the runs scored by the mentioned player. For example DM Bravo gets retired hurt at 23 runs, then result will be given for 23.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">2. Bets of both the player will be valid under concussion substitute.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Total Match- Events (test):-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Minimum of 300 overs to be bowled in the entire test match, otherwise all bets related to the particular event will get void. For example, Total match caught outs will be valid only if 300 overs been bowled in the particular test match</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Limited over events-Test:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">This event will be considered valid only if the number of overs defined on the particular event has been bowled, otherwise all bets related to this event will get void. For example 0-25 over events will be valid only if 25 overs has been bowled, else the same will not be considered as valid</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">If the team gets all out prior to any of the defined overs, then balance overs will be counted in next innings. For example if the team gets all out in 23.1 over the same will be considered as 24 overs and the balance overs will be counted from next innings.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Bowler Wicket event's- Test:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Minimum of one legal over (one complete over) has to be bowled by the bowler mentioned in the event, else the same will not be considered as valid</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Bowler over events- Test:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">The mentioned bowler has to complete the defined number of overs, else the bets related to that particular event will get void. For example if the mentioned bowler has bowled 8 overs, then 5 over run of that particular bowler will be considered as valid and the 10 over run will get void</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Player ball event's- Test:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">This event will be considered valid only if the defined number of runs made by the mentioned player, else the result will be considered as 0 (zero) balls</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">For example if Root makes 20 runs in 60 balls and gets out on 22 runs, result for 20 runs will be 60 balls and the result for balls required for 25 run Root will be considered as 0 (Zero) and the same will be given as result</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Limited over events-ODI:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">This event will be considered valid only if the number of overs defined on the particular event has been bowled, otherwise all bets related to this event will get void. 0-50 over events will be valid only if 50 over completed, if the team batting first get all out prior to 50 over the balance over will be counted from second innings. For example if team batting first gets all out in 35 over balance 15 over will be counted from second innings, the same applies for all events if team gets all out before the defined number of overs</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">The events which remains incomplete will be voided if over gets reduced in the match due to any situation, for example if match interrupted in 15 overs due to rain/badlight and post this over gets reduced. Events for 0-10 will be valid, all other events related to this type will get deleted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">This events will be valid only if the defined number of over is completed. For example team batting first gets all out in 29.4 over then the same will be considered as 30 over, the team batting second must complete 20 overs only then 0-50 over events will be considered as valid. In case team batting second gets all out in 19.4 over then 0-50 over event will not be considered as valid, This same is valid for 1st Innings only.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Bowler event- ODI:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">The mentioned bowler has to complete the defined number of overs, else the bets related to that particular event will get void. For example if the mentioned bowler has bowled 8 overs, then 5 over run of that particular bowler will be considered as valid and the 10 over run will get void</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Both innings are valid</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Other event:- T20</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">The events for 1-10 over and 11-20 over will be considered valid only if the number of over mentioned has been played completely. However if the over got reduced before the particular event then the same will be voided,
                        if the team batting first get all out prior to 20 over the balance over will be counted from second innings. For example if team batting first gets all out in 17 over balance 3 over will be counted from second innings.
                        This same is valid for 1st Innings only.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">If over got reduced in between any running event, then the same will be considered valid and the rest will be voided. For example.., match started and due to rain/bad light or any other situation match got interrupted at 4 over and later over got reduced. Then events for 1-10 is valid rest all will be voided</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Bowler Session: This event is valid only if the bowler has completed his maximum quota of overs, else the same will be voided. However if the match has resulted and the particular bowler has already started bowling his final over then result will be given even if he haven't completed the over. For example B Kumar is bowling his final over and at 3.4 the match has resulted then result will be given for B Kumar over runs</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Incase of DLS, the over got reduced then the bowler who has already bowled his maximum quota of over that result will be considered as valid and the rest will be voided</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">12. Player and partnership are valid only 14 matches.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Boundary on Match 1st Free hit</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Both innings are valid</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Boundary hit on Free hit only be considered as valid</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Bets will be deleted if there is no Free hit in the mentioned match</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Boundary by bat will be considered as valid</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Boundaries by Player</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Both Four and six are valid</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Any query regarding result or rate has to be contacted within 7 days from the event, query after 7 days from the event will not be considered as valid</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Virtual Cricket</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Scorecard available on the video will be considered as valid. At any situation, if there is a difference between the scorecard in the website and the scorecard available on video. Score card available on video will be valid</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">In case of any technical issues the video gets interrupted/stopped and the same cannot be continued, the existing markets will be voided. However the markets which are already finished/settled will remain valid.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">CPL:-</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">If CPL fixture 0f 33 matches gets reduced due to any reason, then all the special fancies will be voided (Match abandoned due to rain/bad light will not be considered in this)</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Fancy based on all individual teams are valid only for league stage</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Total 1st over runs: Average 6 runs will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Total fours: Average 22 fours will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Total sixes: Average 13 sixes will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Total Wickets - Average will 13 Wickets be given in case match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Total Wides - Average 10 wides will be given in case match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Total Extras - Average 18 extras will be given in case match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Total No ball - Average 1 no ball will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Total Fifties - Average 1 fifties will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Total Caught outs: Average 9 caught out will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">At any situation if result is given for any particular event based on the rates given for the same, then the particular result will be considered valid, similarly if the tournament gets canceled due to any reason the previously given result will be considered valid</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Management decision will be final</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Highest innings run - Only first innings is valid</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Lowest innings run - Only first innings is valid</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Highest over run: Both innings are valid</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Highest 1st over run in individual match: Both innings are valid, however for CPL we have created the fancy for 1st innings only</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Highest Fours in individual match: Both innings are valid</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Highest Sixes in individual match: Both innings are valid</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Highest Extras in individual match: Both innings are valid</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Highest Wicket in individual match: Both innings are valid</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Super over will not be included</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Barbados Tridents</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Opening partnership run: Average 24 runs will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">First 6 over run: Average 45 run will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">St Kitts and Nevis Patriots</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Opening partnership run: Average 25 runs will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">First 6 over run: Average 45 run will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Trinbago Knight Riders</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Opening partnership run: Average 22 runs will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">First 6 over run: Average 46 run will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Guyana Amazon Warriors</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Opening partnership run: Average 23 runs will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">First 6 over run: Average 44 run will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">St Lucia Zouks</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Opening partnership run: Average 22 runs will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">First 6 over run: Average 43 run will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="text-danger">Jamaica Tallawahs</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">Opening partnership run: Average 24 runs will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="">First 6 over run: Average 46 run will be given in case  match abandoned or over reduced</span></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        
                                    </div><span tabindex="0"></span></div>
                            </div>
                            <div id="__BVID__68___BV_modal_backdrop_" class="modal-backdrop"></div>
                        </div>
                            
                        {/* Sessionmarket Info Modals End */}
                        </>
                        :
                        null
                    }
                    {isMobile ?
                        <div id="msgBox" className="message-wrap-full success to-open_bets" style={{ display: 'none' }}>
                            <div className="message">
                                <h4 id="header">Bet Matched</h4>
                                <p id="info">
                                    <span id="sideType" className="back_notify">Yes</span>
                                    <strong id="selectionName">Match 1st Over</strong> <strong id="stake_notify"> 100.00</strong> at odds <strong id="price_notify_value">7/100</strong>
                                </p>
                            </div>
                            <Link id="close" className="close ui-link" href="#">Close</Link>
                        </div> : null
                    }
                    <Grid item lg={2} spacing={2} className="xs-none">
                        <LeftSideBar />
                    </Grid>
                    {/* End Main Item Grid */}
                    <Grid item lg={3} xs={12} spacing={2} className="betSlipGrid xs-none">
                        <BetSlip />
                    </Grid>
                </Grid>
            </div>
        </>

    )
}
export default FullMarket;

