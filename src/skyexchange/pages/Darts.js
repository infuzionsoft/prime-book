import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { List, ListItemText, ListItem, Tabs, Button } from '@material-ui/core';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Grid, AppBar, InputLabel } from '@material-ui/core';
import moment from 'moment';
import Appconfig from "../config/config";
import Footer from "../includes/Footer";
import axios from "axios";

import { isMobile } from "react-device-detect";

import Header from '../includes/Header';
import AccountSidebar from '../includes/AccountSidebar';

const gridType = isMobile ? 10 : 8;
export default function Darts() {
    return (
        <>
       
         {isMobile ? 
            <>
            <meta name="viewport" content="width=1300" />
            <Header /> 
            <Grid className="account-stat" container spacing={0}>
                <Grid item lg={gridType} xs={12} spacing={2}>
                    {/* <div className="helloaccount">
                        Hello
                    </div> */}
  <div class="report-container">
	 
	<div class="card" style={{border: "none"}}>
		<div class="card-header">
			<h4 class="mb-0">Profit Loss</h4></div>
		<div class="card-body container-fluid container-fluid-5">
			<div class="row row5">
				<div class="col-6">
					<div class="form-group mb-0">
						<div class="mx-datepicker" not-before="Thu Nov 18 2021 05:30:00 GMT+0530 (India Standard Time)" not-after="Sat Dec 18 2021 05:30:00 GMT+0530 (India Standard Time)"  style={{width: "auto"}}>
							<div class="mx-input-wrapper">
								<input name="date" type="text" autocomplete="off" placeholder="Select Date" class="mx-input"/> 
                                <span class="mx-input-append mx-clear-wrapper">
                                    <i class="mx-input-icon mx-clear-icon"></i>
                                </span> 
                                <span class="mx-input-append">
                                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 200 200" class="mx-calendar-icon">
                                        <rect x="13" y="29" rx="14" ry="14" width="174" height="158" fill="transparent"></rect> 
                                        <line x1="46" x2="46" y1="8" y2="50"></line> 
                                        <line x1="154" x2="154" y1="8" y2="50"></line> 
                                        <line x1="13" x2="187" y1="70" y2="70"></line> 
                                        <text x="50%" y="135" font-size="90" stroke-width="1" text-anchor="middle" dominant-baseline="middle"></text>
                                    </svg>
                                </span>
                            </div>
							<div class="mx-datepicker-popup"  style={{display: "none"}}>
								 
								<div class="mx-calendar mx-calendar-panel-none">
									<div class="mx-calendar-header">
                                        <a class="mx-icon-last-year">«</a> 
                                        <a class="mx-icon-last-month"  style={{display: "none"}}>‹</a> 
                                        <a class="mx-icon-next-year">»</a> 
                                        <a class="mx-icon-next-month"  style={{display: "none"}}>›</a> 
                                        <a class="mx-current-month"  style={{display: "none"}}>Dec</a> 
                                        <a class="mx-current-year"  style={{display: "none"}}>2021</a> 
                                        <a class="mx-current-year"  style={{display: "none"}}>2020 ~ 2029</a> 
                                        <a class="mx-time-header"  style={{display: "none"}}>2021-12-11</a>
                                    </div>
									<div class="mx-calendar-content">
										<table class="mx-panel mx-panel-date"  style={{display: "none"}}>
											<thead>
												<tr>
													<th>Sun</th>
													<th>Mon</th>
													<th>Tue</th>
													<th>Wed</th>
													<th>Thu</th>
													<th>Fri</th>
													<th>Sat</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td data-year="2021" data-month="10" title="2021-11-28" class="cell last-month">28</td>
													<td data-year="2021" data-month="10" title="2021-11-29" class="cell last-month">29</td>
													<td data-year="2021" data-month="10" title="2021-11-30" class="cell last-month">30</td>
													<td data-year="2021" data-month="11" title="2021-12-01" class="cell cur-month">1</td>
													<td data-year="2021" data-month="11" title="2021-12-02" class="cell cur-month">2</td>
													<td data-year="2021" data-month="11" title="2021-12-03" class="cell cur-month">3</td>
													<td data-year="2021" data-month="11" title="2021-12-04" class="cell cur-month">4</td>
												</tr>
												<tr>
													<td data-year="2021" data-month="11" title="2021-12-05" class="cell cur-month">5</td>
													<td data-year="2021" data-month="11" title="2021-12-06" class="cell cur-month">6</td>
													<td data-year="2021" data-month="11" title="2021-12-07" class="cell cur-month">7</td>
													<td data-year="2021" data-month="11" title="2021-12-08" class="cell cur-month">8</td>
													<td data-year="2021" data-month="11" title="2021-12-09" class="cell cur-month">9</td>
													<td data-year="2021" data-month="11" title="2021-12-10" class="cell cur-month">10</td>
													<td data-year="2021" data-month="11" title="2021-12-11" class="cell cur-month actived">11</td>
												</tr>
												<tr>
													<td data-year="2021" data-month="11" title="2021-12-12" class="cell cur-month">12</td>
													<td data-year="2021" data-month="11" title="2021-12-13" class="cell cur-month">13</td>
													<td data-year="2021" data-month="11" title="2021-12-14" class="cell cur-month">14</td>
													<td data-year="2021" data-month="11" title="2021-12-15" class="cell cur-month">15</td>
													<td data-year="2021" data-month="11" title="2021-12-16" class="cell cur-month">16</td>
													<td data-year="2021" data-month="11" title="2021-12-17" class="cell cur-month">17</td>
													<td data-year="2021" data-month="11" title="2021-12-18" class="cell cur-month today">18</td>
												</tr>
												<tr>
													<td data-year="2021" data-month="11" title="2021-12-19" class="cell cur-month disabled">19</td>
													<td data-year="2021" data-month="11" title="2021-12-20" class="cell cur-month disabled">20</td>
													<td data-year="2021" data-month="11" title="2021-12-21" class="cell cur-month disabled">21</td>
													<td data-year="2021" data-month="11" title="2021-12-22" class="cell cur-month disabled">22</td>
													<td data-year="2021" data-month="11" title="2021-12-23" class="cell cur-month disabled">23</td>
													<td data-year="2021" data-month="11" title="2021-12-24" class="cell cur-month disabled">24</td>
													<td data-year="2021" data-month="11" title="2021-12-25" class="cell cur-month disabled">25</td>
												</tr>
												<tr>
													<td data-year="2021" data-month="11" title="2021-12-26" class="cell cur-month disabled">26</td>
													<td data-year="2021" data-month="11" title="2021-12-27" class="cell cur-month disabled">27</td>
													<td data-year="2021" data-month="11" title="2021-12-28" class="cell cur-month disabled">28</td>
													<td data-year="2021" data-month="11" title="2021-12-29" class="cell cur-month disabled">29</td>
													<td data-year="2021" data-month="11" title="2021-12-30" class="cell cur-month disabled">30</td>
													<td data-year="2021" data-month="11" title="2021-12-31" class="cell cur-month disabled">31</td>
													<td data-year="2021" data-month="12" title="2022-01-01" class="cell next-month disabled">1</td>
												</tr>
												<tr>
													<td data-year="2021" data-month="12" title="2022-01-02" class="cell next-month disabled">2</td>
													<td data-year="2021" data-month="12" title="2022-01-03" class="cell next-month disabled">3</td>
													<td data-year="2021" data-month="12" title="2022-01-04" class="cell next-month disabled">4</td>
													<td data-year="2021" data-month="12" title="2022-01-05" class="cell next-month disabled">5</td>
													<td data-year="2021" data-month="12" title="2022-01-06" class="cell next-month disabled">6</td>
													<td data-year="2021" data-month="12" title="2022-01-07" class="cell next-month disabled">7</td>
													<td data-year="2021" data-month="12" title="2022-01-08" class="cell next-month disabled">8</td>
												</tr>
											</tbody>
										</table>
										<div class="mx-panel mx-panel-year"  style={{display: "none"}}><span class="cell disabled">2020</span><span class="cell actived">2021</span><span class="cell disabled">2022</span><span class="cell disabled">2023</span><span class="cell disabled">2024</span><span class="cell disabled">2025</span><span class="cell disabled">2026</span><span class="cell disabled">2027</span><span class="cell disabled">2028</span><span class="cell disabled">2029</span></div>
										<div class="mx-panel mx-panel-month"  style={{display: "none"}}><span class="cell disabled">Jan</span><span class="cell disabled">Feb</span><span class="cell disabled">Mar</span><span class="cell disabled">Apr</span><span class="cell disabled">May</span><span class="cell disabled">Jun</span><span class="cell disabled">Jul</span><span class="cell disabled">Aug</span><span class="cell disabled">Sep</span><span class="cell disabled">Oct</span><span class="cell">Nov</span><span class="cell actived">Dec</span></div>
										<div class="mx-panel mx-panel-time"  style={{display: "none"}}>
											<ul class="mx-time-list"  style={{width: "33.3333%"}}>
												<li class="cell">00</li>
												<li class="cell">01</li>
												<li class="cell">02</li>
												<li class="cell">03</li>
												<li class="cell">04</li>
												<li class="cell actived">05</li>
												<li class="cell">06</li>
												<li class="cell">07</li>
												<li class="cell">08</li>
												<li class="cell">09</li>
												<li class="cell">10</li>
												<li class="cell">11</li>
												<li class="cell">12</li>
												<li class="cell">13</li>
												<li class="cell">14</li>
												<li class="cell">15</li>
												<li class="cell">16</li>
												<li class="cell">17</li>
												<li class="cell">18</li>
												<li class="cell">19</li>
												<li class="cell">20</li>
												<li class="cell">21</li>
												<li class="cell">22</li>
												<li class="cell">23</li>
											</ul>
											<ul class="mx-time-list"  style={{width: "33.3333%"}}>
												<li class="cell">00</li>
												<li class="cell">01</li>
												<li class="cell">02</li>
												<li class="cell">03</li>
												<li class="cell">04</li>
												<li class="cell">05</li>
												<li class="cell">06</li>
												<li class="cell">07</li>
												<li class="cell">08</li>
												<li class="cell">09</li>
												<li class="cell">10</li>
												<li class="cell">11</li>
												<li class="cell">12</li>
												<li class="cell">13</li>
												<li class="cell">14</li>
												<li class="cell">15</li>
												<li class="cell">16</li>
												<li class="cell">17</li>
												<li class="cell">18</li>
												<li class="cell">19</li>
												<li class="cell">20</li>
												<li class="cell">21</li>
												<li class="cell">22</li>
												<li class="cell">23</li>
												<li class="cell">24</li>
												<li class="cell">25</li>
												<li class="cell">26</li>
												<li class="cell">27</li>
												<li class="cell">28</li>
												<li class="cell">29</li>
												<li class="cell actived">30</li>
												<li class="cell">31</li>
												<li class="cell">32</li>
												<li class="cell">33</li>
												<li class="cell">34</li>
												<li class="cell">35</li>
												<li class="cell">36</li>
												<li class="cell">37</li>
												<li class="cell">38</li>
												<li class="cell">39</li>
												<li class="cell">40</li>
												<li class="cell">41</li>
												<li class="cell">42</li>
												<li class="cell">43</li>
												<li class="cell">44</li>
												<li class="cell">45</li>
												<li class="cell">46</li>
												<li class="cell">47</li>
												<li class="cell">48</li>
												<li class="cell">49</li>
												<li class="cell">50</li>
												<li class="cell">51</li>
												<li class="cell">52</li>
												<li class="cell">53</li>
												<li class="cell">54</li>
												<li class="cell">55</li>
												<li class="cell">56</li>
												<li class="cell">57</li>
												<li class="cell">58</li>
												<li class="cell">59</li>
											</ul>
											<ul class="mx-time-list"  style={{width: "33.3333%"}}>
												<li class="cell actived">00</li>
												<li class="cell">01</li>
												<li class="cell">02</li>
												<li class="cell">03</li>
												<li class="cell">04</li>
												<li class="cell">05</li>
												<li class="cell">06</li>
												<li class="cell">07</li>
												<li class="cell">08</li>
												<li class="cell">09</li>
												<li class="cell">10</li>
												<li class="cell">11</li>
												<li class="cell">12</li>
												<li class="cell">13</li>
												<li class="cell">14</li>
												<li class="cell">15</li>
												<li class="cell">16</li>
												<li class="cell">17</li>
												<li class="cell">18</li>
												<li class="cell">19</li>
												<li class="cell">20</li>
												<li class="cell">21</li>
												<li class="cell">22</li>
												<li class="cell">23</li>
												<li class="cell">24</li>
												<li class="cell">25</li>
												<li class="cell">26</li>
												<li class="cell">27</li>
												<li class="cell">28</li>
												<li class="cell">29</li>
												<li class="cell">30</li>
												<li class="cell">31</li>
												<li class="cell">32</li>
												<li class="cell">33</li>
												<li class="cell">34</li>
												<li class="cell">35</li>
												<li class="cell">36</li>
												<li class="cell">37</li>
												<li class="cell">38</li>
												<li class="cell">39</li>
												<li class="cell">40</li>
												<li class="cell">41</li>
												<li class="cell">42</li>
												<li class="cell">43</li>
												<li class="cell">44</li>
												<li class="cell">45</li>
												<li class="cell">46</li>
												<li class="cell">47</li>
												<li class="cell">48</li>
												<li class="cell">49</li>
												<li class="cell">50</li>
												<li class="cell">51</li>
												<li class="cell">52</li>
												<li class="cell">53</li>
												<li class="cell">54</li>
												<li class="cell">55</li>
												<li class="cell">56</li>
												<li class="cell">57</li>
												<li class="cell">58</li>
												<li class="cell">59</li>
											</ul>
										</div>
									</div>
								</div>
								 
							</div>
						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group mb-0">
						<div class="mx-datepicker" not-before="Thu Nov 18 2021 05:30:00 GMT+0530 (India Standard Time)" not-after="Sat Dec 18 2021 05:30:00 GMT+0530 (India Standard Time)"  style={{width: "auto"}}>
							<div class="mx-input-wrapper">
								<input name="date" type="text" autocomplete="off" placeholder="Select Date" class="mx-input"/> 
                                <span class="mx-input-append mx-clear-wrapper">
                                    <i class="mx-input-icon mx-clear-icon"></i>
                                </span> 
                                <span class="mx-input-append">
                                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 200 200" class="mx-calendar-icon">
                                        <rect x="13" y="29" rx="14" ry="14" width="174" height="158" fill="transparent"></rect> 
                                        <line x1="46" x2="46" y1="8" y2="50"></line> 
                                        <line x1="154" x2="154" y1="8" y2="50"></line> 
                                        <line x1="13" x2="187" y1="70" y2="70"></line> 
                                        <text x="50%" y="135" font-size="90" stroke-width="1" text-anchor="middle" dominant-baseline="middle"></text>
                                    </svg>
                                </span>
                            </div>
							<div class="mx-datepicker-popup"  style={{display: "none"}}>
								 
								<div class="mx-calendar mx-calendar-panel-none">
									<div class="mx-calendar-header">
                                        <a class="mx-icon-last-year">«</a> 
                                        <a class="mx-icon-last-month"  style={{display: "none"}}>‹</a> 
                                        <a class="mx-icon-next-year">»</a> 
                                        <a class="mx-icon-next-month"  style={{display: "none"}}>›</a> 
                                        <a class="mx-current-month"  style={{display: "none"}}>Dec</a> 
                                        <a class="mx-current-year"  style={{display: "none"}}>2021</a> 
                                        <a class="mx-current-year"  style={{display: "none"}}>2020 ~ 2029</a> 
                                        <a class="mx-time-header"  style={{display: "none"}}>2021-12-18</a>
                                    </div>
									<div class="mx-calendar-content">
										<table class="mx-panel mx-panel-date"  style={{display: "none"}}>
											<thead>
												<tr>
													<th>Sun</th>
													<th>Mon</th>
													<th>Tue</th>
													<th>Wed</th>
													<th>Thu</th>
													<th>Fri</th>
													<th>Sat</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td data-year="2021" data-month="10" title="2021-11-28" class="cell last-month">28</td>
													<td data-year="2021" data-month="10" title="2021-11-29" class="cell last-month">29</td>
													<td data-year="2021" data-month="10" title="2021-11-30" class="cell last-month">30</td>
													<td data-year="2021" data-month="11" title="2021-12-01" class="cell cur-month">1</td>
													<td data-year="2021" data-month="11" title="2021-12-02" class="cell cur-month">2</td>
													<td data-year="2021" data-month="11" title="2021-12-03" class="cell cur-month">3</td>
													<td data-year="2021" data-month="11" title="2021-12-04" class="cell cur-month">4</td>
												</tr>
												<tr>
													<td data-year="2021" data-month="11" title="2021-12-05" class="cell cur-month">5</td>
													<td data-year="2021" data-month="11" title="2021-12-06" class="cell cur-month">6</td>
													<td data-year="2021" data-month="11" title="2021-12-07" class="cell cur-month">7</td>
													<td data-year="2021" data-month="11" title="2021-12-08" class="cell cur-month">8</td>
													<td data-year="2021" data-month="11" title="2021-12-09" class="cell cur-month">9</td>
													<td data-year="2021" data-month="11" title="2021-12-10" class="cell cur-month">10</td>
													<td data-year="2021" data-month="11" title="2021-12-11" class="cell cur-month">11</td>
												</tr>
												<tr>
													<td data-year="2021" data-month="11" title="2021-12-12" class="cell cur-month">12</td>
													<td data-year="2021" data-month="11" title="2021-12-13" class="cell cur-month">13</td>
													<td data-year="2021" data-month="11" title="2021-12-14" class="cell cur-month">14</td>
													<td data-year="2021" data-month="11" title="2021-12-15" class="cell cur-month">15</td>
													<td data-year="2021" data-month="11" title="2021-12-16" class="cell cur-month">16</td>
													<td data-year="2021" data-month="11" title="2021-12-17" class="cell cur-month">17</td>
													<td data-year="2021" data-month="11" title="2021-12-18" class="cell cur-month today actived">18</td>
												</tr>
												<tr>
													<td data-year="2021" data-month="11" title="2021-12-19" class="cell cur-month disabled">19</td>
													<td data-year="2021" data-month="11" title="2021-12-20" class="cell cur-month disabled">20</td>
													<td data-year="2021" data-month="11" title="2021-12-21" class="cell cur-month disabled">21</td>
													<td data-year="2021" data-month="11" title="2021-12-22" class="cell cur-month disabled">22</td>
													<td data-year="2021" data-month="11" title="2021-12-23" class="cell cur-month disabled">23</td>
													<td data-year="2021" data-month="11" title="2021-12-24" class="cell cur-month disabled">24</td>
													<td data-year="2021" data-month="11" title="2021-12-25" class="cell cur-month disabled">25</td>
												</tr>
												<tr>
													<td data-year="2021" data-month="11" title="2021-12-26" class="cell cur-month disabled">26</td>
													<td data-year="2021" data-month="11" title="2021-12-27" class="cell cur-month disabled">27</td>
													<td data-year="2021" data-month="11" title="2021-12-28" class="cell cur-month disabled">28</td>
													<td data-year="2021" data-month="11" title="2021-12-29" class="cell cur-month disabled">29</td>
													<td data-year="2021" data-month="11" title="2021-12-30" class="cell cur-month disabled">30</td>
													<td data-year="2021" data-month="11" title="2021-12-31" class="cell cur-month disabled">31</td>
													<td data-year="2021" data-month="12" title="2022-01-01" class="cell next-month disabled">1</td>
												</tr>
												<tr>
													<td data-year="2021" data-month="12" title="2022-01-02" class="cell next-month disabled">2</td>
													<td data-year="2021" data-month="12" title="2022-01-03" class="cell next-month disabled">3</td>
													<td data-year="2021" data-month="12" title="2022-01-04" class="cell next-month disabled">4</td>
													<td data-year="2021" data-month="12" title="2022-01-05" class="cell next-month disabled">5</td>
													<td data-year="2021" data-month="12" title="2022-01-06" class="cell next-month disabled">6</td>
													<td data-year="2021" data-month="12" title="2022-01-07" class="cell next-month disabled">7</td>
													<td data-year="2021" data-month="12" title="2022-01-08" class="cell next-month disabled">8</td>
												</tr>
											</tbody>
										</table>
										<div class="mx-panel mx-panel-year"  style={{display: "none"}}><span class="cell disabled">2020</span><span class="cell actived">2021</span><span class="cell disabled">2022</span><span class="cell disabled">2023</span><span class="cell disabled">2024</span><span class="cell disabled">2025</span><span class="cell disabled">2026</span><span class="cell disabled">2027</span><span class="cell disabled">2028</span><span class="cell disabled">2029</span></div>
										<div class="mx-panel mx-panel-month"  style={{display: "none"}}><span class="cell disabled">Jan</span><span class="cell disabled">Feb</span><span class="cell disabled">Mar</span><span class="cell disabled">Apr</span><span class="cell disabled">May</span><span class="cell disabled">Jun</span><span class="cell disabled">Jul</span><span class="cell disabled">Aug</span><span class="cell disabled">Sep</span><span class="cell disabled">Oct</span><span class="cell">Nov</span><span class="cell actived">Dec</span></div>
										<div class="mx-panel mx-panel-time"  style={{display: "none"}}>
											<ul class="mx-time-list"  style={{width: "33.3333%"}}>
												<li class="cell disabled">00</li>
												<li class="cell disabled">01</li>
												<li class="cell disabled">02</li>
												<li class="cell disabled">03</li>
												<li class="cell disabled">04</li>
												<li class="cell actived disabled">05</li>
												<li class="cell disabled">06</li>
												<li class="cell disabled">07</li>
												<li class="cell disabled">08</li>
												<li class="cell disabled">09</li>
												<li class="cell disabled">10</li>
												<li class="cell disabled">11</li>
												<li class="cell disabled">12</li>
												<li class="cell disabled">13</li>
												<li class="cell disabled">14</li>
												<li class="cell disabled">15</li>
												<li class="cell disabled">16</li>
												<li class="cell disabled">17</li>
												<li class="cell disabled">18</li>
												<li class="cell disabled">19</li>
												<li class="cell disabled">20</li>
												<li class="cell disabled">21</li>
												<li class="cell disabled">22</li>
												<li class="cell disabled">23</li>
											</ul>
											<ul class="mx-time-list"  style={{width: "33.3333%"}}>
												<li class="cell disabled">00</li>
												<li class="cell disabled">01</li>
												<li class="cell disabled">02</li>
												<li class="cell disabled">03</li>
												<li class="cell disabled">04</li>
												<li class="cell disabled">05</li>
												<li class="cell disabled">06</li>
												<li class="cell disabled">07</li>
												<li class="cell disabled">08</li>
												<li class="cell disabled">09</li>
												<li class="cell disabled">10</li>
												<li class="cell disabled">11</li>
												<li class="cell disabled">12</li>
												<li class="cell disabled">13</li>
												<li class="cell disabled">14</li>
												<li class="cell disabled">15</li>
												<li class="cell disabled">16</li>
												<li class="cell disabled">17</li>
												<li class="cell disabled">18</li>
												<li class="cell disabled">19</li>
												<li class="cell disabled">20</li>
												<li class="cell disabled">21</li>
												<li class="cell disabled">22</li>
												<li class="cell disabled">23</li>
												<li class="cell disabled">24</li>
												<li class="cell disabled">25</li>
												<li class="cell disabled">26</li>
												<li class="cell disabled">27</li>
												<li class="cell disabled">28</li>
												<li class="cell disabled">29</li>
												<li class="cell actived disabled">30</li>
												<li class="cell disabled">31</li>
												<li class="cell disabled">32</li>
												<li class="cell disabled">33</li>
												<li class="cell disabled">34</li>
												<li class="cell disabled">35</li>
												<li class="cell disabled">36</li>
												<li class="cell disabled">37</li>
												<li class="cell disabled">38</li>
												<li class="cell disabled">39</li>
												<li class="cell disabled">40</li>
												<li class="cell disabled">41</li>
												<li class="cell disabled">42</li>
												<li class="cell disabled">43</li>
												<li class="cell disabled">44</li>
												<li class="cell disabled">45</li>
												<li class="cell disabled">46</li>
												<li class="cell disabled">47</li>
												<li class="cell disabled">48</li>
												<li class="cell disabled">49</li>
												<li class="cell disabled">50</li>
												<li class="cell disabled">51</li>
												<li class="cell disabled">52</li>
												<li class="cell disabled">53</li>
												<li class="cell disabled">54</li>
												<li class="cell disabled">55</li>
												<li class="cell disabled">56</li>
												<li class="cell disabled">57</li>
												<li class="cell disabled">58</li>
												<li class="cell disabled">59</li>
											</ul>
											<ul class="mx-time-list"  style={{width: "33.3333%"}}>
												<li class="cell actived disabled">00</li>
												<li class="cell disabled">01</li>
												<li class="cell disabled">02</li>
												<li class="cell disabled">03</li>
												<li class="cell disabled">04</li>
												<li class="cell disabled">05</li>
												<li class="cell disabled">06</li>
												<li class="cell disabled">07</li>
												<li class="cell disabled">08</li>
												<li class="cell disabled">09</li>
												<li class="cell disabled">10</li>
												<li class="cell disabled">11</li>
												<li class="cell disabled">12</li>
												<li class="cell disabled">13</li>
												<li class="cell disabled">14</li>
												<li class="cell disabled">15</li>
												<li class="cell disabled">16</li>
												<li class="cell disabled">17</li>
												<li class="cell disabled">18</li>
												<li class="cell disabled">19</li>
												<li class="cell disabled">20</li>
												<li class="cell disabled">21</li>
												<li class="cell disabled">22</li>
												<li class="cell disabled">23</li>
												<li class="cell disabled">24</li>
												<li class="cell disabled">25</li>
												<li class="cell disabled">26</li>
												<li class="cell disabled">27</li>
												<li class="cell disabled">28</li>
												<li class="cell disabled">29</li>
												<li class="cell disabled">30</li>
												<li class="cell disabled">31</li>
												<li class="cell disabled">32</li>
												<li class="cell disabled">33</li>
												<li class="cell disabled">34</li>
												<li class="cell disabled">35</li>
												<li class="cell disabled">36</li>
												<li class="cell disabled">37</li>
												<li class="cell disabled">38</li>
												<li class="cell disabled">39</li>
												<li class="cell disabled">40</li>
												<li class="cell disabled">41</li>
												<li class="cell disabled">42</li>
												<li class="cell disabled">43</li>
												<li class="cell disabled">44</li>
												<li class="cell disabled">45</li>
												<li class="cell disabled">46</li>
												<li class="cell disabled">47</li>
												<li class="cell disabled">48</li>
												<li class="cell disabled">49</li>
												<li class="cell disabled">50</li>
												<li class="cell disabled">51</li>
												<li class="cell disabled">52</li>
												<li class="cell disabled">53</li>
												<li class="cell disabled">54</li>
												<li class="cell disabled">55</li>
												<li class="cell disabled">56</li>
												<li class="cell disabled">57</li>
												<li class="cell disabled">58</li>
												<li class="cell disabled">59</li>
											</ul>
										</div>
									</div>
								</div>
								 
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row row5">
				<div class="col-12">
					<button class="btn btn-primary btn-block btn-sm" style={{borderRadius: "0",fontWeight: "400",fontSize: "1.2rem"}}>Submit</button>
				</div>
			</div>
			<div class="row row5 ">
				<div class="col-12">
					<div class="row row5">
						<div class="col-12">
							<p class="mb-0 text-center">There are no records to show</p>
						</div>
					</div>
				</div>
			</div>
			 
		</div>
	</div>
</div>

                   
                </Grid>
            </Grid>
            </>
            : 
            <>
            <Header /> 
           <div class="row row5">
                <div className="sidebar-whold-cont">
                    <AccountSidebar />
                </div> 
                <div class="col-md-10 featured-box">
	<div>
		<div>
			<ul role="tablist" id="home-events" class="nav nav-tabs" style={{paddingLeft: "0"}}>
				<li class="nav-item"><a href="javascript:void(0)" data-toggle="tab" class="nav-link active">Darts</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active">
					<div class="coupon-card coupon-card-first">
						<div>
							<table class="table coupon-table" style={{marginBottom: "0"}}>
								<thead>
									<tr>
										<th style={{width: "63%"}}> Game</th>
										<th colspan="2"> 1 </th>
										<th colspan="2"> X </th>
										<th colspan="2"> 2 </th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
							<div class="norecords"><a>
                  No real-time records found
                </a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
           
            </div>
            <Footer />    
            </>}
        </>

    )
}
