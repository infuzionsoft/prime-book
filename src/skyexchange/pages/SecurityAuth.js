import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { List, ListItemText, ListItem, Tabs, Button } from '@material-ui/core';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Grid, AppBar, InputLabel } from '@material-ui/core';
import moment from 'moment';
import Appconfig from "../config/config";
import Footer from "../includes/Footer";
import axios from "axios";

import { isMobile } from "react-device-detect";

import Header from '../includes/Header';
import AccountSidebar from '../includes/AccountSidebar';

const gridType = isMobile ? 10 : 8;
export default function SecurityAuth() {
    return (
        <>
       
         {isMobile ? 
            <>
            <meta name="viewport" content="width=1300" />
            <Header /> 
            <Grid className="account-stat" container spacing={0}>
                <Grid item lg={gridType} xs={12} spacing={2}>
                    {/* <div className="helloaccount">
                        Hello
                    </div> */}
                    <div class="main-content">
	 
	<ul class="nav nav-tabs game-nav-bar">
		<li class="nav-item"><a href="/m/home" class="nav-link">
            In-play
        </a></li>
		<li class="nav-item"><a href="/m/sports" class="nav-link">
            Sports
        </a></li>
		<li class="nav-item"><a href="/m/slot" class="nav-link">
            Casino + Slot
        </a></li>
		<li class="nav-item"><a href="/m/others" class="nav-link">
            Others
        </a></li>
	</ul>
	<div class="card" style={{border: "none"}}>
		<div class="card-header">
			<h4 class="mb-0">Secure Auth Verification</h4></div>
		<div class="card-body security-auth container-fluid container-fluid-5">
			<div class="setting-box">
                <span class="mr-3"  style={{color: "black"}}>Secure Auth Verification Status:</span> 
                <span class="badge badge-danger">Disabled</span>
            </div>
			<div class="mt-2 text-center">
				<fieldset class="form-group" id="__BVID__166">
					<legend tabindex="-1" class="bv-no-focus-ring col-form-label pt-0" id="__BVID__166__BV_label_">Please select below option to enable secure auth verification</legend>
					<div tabindex="-1" role="group" class="bv-no-focus-ring">
						<div id="radio-group-1" role="radiogroup" tabindex="-1" class="btn-group-toggle btn-group btn-group-sm bv-no-focus-ring">
							<label class="btn btn-outline-success btn-sm" style={{borderRight: "none",lineHeight: "1.5"}}>
								<input id="radio-group-1__BV_option_0_" style={{display: "none"}} type="radio" name="auth-options" autocomplete="off" class="" value="1"/>
                                <span>Enable Using Mobile App</span>
                            </label>
							<label class="btn btn-outline-success btn-sm" style={{lineHeight: "1.5"}}>
								<input id="radio-group-1__BV_option_1_" style={{display: "none"}} type="radio" name="auth-options" autocomplete="off" class="" value="2"/>
                                <span>Enable Using Telegram</span>
                            </label>
						</div>
						 
						 
						 
					</div>
				</fieldset>
			</div>
			 
			 
		</div>
	</div>
</div>

                   
                </Grid>
            </Grid>
            </>
            : 
            <>
            <Header /> 
           <div class="row row5">
                <div className="sidebar-whold-cont">
                    <AccountSidebar />
                </div> 
				<div class="col-md-10 report-main-content m-t-5">
	<div class="card">
		<div class="card-header-desktop">
			<h4 class="mb-0">Secure Auth Verification</h4></div>
		<div class="card-body security-auth-desktop">
			<div class="row justify-content-center mt-5 mb-5">
				<div class="col-6">
					<div class="setting-box">
						<span class="mr-3">Secure Auth Verification Status:</span> 
						<span class="badge badge-danger">Disabled</span>
					</div>
					<div class="mt-2 text-center">
						<fieldset class="form-group" id="__BVID__358">
							<legend tabindex="-1" class="bv-no-focus-ring col-form-label pt-0" id="__BVID__358__BV_label_">Please select below option to enable secure auth verification</legend>
							<div tabindex="-1" role="group" class="bv-no-focus-ring">
								<div id="radio-group-1" role="radiogroup" tabindex="-1" class="btn-group-toggle btn-group btn-group-lg bv-no-focus-ring">
									<label class="btn btn-outline-success btn-lg" style={{borderRight: "0"}}>
										<input id="radio-group-1__BV_option_0_" type="radio" name="auth-options" autocomplete="off" class="" value="1"/>
										<span>Enable Using Mobile App</span>
									</label>
									<label class="btn btn-outline-success btn-lg">
										<input id="radio-group-1__BV_option_1_" type="radio" name="auth-options" autocomplete="off" class="" value="2"/>
										<span>Enable Using Telegram</span>
									</label>
								</div>
								 
								 
								 
							</div>
						</fieldset>
					</div>
					 
					 
				</div>
			</div>
		</div>
	</div>
</div>
            </div>
            <Footer />    
            </>}
        </>

    )
}
