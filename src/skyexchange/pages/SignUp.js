import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {Box,Link} from '@material-ui/core';

import betbarterLogo from '../assets/images/betbarter-logo.png';
import BetHiveLogo from '../assets/images/BetHive-logo.png';
import sky247Logo from '../assets/images/sky247-logo-black.png';
import playerFootball from '../assets/images/player-football.webp';
import playerCricket from '../assets/images/player-cricket.webp';



export default function SignUp() {

  return (
    <>
      {/* <Header/> */}
      <Grid container className="signup-body">
        <Grid item lg={4} xs={12} spacing={0} className="brand-column betbarter-column">
            <Box className="middle-bar align-start">
                <Box className="middle-bar-content">
                    <img src={betbarterLogo} alt="" className="middle-bar-logo"/>
                    <Typography variant="span">SPORTSBOOK . EXCHANGE . LIVE CASINO</Typography>
                </Box>
            </Box>
            <Box className="bottom-bar text-align-left">
                <Box className="inner-text">
                    <Typography variant="p" className="big-text bold">Check our <br/>Special offers </Typography>
                </Box>
                <Link to="https://wlskyinfopartners.adsrv.eacdn.com/C.ashx?btag=a_496b_32c_&affid=4&siteid=496&adid=32&c=" target="_blank" className="btn btn-betbarter">Play Now</Link>
            </Box>
        </Grid>
        <Grid item lg={4} xs={12} spacing={0} className="brand-column bethive-column">
            <Box class="middle-bar align-center">
                <Box class="middle-bar-content">
                    <img src={BetHiveLogo} alt="" class="middle-bar-logo"/>
                    <Typography variant ="span">SPORTSBOOK . EXCHANGE . CASINO</Typography>
                </Box>
            </Box>
            <Box class="bottom-bar text-align-center">
                <Link to="https://bit.ly/2NLOwD2" target="_blank" class="btn btn-bethive">Play Now</Link>
            </Box>

            <Box class="characters">
                <Box class="character football-player">
                    <img src={playerFootball} alt=""/>
                </Box>
                <Box class="character cricket-player">
                    <img src={playerCricket} alt=""/>
                </Box>
            </Box>           
        </Grid>
        <Grid item lg={4} xs={12} spacing={0} className="brand-column sky247-column">
            <Box class="middle-bar align-end">
                <Box class="middle-bar-content">
                    <img src={sky247Logo} alt="" class="middle-bar-logo"/>
                    <Typography variant ="span">EXCHANGE . LIVE CASINO . SLOTS</Typography>
                </Box>
            </Box>
            <Box class="bottom-bar text-align-right">
                <Box class="inner-text">
                    <p class="small-text">Grab Sky247 <br/> deposit boosts.</p>
                </Box>
                <Link to="https://wlskyinfopartners.adsrv.eacdn.com/C.ashx?btag=a_496b_112c_&affid=4&siteid=496&adid=112&c=" target="_blank" class="btn btn-sky247">Play Now</Link>
            </Box>
        </Grid>
      </Grid>

    </>
  );
}
