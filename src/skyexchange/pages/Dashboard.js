import React from "react";
import Carousel from "react-material-ui-carousel";
import {
  makeStyles,
  Grid,
  Tabs,
  Tab,
  Box,
  Typography,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import Appconfig from "../config/config";
import { Route, Redirect } from 'react-router';
import axios from 'axios';
import moment from 'moment';

import Footer from "../includes/Footer";
import Header from "../includes/Header";

// Import Images
import Slide1 from "../assets/images/slide1.jpg";
import Slide2 from "../assets/images/slide2.jpg";
import Loader from '../assets/images/loading40.gif';
import AccountSidebar from '../includes/AccountSidebar';
import { MobileView, BrowserView, isMobile } from 'react-device-detect';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearchPlus } from '@fortawesome/free-solid-svg-icons'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons'
import { faTv } from '@fortawesome/free-solid-svg-icons'
import { faLandmark } from '@fortawesome/free-solid-svg-icons'
import { faAndroid } from '@fortawesome/free-brands-svg-icons'
import FancyImg from '../assets/images/fancy.svg';
import BookmakerImg from '../assets/images/bookmaker.svg';
import InPlayImg from '../assets/images/play.svg';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  dashboardWrap: {
    width: "75%",
    margin: "auto",
    marginTop: "2px",
  },
  AlignCenter: {
    alignItems: "center",
  },
  borderRight: {
    borderRight: "1px solid #999",
    height: "100%",
  },
  marginBottomLast: {
    marginBottom: "24px !important",
  },
}));

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}
// function loader_default(){
//   document.getElementById("poker_loading").style.display = "block";

// }


// function loader_remove() {
//   var pokerload =  document.getElementById("poker_loading");
//   if (pokerload != "")  {
//     pokerload.style.display = "none";
//   }
// }
var items = [
  {
    image: Slide1,
  },
  {
    image: Slide2,
  },
];

function Item(props) {
  return <img alt="skyexchange" src={props.item.image} />;
}
// function openTabs(evt, Name) {
//   var i, tabcontent, tablinks;
//   tabcontent = document.getElementsByClassName("tabcontent");
//   for (i = 0; i < tabcontent.length; i++) {
//     tabcontent[i].style.display = "none";
//   }
//   // tablinks = document.getElementsByClassName("tablinks");
//   // for (i = 0; i < tablinks.length; i++) {
//   //   // tablinks[i].className = tablinks[i].className.replace("active", "");
//   //   // alert();
//   // }
//   document.getElementById(Name).style.display = "block";
//   // evt.currentTarget.className += "active";
// }
const Dashboard = () => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [bal, setBal] = React.useState(false);
  const isLoggedIn = window.sessionStorage.getItem("loggedIn") && window.sessionStorage.getItem("loggedIn") != "false" ? true : false;
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const openCasinoModal = () => {
    setBal(true);
  };
  const [scoreUrl, setScoreUrl] = React.useState("");

  async function getScoreUrl(eventId) {
    await fetch(`${Appconfig.marketSarket}animurl/${eventId}`)
      .then(response => response.json())
      .then(data => setScoreUrl(data.animation));
      console.log();
  }

  const current = new Date();
  const today = `${current.getFullYear()}-${current.getMonth()+1}-${current.getDate()}`;
  const tomorrow   = `${current.getFullYear()}-${current.getMonth()+1}-${current.getDate()+1}`;

  const [eventListData, setEventListData] = React.useState([]);

  function getCricketData() {
    var data = JSON.stringify({
        // user_id: userInfo._id,
        event_type_id: 4,
    });

    var config = {
        method: "post",
        url: `${Appconfig.apiUrl}eventsDashboard/getDashboardData`,
        headers: {
            "Content-Type": "application/json",
        },
        data: data,
    };

    axios(config)
        .then(function (response) {
            console.log('eventListData', response.data.resultData);
            renderEventData(response.data.resultData);
            

            console.log(response.data.resultData);
        })
        .catch(function (error) {
            console.log(error);
        });
}

const renderEventData = (eventData) => {
  let tempData = [];
  eventData.map((Crick, index) => {
      Crick.competitions.map((competition, index) => {
          competition.events.map((event, index) => {
            console.log("asjdfklcheck", event);
              if (event.marketTypes.length) {
                  if (event.marketTypes[0].marketRunners.length) {
                      // console.log('check for market runnner',event.marketTypes[0].marketRunners[0])
                      if (event.marketTypes[0].marketRunners[0].selection_id) {
                          // console.log('test for empty',event.marketTypes[0].marketRunners.length)
                          let sport_type = '';
                          console.log(event);
                          if (event.event_type == 4) {
                              sport_type = 'Cricket';
                          }
                          // } else if (event.event_type == 2) {
                          //     sport_type = 'Tennis';

                          // }
                          // else if (event.event_type == 1) {
                          //     sport_type = 'Soccer';
                          // }

                          console.log('event.marketTypes[0].marketRunners[0].open_date',event);

                          let eventDetail = {
                              sport: sport_type,
                              event_id: event.event_id,
                              name: event.event_name,
                              // time: moment(event.open_date).format("hh:mm"),
                              time:moment(event.open_date, "MM/DD/YYYY hh:mm:ss A").format("HH:mm"),
                              date:moment(event.open_date, "MM/DD/YYYY hh:mm:ss A").format("YYYY-MM-DD"),

                              // date: moment(event.open_date).format("YYYY-MM-DD"),
                              is_inplay: event.is_inplay == 'True' ? "Inplay" : "Going Inplay",
                              backFirst: event.marketTypes[0].marketRunners[0].back_1_price,
                              layFirst: event.marketTypes[0].marketRunners[0].lay_1_price,
                              backSecond: event.marketTypes[0].marketRunners[1].back_1_price,
                              laySecond: event.marketTypes[0].marketRunners[1].lay_1_price,
                              selection1Id: event.marketTypes[0].marketRunners[0].selection_id,
                              selection2Id: event.marketTypes[0].marketRunners[1].selection_id,
                              is_fancy: event.is_fancy,
                              is_bookmaker: event.is_bookmaker,


                          }

                          console.log('eventDetail',eventDetail);

                          tempData.push(eventDetail);
                      }
                  }
                  else {
                      // console.log('its empty',event.marketTypes[0].marketRunners.length)
                  }
              }


          });
      });
  });
  setEventListData(tempData);
};
  React.useEffect(() => {
    getCricketData();
  }, []);

  function openTabsDashboard(evt, Name , tabName) {
    
    var i, tabcontent, tablinks , tab , tabDisplay;
    
    tab = document.getElementsByClassName("nav-link-active");
    for (i = 0; i < tab.length; i++) {
      tab[i].classList.remove("active")
    }
    document.getElementById(Name).classList.add("active");

    tabDisplay = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabDisplay.length; i++) {
      tabDisplay[i].classList.remove("tabactive")
    }
    document.getElementById(tabName).classList.add("tabactive");
    
    // evt.currentTarget.className += "active";
  }
  
  return (
    <>

      {isLoggedIn ?
      
      
      <>
      <Header />
      <div className="sidebar">
          {isMobile ? null :
            <div className="sidebar-whold-cont">
              <AccountSidebar />
            </div>}
          <div className="home-whole-cont">
            {isMobile ?
              <ul class="nav nav-tabs mobile-navbar-first game-nav-bar">
                <li class="nav-item">
                  <a href="/m/home" class="nav-link router-link-exact-active router-link-active active">In-play</a>
                </li>
                <li class="nav-item">
                  <a href="/m/sports" class="nav-link">Sports</a>
                </li>
                <li class="nav-item">
                  <a href="/m/slot" class="nav-link">Casino + Slot</a>
                </li>
                <li class="nav-item">
                  <a href="/m/others" class="nav-link">Others</a>
                </li>
              </ul>
              : null}
            <div className="home-tabs-cont">


              {isMobile ?

                <div data-v-51245d04 className="mobile-icon-tab">
                  <div data-v-51245d04 class="tab-content">
                    <div data-v-51245d04 id="home" class="tab-pane sports active">
                      <ul data-v-51245d04="" className="nav nav-tabs game-nav-bar">
                        <li data-v-51245d04="" class="nav-item text-center ">
                          <Link data-v-51245d04="" id="football" data-toggle="tab" href="#game1" class="nav-link  nav-link-active tablinks" onClick={() => openTabsDashboard('event', 'football','footballtab')}>
                            <div data-v-51245d04="">
                              <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/gameImg/1.png" />
                            </div>
                            <div data-v-51245d04="">Football</div>
                          </Link>
                        </li>
                        <li data-v-51245d04="" class="nav-item text-center">
                          <Link data-v-51245d04="" id="tennis" data-toggle="tab" href="#game2" class="nav-link  nav-link-active tablinks" onClick={() => openTabsDashboard('event', 'tennis' , 'tennistab')}>
                            <div data-v-51245d04="">
                              <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/gameImg/2.png" />
                            </div>
                            <div data-v-51245d04="">Tennis</div>
                          </Link>
                        </li>
                        <li data-v-51245d04="" class="nav-item text-center ">
                          <Link data-v-51245d04="" id="cricket" data-toggle="tab" href="#game4" class="nav-link active  nav-link-active" onClick={() => openTabsDashboard('event', 'cricket' ,'crickettab')}>
                            <div data-v-51245d04="">
                              <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/gameImg/4.png" />
                            </div>
                            <div data-v-51245d04="">Cricket</div>
                          </Link>
                        </li>
                        <li data-v-51245d04="" class="nav-item text-center ">
                          <Link data-v-51245d04="" id="icehockey" data-toggle="tab" href="#game7524" class="nav-link  nav-link-active" onClick={()=> openTabsDashboard('event', 'icehockey' , 'icehockeytab')}>
                            <div data-v-51245d04="">
                              <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/gameImg/7524.png" />
                            </div>
                            <div data-v-51245d04="">Ice Hockey</div>
                          </Link>
                        </li>
                        <li data-v-51245d04="" class="nav-item text-center">
                          <a data-v-51245d04="" data-toggle="tab" id="volleyball" href="#game998917" class="nav-link  nav-link-active" onClick={() => openTabsDashboard('event', 'volleyball','volleyballtab')}>
                            <div data-v-51245d04="">
                              <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/gameImg/998917.png" />
                            </div>
                            <div data-v-51245d04="">Volleyball</div>
                          </a>
                        </li>
                        <li data-v-51245d04="" class="nav-item text-center">
                          <a data-v-51245d04="" data-toggle="tab" id="basketball" href="#game7522" class="nav-link  nav-link-active"  onClick={() => openTabsDashboard('event', 'basketball','basketballtab')}>
                            <div data-v-51245d04="">
                              <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/gameImg/7522.png" />
                            </div>
                            <div data-v-51245d04="">Basketball</div>
                          </a>
                        </li>
                        <li data-v-51245d04="" class="nav-item text-center">
                          <a data-v-51245d04="" data-toggle="tab" id="tabletennis" href="#game70" class="nav-link  nav-link-active"  onClick={() => openTabsDashboard('event', 'tabletennis','tabletennistab')}>
                            <div data-v-51245d04="">
                              <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/gameImg/70.png" />
                            </div>
                            <div data-v-51245d04="">Table Tennis</div>
                          </a>
                        </li>
                        <li data-v-51245d04="" class="nav-item text-center">
                          <a data-v-51245d04="" data-toggle="tab" id="darts" href="#game3503" class="nav-link  nav-link-active"  onClick={() => openTabsDashboard('event', 'darts' ,'dartstab')}>
                            <div data-v-51245d04="">
                              <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/gameImg/3503.png" />
                            </div>
                            <div data-v-51245d04="">Darts</div>
                          </a>
                        </li>
                        <li data-v-51245d04="" class="nav-item text-center">
                          <a data-v-51245d04="" data-toggle="tab" id="badminton" href="#game71" class="nav-link  nav-link-active"  onClick={() => openTabsDashboard('event', 'badminton','badmintontab')}>
                            <div data-v-51245d04="">
                              <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/gameImg/71.png" />
                            </div>
                            <div data-v-51245d04="">Badminton</div>
                          </a>
                        </li>
                        <li data-v-51245d04="" class="nav-item text-center">
                          <a data-v-51245d04="" data-toggle="tab" id="kabaddi" href="#game52" class="nav-link  nav-link-active"  onClick={() => openTabsDashboard('event', 'kabaddi','kabadditab')}>
                            <div data-v-51245d04="">
                              <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/gameImg/52.png" />
                            </div>
                            <div data-v-51245d04="">Kabaddi</div>
                          </a>
                        </li>
                        <li data-v-51245d04="" class="nav-item text-center">
                          <a data-v-51245d04="" data-toggle="tab" id="boxing" href="#game6" class="nav-link  nav-link-active"  onClick={() => openTabsDashboard('event', 'boxing' , 'boxingtab')}>
                            <div data-v-51245d04="">
                              <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/gameImg/6.png" />
                            </div>
                            <div data-v-51245d04="">Boxing</div>
                          </a>
                        </li>
                        <li data-v-51245d04="" class="nav-item text-center">
                          <a data-v-51245d04="" data-toggle="tab" id="mixedmartialart" href="#game26420387" class="nav-link  nav-link-active"  onClick={() => openTabsDashboard('event', 'mixedmartialart','mixedmartialarttab')}>
                            <div data-v-51245d04="">
                              <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/gameImg/26420387.png" />
                            </div>
                            <div data-v-51245d04="">Mixed Martial Arts</div>
                          </a>
                        </li>
                        <li data-v-51245d04="" class="nav-item text-center">
                          <a data-v-51245d04="" data-toggle="tab" id="motorsport" href="#game8" class="nav-link  nav-link-active"  onClick={() => openTabsDashboard('event', 'motorsport','motorsporttab')}>
                            <div data-v-51245d04="">
                              <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/gameImg/8.png" />
                            </div>
                            <div data-v-51245d04="">Motor Sport</div>
                          </a>
                        </li>
                      </ul>
                      <div id="footballtab" class="tabcontent">
                        <h3>London</h3>
                        <p>London is the capital city of England.</p>
                      </div>
                      <div id="tennistab" class="tabcontent">
                        <h3>Paris</h3>
                        <p>Paris is  the capital of France. Paris is  the capital of FParis is  the capital of F</p>
                      </div>
                      <div id="crickettab" class="tabcontent tabactive">


                        <div data-v-51245d04="" class="game-listing-container all-list-container" style={{maxHeight: "calc((100vh - 184px) / 2)", overflowX: "auto" }}>
                          {eventListData.map((row) => (
                          <Link data-v-51245d04="" to={`/game-detail/${row.event_id}`} class="text-dark">
                            <div data-v-51245d04="" class="game-list pt-1 pb-1 container-fluid">
                              <div data-v-51245d04="" class="row row5">
                                <div data-v-51245d04="" class="col-8">
                                  <p data-v-51245d04="" class="mb-0 mt-0 game-name">
                                    <strong data-v-51245d04="">{row.name}</strong>
                                  </p>
                                  <p data-v-51245d04="" class="mb-0 mt-0">Nov 27 2021  3:50PM (IST)</p>
                                </div>
                                <div data-v-51245d04="" class="col-4 text-right">
                                  <div data-v-51245d04="" class="game-icons">
                                    <span data-v-51245d04="" class="game-icon">
                                      <span data-v-51245d04="" class="active-icon" style={{ verticalAlign: 'bottom' }}></span>
                                    </span>
                                    <span data-v-51245d04="" class="game-icon">
                                      <FontAwesomeIcon className="iconin-form" icon={faTv} />
                                    </span>
                                    <span data-v-51245d04="" class="game-icon bm-icon">
                                      <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/ic_bm.png" />
                                    </span>
                                    <span data-v-51245d04="" class="game-icon vir-icon">
                                      <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/ic_vir.png" />
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <div data-v-51245d04="" class="row row5">
                                <div data-v-51245d04="" class="col-12">
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <b data-v-51245d04="">1</b>
                                  </div>
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <b data-v-51245d04="">X</b>
                                  </div>
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <b data-v-51245d04="">2</b>
                                  </div>
                                </div>
                              </div>
                              <div data-v-51245d04="" class="row row5">
                                <div data-v-51245d04="" class="col-12">
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-back">0</a>
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-lay">0</a>
                                  </div>
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-back">0</a>
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-lay">0</a>
                                  </div>
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-back">0</a>
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-lay">0</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </Link>
                          ))}
                          {/* <Link data-v-51245d04="" href="/m/casino/cricketvirtual/2111271426" class="text-dark">
                            <div data-v-51245d04="" class="game-list pt-1 pb-1 container-fluid">
                              <div data-v-51245d04="" class="row row5">
                                <div data-v-51245d04="" class="col-8">
                                  <p data-v-51245d04="" class="mb-0 game-name">
                                    <strong data-v-51245d04="">Sri Lanka T10 v India T10</strong>
                                  </p>
                                  <p data-v-51245d04="" class="mb-0 mt-0">Nov 27 2021  4:45PM (IST)</p>
                                </div>
                                <div data-v-51245d04="" class="col-4 text-right">
                                  <div data-v-51245d04="" class="game-icons">
                                    <span data-v-51245d04="" class="game-icon">
                                      <span data-v-51245d04="" class="active-icon" style={{ verticalAlign: 'bottom' }}></span>
                                    </span>
                                    <span data-v-51245d04="" class="game-icon">
                                      <FontAwesomeIcon className="iconin-form" icon={faTv} />
                                    </span>
                                    <span data-v-51245d04="" class="game-icon">
                                      <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/ic_fancy.png" />
                                    </span>
                                    <span data-v-51245d04="" class="game-icon bm-icon">
                                      <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/ic_bm.png" />
                                    </span>
                                    <span data-v-51245d04="" class="game-icon vir-icon">
                                      <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/game-icon.svg" />
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <div data-v-51245d04="" class="row row5">
                                <div data-v-51245d04="" class="col-12">
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <b data-v-51245d04="">1</b>
                                  </div>
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <b data-v-51245d04="">X</b>
                                  </div>
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <b data-v-51245d04="">2</b>
                                  </div>
                                </div>
                              </div>
                              <div data-v-51245d04="" class="row row5">
                                <div data-v-51245d04="" class="col-12">
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-back">0</a>
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-lay">0</a>
                                  </div>
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-back">0</a>
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-lay">0</a>
                                  </div>
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-back">0</a>
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-lay">0</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </Link>
                          <Link data-v-51245d04="" href="/m/gamedetail/31089509" class="">
                            <div data-v-51245d04="" class="game-list pt-1 pb-1 container-fluid">
                              <div data-v-51245d04="" class="row row5">
                                <div data-v-51245d04="" class="col-8">
                                  <p data-v-51245d04="" class="mb-0 game-name">
                                    <strong data-v-51245d04="">Abu Dhabi v Northern Warriors</strong>
                                  </p>
                                  <p data-v-51245d04="" class="mb-0 mt-0">Nov 27 2021  5:30PM (IST)</p>
                                </div>
                                <div data-v-51245d04="" class="col-4 text-right">
                                  <div data-v-51245d04="" class="game-icons">
                                    <span data-v-51245d04="" class="game-icon">
                                      <span data-v-51245d04="" class="active-icon" style={{ verticalAlign: 'bottom' }}></span>
                                    </span>
                                    <span data-v-51245d04="" class="game-icon">
                                      <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/ic_fancy.png" />
                                    </span>
                                    <span data-v-51245d04="" class="game-icon">
                                      <img data-v-51245d04="" src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/ic_bm.png" class="bm-icon" />
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <div data-v-51245d04="" class="row row5">
                                <div data-v-51245d04="" class="col-12">
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <b data-v-51245d04="">1</b>
                                  </div>
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <b data-v-51245d04="">X</b>
                                  </div>
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <b data-v-51245d04="">2</b>
                                  </div>
                                </div>
                              </div>
                              <div data-v-51245d04="" class="row row5">
                                <div data-v-51245d04="" class="col-12">
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-back">1.57</a>
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-lay">1.59</a>
                                  </div>
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-back">0</a>
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-lay">0</a>
                                  </div>
                                  <div data-v-51245d04="" class="text-center game-col game-home">
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-back">2.68</a>
                                    <a data-v-51245d04="" href="javascript:void(0);" class="btn-lay">2.74</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </Link> */}
                        </div>


                      </div>
                      <div id="icehockeytab" class="tabcontent">
                          <div data-v-51245d04="" class="game-listing-container" style={{maxHeight: "calc((100vh - 184px) / 2)", overflowX: "auto"}}>
                              <div data-v-51245d04="" class="game-list norecords pt-2 pb-2 container-fluid">
                                <div data-v-51245d04="" class="row row5">
                                  <div data-v-51245d04="" class="col-12">
                                    <p data-v-51245d04="" class="text-center mb-1 mt-1">No real-time records found</p>
                                  </div>
                                </div>
                              </div>
                          </div>
                      </div>
                      <div id="volleyballtab" class="tabcontent">
                          <div data-v-51245d04="" class="game-listing-container" style={{maxHeight: "calc((100vh - 184px) / 2)", overflowX: "auto"}}>
                              <div data-v-51245d04="" class="game-list norecords pt-2 pb-2 container-fluid">
                                <div data-v-51245d04="" class="row row5">
                                  <div data-v-51245d04="" class="col-12">
                                    <p data-v-51245d04="" class="text-center mb-1 mt-1">No real-time records found</p>
                                  </div>
                                </div>
                              </div>
                          </div>
                      </div>
                      <div id="icehockeytab" class="tabcontent">
                          <div data-v-51245d04="" class="game-listing-container" style={{maxHeight: "calc((100vh - 184px) / 2)", overflowX: "auto"}}>
                              <div data-v-51245d04="" class="game-list norecords pt-2 pb-2 container-fluid">
                                <div data-v-51245d04="" class="row row5">
                                  <div data-v-51245d04="" class="col-12">
                                    <p data-v-51245d04="" class="text-center mb-1 mt-1">No real-time records found</p>
                                  </div>
                                </div>
                              </div>
                          </div>
                      </div>
                      <div id="basketballtab" class="tabcontent">
                          <div data-v-51245d04="" class="game-listing-container" style={{maxHeight: "calc((100vh - 184px) / 2)", overflowX: "auto"}}>
                              <div data-v-51245d04="" class="game-list norecords pt-2 pb-2 container-fluid">
                                <div data-v-51245d04="" class="row row5">
                                  <div data-v-51245d04="" class="col-12">
                                    <p data-v-51245d04="" class="text-center mb-1 mt-1">No real-time records found</p>
                                  </div>
                                </div>
                              </div>
                          </div>
                      </div>
                      <div id="tabletennistab" class="tabcontent">
                          <div data-v-51245d04="" class="game-listing-container" style={{maxHeight: "calc((100vh - 184px) / 2)", overflowX: "auto"}}>
                              <div data-v-51245d04="" class="game-list norecords pt-2 pb-2 container-fluid">
                                <div data-v-51245d04="" class="row row5">
                                  <div data-v-51245d04="" class="col-12">
                                    <p data-v-51245d04="" class="text-center mb-1 mt-1">No real-time records found</p>
                                  </div>
                                </div>
                              </div>
                          </div>
                      </div>
                      <div id="dartstab" class="tabcontent">
                          <div data-v-51245d04="" class="game-listing-container" style={{maxHeight: "calc((100vh - 184px) / 2)", overflowX: "auto"}}>
                              <div data-v-51245d04="" class="game-list norecords pt-2 pb-2 container-fluid">
                                <div data-v-51245d04="" class="row row5">
                                  <div data-v-51245d04="" class="col-12">
                                    <p data-v-51245d04="" class="text-center mb-1 mt-1">No real-time records found</p>
                                  </div>
                                </div>
                              </div>
                          </div>
                      </div>
                      <div id="badmintontab" class="tabcontent">
                          <div data-v-51245d04="" class="game-listing-container" style={{maxHeight: "calc((100vh - 184px) / 2)", overflowX: "auto"}}>
                              <div data-v-51245d04="" class="game-list norecords pt-2 pb-2 container-fluid">
                                <div data-v-51245d04="" class="row row5">
                                  <div data-v-51245d04="" class="col-12">
                                    <p data-v-51245d04="" class="text-center mb-1 mt-1">No real-time records found</p>
                                  </div>
                                </div>
                              </div>
                          </div>
                      </div>
                      <div id="kabadditab" class="tabcontent">
                          <div data-v-51245d04="" class="game-listing-container" style={{maxHeight: "calc((100vh - 184px) / 2)", overflowX: "auto"}}>
                              <div data-v-51245d04="" class="game-list norecords pt-2 pb-2 container-fluid">
                                <div data-v-51245d04="" class="row row5">
                                  <div data-v-51245d04="" class="col-12">
                                    <p data-v-51245d04="" class="text-center mb-1 mt-1">No real-time records found</p>
                                  </div>
                                </div>
                              </div>
                          </div>
                      </div>
                      <div id="boxingtab" class="tabcontent">
                          <div data-v-51245d04="" class="game-listing-container" style={{maxHeight: "calc((100vh - 184px) / 2)", overflowX: "auto"}}>
                              <div data-v-51245d04="" class="game-list norecords pt-2 pb-2 container-fluid">
                                <div data-v-51245d04="" class="row row5">
                                  <div data-v-51245d04="" class="col-12">
                                    <p data-v-51245d04="" class="text-center mb-1 mt-1">No real-time records found</p>
                                  </div>
                                </div>
                              </div>
                          </div>
                      </div>
                      <div id="mixedmartialarttab" class="tabcontent">
                          <div data-v-51245d04="" class="game-listing-container" style={{maxHeight: "calc((100vh - 184px) / 2)", overflowX: "auto"}}>
                              <div data-v-51245d04="" class="game-list norecords pt-2 pb-2 container-fluid">
                                <div data-v-51245d04="" class="row row5">
                                  <div data-v-51245d04="" class="col-12">
                                    <p data-v-51245d04="" class="text-center mb-1 mt-1">No real-time records found</p>
                                  </div>
                                </div>
                              </div>
                          </div>
                      </div>
                      <div id="motorsporttab" class="tabcontent">
                          <div data-v-51245d04="" class="game-listing-container" style={{maxHeight: "calc((100vh - 184px) / 2)", overflowX: "auto"}}>
                              <div data-v-51245d04="" class="game-list norecords pt-2 pb-2 container-fluid">
                                <div data-v-51245d04="" class="row row5">
                                  <div data-v-51245d04="" class="col-12">
                                    <p data-v-51245d04="" class="text-center mb-1 mt-1">No real-time records found</p>
                                  </div>
                                </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>



                :

                <><div className="pb-4">
                  {/* <div className="tabs"> */}
                  {/* <ul className="nav-tabs"> */}
                  <ul className="nav nav-tabs">
                  
                    <li className="nav-item">
                      
                      <Link id="football" className="nav-link  nav-link-active tablinks" onClick={() => openTabsDashboard('event', 'football','footballtab')} to="/#">Football</Link>
                    </li>
                    <li className="nav-item">
                      <Link id="tennis" className="nav-link  nav-link-active tablinks" onClick={() => openTabsDashboard('event', 'tennis','tennistab')} to="/#">Tennis</Link>
                    </li>
                    <li className="nav-item">
                      <Link id="cricket" className="nav-link  nav-link-active tablinks active" onClick={() => openTabsDashboard('event', 'cricket','crickettab')} to="/#">Cricket</Link>
                    </li>
                    <li className="nav-item">
                      <Link id="icehockey" className="nav-link  nav-link-active tablinks pr-4" onClick={() => openTabsDashboard('event', 'icehockey','icehockeytab')} to="/#">Ice<br/>Hockey</Link>
                    </li>
                    <li className="nav-item">
                      <Link id="volleyball" className="nav-link  nav-link-active tablinks" onClick={() => openTabsDashboard('event', 'volleyball','volleyballtab')}>Volleyball</Link>
                    </li>
                    <li className="nav-item">
                      <Link  id="basketball" className="nav-link  nav-link-active tablinks" onClick={()=> openTabsDashboard('event', 'basketball','basketballtab')} to="">Basketball</Link>
                    </li>
                    <li className="nav-item">
                      <Link  id="tabletennis" className="nav-link  nav-link-active tablinks pr-4" onClick={()=> openTabsDashboard('event', 'tabletennis','tabletennistab')} to="">Table<br/>Tennis</Link>
                    </li>
                    <li className="nav-item">
                      <Link  id="darts" className="nav-link  nav-link-active tablinks" onClick={()=> openTabsDashboard('event', 'darts' ,'dartstab')} to="">Darts</Link>
                    </li>
                    <li className="nav-item">
                      <Link  id="badminton" className="nav-link  nav-link-active tablinks" onClick={()=> openTabsDashboard('event', 'badminton','badmintontab')} to="">Badminton</Link>
                    </li>
                    <li className="nav-item">
                      <Link  id="kabaddi" className="nav-link  nav-link-active nav-link-active tablinks" onClick={()=> openTabsDashboard('event', 'kabaddi','kabadditab')} to="">kabaddi</Link>
                    </li>
                    <li className="nav-item">
                      <Link  id="boxing" className="nav-link  nav-link-active tablinks" onClick={()=> openTabsDashboard('event', 'boxing','boxingtab')} to="">Boxing</Link>
                    </li>
                    <li className="nav-item">
                      <Link  id="mixedmartialarts" className="nav-link  nav-link-active tablinks pr-4" onClick={()=> openTabsDashboard('event', 'mixedmartialarts','mixedmartialartstab')} to="">Mixed<br/>Martial Arts</Link>
                    </li>
                    <li className="nav-item">
                      <Link  id="motorsport" className="nav-link  nav-link-active tablinks pr-4" onClick={()=> openTabsDashboard('event', 'motorsport','motorsporttab')} to="">Motor<br/>Sport</Link>
                    </li>
                  </ul>
                
                  <div id="footballtab" class="tabcontent">
                    <h3>London</h3>
                    <p>London is the capital city of England.</p>
                  </div>
                  <div id="tennistab" class="tabcontent">
                    <h3>Paris</h3>
                    <p>Paris is  the capital of France. Paris is  the capital of FParis is  the capital of F</p>
                  </div>
                  <div id="crickettab" class="tabcontent tabactive">
                    <table class="table coupon-table">
                      <thead>
                        <tr>
                          <th style={{ width: '63%' }}>Game</th>
                          <th colspan="2">1</th> <th colspan="2">X</th>
                          <th colspan="2">2</th>
                        </tr>
                      </thead>
                      <tbody>
                      {eventListData.map((row) => (
                        <tr>
                          <td>
                            <div className="game-name">
                              <a href={`/game-detail/${row.event_id}`} className="text-dark">{row.name}
                              
                              {
                                <span className="time"> / { row.is_inplay == 'Inplay' ? 'In-Play' : row.date == today ? row.time: row.date == tomorrow?`Tomorrow ${row.time}` : `${row.date} ${row.time}` }</span>

                              } 
                              </a>
                            </div>
                            <div className="game-icons">
                              <span className="game-icon"><span className="active">
                              {
                                row.is_inplay == "true" ?
                                <img src="https:dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/icons/ic_fancy.png" alt="skyexchange" className="fancy-icon" />
                                : null
                              }   
                              </span></span>
                              
                              {
                                row.is_tv == "True" ?
                                <span data-v-51245d04="" class="game-icon"><FontAwesomeIcon className="iconin-form" icon={faTv} /></span>
                               : null
                             }  
                              <span className="game-icon"> 
                              {
                                row.is_fancy == "True" ?
                                  <>
                                  <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/ic_fancy.png" alt="skyexchange" className="fancy-f-icon" />
                                  </> : null
                              }</span>

                              <span className="game-icon"> 
                              {
                                row.is_bookmaker == "True" ?
                                <>
                                    <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/ic_bm.png" alt="skyexchange" className="bookmarker-f-icon" />
                                </> : null
                              }
                              </span>
                              <span className="game-icon"> </span>
                              
                            </div>
                          </td>
                          <td>
                            <button className="back">
                              <span className="odd">1.78</span>
                            </button>
                          </td>
                          <td><button className="lay"><span className="odd">1.9</span></button></td>
                          <td><button className="back"><span className="odd">-</span></button></td>
                          <td><button className="lay"><span className="odd">-</span></button></td>
                          <td><button className="back"><span className="odd">2.1</span></button></td>
                          <td><button className="lay"><span className="odd">2.3</span></button></td>
                        </tr>
                      ))}
                      </tbody>
                    </table>
                  </div>
                  <div id="icehockeytab" class="tabcontent">
                  <div class="tab-content">
                      <div class="tab-pane active">
                        <div class="coupon-card coupon-card-first">
                          <div>
                            <table class="table coupon-table" style={{marginBottom: "0"}}>
                              <thead>
                                <tr>
                                  <th style={{width: "63%"}}>Game</th>
                                  <th colspan="2">1</th>
                                  <th colspan="2">X</th>
                                  <th colspan="2">2</th>
                                </tr>
                              </thead>
                              <tbody></tbody>
                            </table>
                            <div class="norecords"><a>No real-time records found</a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="volleyballtab" class="tabcontent">
                  <div class="tab-content">
                      <div class="tab-pane active">
                        <div class="coupon-card coupon-card-first">
                          <div>
                            <table class="table coupon-table" style={{marginBottom: "0"}}>
                              <thead>
                                <tr>
                                  <th style={{width: "63%"}}>Game</th>
                                  <th colspan="2">1</th>
                                  <th colspan="2">X</th>
                                  <th colspan="2">2</th>
                                </tr>
                              </thead>
                              <tbody></tbody>
                            </table>
                            <div class="norecords"><a>No real-time records found</a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="basketballtab" class="tabcontent">
                  <div class="tab-content">
                      <div class="tab-pane active">
                        <div class="coupon-card coupon-card-first">
                          <div>
                            <table class="table coupon-table" style={{marginBottom: "0"}}>
                              <thead>
                                <tr>
                                  <th style={{width: "63%"}}>Game</th>
                                  <th colspan="2">1</th>
                                  <th colspan="2">X</th>
                                  <th colspan="2">2</th>
                                </tr>
                              </thead>
                              <tbody></tbody>
                            </table>
                            <div class="norecords"><a>No real-time records found</a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="tabletennistab" class="tabcontent">
                  <div class="tab-content">
                      <div class="tab-pane active">
                        <div class="coupon-card coupon-card-first">
                          <div>
                            <table class="table coupon-table" style={{marginBottom: "0"}}>
                              <thead>
                                <tr>
                                  <th style={{width: "63%"}}>Game</th>
                                  <th colspan="2">1</th>
                                  <th colspan="2">X</th>
                                  <th colspan="2">2</th>
                                </tr>
                              </thead>
                              <tbody></tbody>
                            </table>
                            <div class="norecords"><a>No real-time records found</a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="dartstab" class="tabcontent">
                  <div class="tab-content">
                      <div class="tab-pane active">
                        <div class="coupon-card coupon-card-first">
                          <div>
                            <table class="table coupon-table" style={{marginBottom: "0"}}>
                              <thead>
                                <tr>
                                  <th style={{width: "63%"}}>Game</th>
                                  <th colspan="2">1</th>
                                  <th colspan="2">X</th>
                                  <th colspan="2">2</th>
                                </tr>
                              </thead>
                              <tbody></tbody>
                            </table>
                            <div class="norecords"><a>No real-time records found</a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="badmintontab" class="tabcontent">
                  <div class="tab-content">
                      <div class="tab-pane active">
                        <div class="coupon-card coupon-card-first">
                          <div>
                            <table class="table coupon-table" style={{marginBottom: "0"}}>
                              <thead>
                                <tr>
                                  <th style={{width: "63%"}}>Game</th>
                                  <th colspan="2">1</th>
                                  <th colspan="2">X</th>
                                  <th colspan="2">2</th>
                                </tr>
                              </thead>
                              <tbody></tbody>
                            </table>
                            <div class="norecords"><a>No real-time records found</a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="kabadditab" class="tabcontent">
                  <div class="tab-content">
                      <div class="tab-pane active">
                        <div class="coupon-card coupon-card-first">
                          <div>
                            <table class="table coupon-table" style={{marginBottom: "0"}}>
                              <thead>
                                <tr>
                                  <th style={{width: "63%"}}>Game</th>
                                  <th colspan="2">1</th>
                                  <th colspan="2">X</th>
                                  <th colspan="2">2</th>
                                </tr>
                              </thead>
                              <tbody></tbody>
                            </table>
                            <div class="norecords"><a>No real-time records found</a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="boxingtab" class="tabcontent">
                  <div class="tab-content">
                      <div class="tab-pane active">
                        <div class="coupon-card coupon-card-first">
                          <div>
                            <table class="table coupon-table" style={{marginBottom: "0"}}>
                              <thead>
                                <tr>
                                  <th style={{width: "63%"}}>Game</th>
                                  <th colspan="2">1</th>
                                  <th colspan="2">X</th>
                                  <th colspan="2">2</th>
                                </tr>
                              </thead>
                              <tbody></tbody>
                            </table>
                            <div class="norecords"><a>No real-time records found</a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="mixedmartialartstab" class="tabcontent">
                  <div class="tab-content">
                      <div class="tab-pane active">
                        <div class="coupon-card coupon-card-first">
                          <div>
                            <table class="table coupon-table" style={{marginBottom: "0"}}>
                              <thead>
                                <tr>
                                  <th style={{width: "63%"}}>Game</th>
                                  <th colspan="2">1</th>
                                  <th colspan="2">X</th>
                                  <th colspan="2">2</th>
                                </tr>
                              </thead>
                              <tbody></tbody>
                            </table>
                            <div class="norecords"><a>No real-time records found</a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="motorsporttab" class="tabcontent">
                  <div class="tab-content">
                      <div class="tab-pane active">
                        <div class="coupon-card coupon-card-first">
                          <div>
                            <table class="table coupon-table" style={{marginBottom: "0"}}>
                              <thead>
                                <tr>
                                  <th style={{width: "63%"}}>Game</th>
                                  <th colspan="2">1</th>
                                  <th colspan="2">X</th>
                                  <th colspan="2">2</th>
                                </tr>
                              </thead>
                              <tbody></tbody>
                            </table>
                            <div class="norecords"><a>No real-time records found</a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  </div></>}
            </div>
            {/* Home Product Container Start Prime book */}
            {isMobile ?
              <div class="tab-pane active casino-tables">
                <div class="container-fluid">
                  <div class="row row5">
                    <div class="col-12">
                      <h4 class="text-uppercase mt-3">Live Casino</h4>
                    </div>
                  </div>
                  <div class="row row5 mt-2">
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/superover" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/superover.jpg" class="img-fluid" />
                          <div class="casino-name">Super Over</div>
                          <div class="new-launch-casino">New Launch</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                    <div class="col-3 text-center">
                      <div class="casinoicons">
                        <a href="/m/casino/race20" class="">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/mobile/img/casinoicons/img/race20.png" class="img-fluid" />
                          <div class="casino-name">Race 20-20</div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              :
              <div class="home-products-container">
                <div class="row row5">
                  <div class="col-md-12">
                    <a href="/casino/superover" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/superover.jpg" class="img-fluid" />
                        <div class="casino-name">Super Over</div>
                        <div class="new-launch-casino-desktop">
                          <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/img/offer-patch.png" />
                        </div>
                      </div>
                    </a>
                    <a href="/casino/race/t20" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/race20.png" class="img-fluid" />
                        <div class="casino-name">Race 20-20</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                    <a href="/casino/queen" class="">
                      <div class="d-inline-block casinoicons bs">
                        <img src="https://dzm0kbaskt4pv.cloudfront.net/v2/static/front/img/casinoicons/img/queen.jpg" class="img-fluid" />
                        <div class="casino-name">Casino Queen</div>
                      </div>
                    </a>
                  </div>
                </div>
              </div>}
            {/* Home Product Container End Prime book */}
          </div>
        </div><Footer /></>
                : <Redirect to="/login"/> }
                </>
                
  );
};

export default Dashboard;
