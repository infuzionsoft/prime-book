// import React from 'react';
import '../assets/css/style.css';
import background from '../assets/images/bg-purple.png';
import Header from '../includes/Header';
import BetSlip from '../includes/BetSlip'

import { useHistory } from 'react-router';
import Appconfig from "../config/config";
import React from 'react';
import PropTypes from 'prop-types';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import { Link } from 'react-router-dom';
import RoomIcon from '@material-ui/icons/Room';
import { makeStyles, Grid, Typography, Accordion, Table, TableHead, TableRow, TableCell, TableBody, AccordionSummary, AccordionDetails, AppBar, Tab, Tabs, Button, Box, Paper, Divider, List, ListItem } from '@material-ui/core'

import InPlayImg from '../assets/images/play.svg'
import BookmakerImg from '../assets/images/bookmaker.svg'
import PremiumImg from '../assets/images/premium.svg'

import FancyImg from '../assets/images/fancy.svg'

import Loader from '../assets/images/loading40.gif';

import bookmarkwithtime from '../assets/images/bookmarkwithtime.png';
import cricketE from '../assets/images/cricket-e.png';
import soccerE from '../assets/images/soccer-e.png';
import tennisE from '../assets/images/tennis-e.png';
import PinOff from '../assets/images/pin-off.svg';
import GreenPinOff from '../assets/images/green-pin-off.svg';
import axios from 'axios';
import moment from 'moment';
import { BrowserView, MobileView, isMobile } from "react-device-detect";

function loader_default(){
    document.getElementById("poker_loading").style.display = "block";

}


function loader_remove(){
    document.getElementById("poker_loading").style.display = "none";

}

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        paddingLeft: 0,
        marginLeft: 20,
        marginRight: 20,
        height: '100%',
        overflow: 'hidden',
        // width: '75%',
        // maxWidth: "70%",
        backgroundColor: theme.palette.background.paper,
    },
    paperStyle: {
        height: 'calc(100% - 104px)',
        // height:'80%',
        overflow: 'hidden',
        overflowY: 'scroll',
    },
    heightSet: {
        height: 'calc(100% - 105px)',
    },
    time: {
        width: '70px',
        fontWeight: 'bold',
        fontSize: '0.8rem',
    },
    textSize: {
        fontSize: '0.8rem',
        verticalAlign: 'middle',
    },
    listStyle: {
        paddingBottom: '4px',
        paddingTop: '4px',
    },
    fromto: {
        paddingLeft: '4px',
        paddingRight: '4px',
    },
    betSlipBar: {
        marginRight: '20px',
        backgroundColor: 'pink',
    },
    betSlipGrid: {
        backgroundColor: 'red',
    },
}));

function ListItemLink(props) {
    return <ListItem button component="a" {...props} />;
}
function TableRowLink(props) {
    return <TableRow button component="Link" {...props} />;
}
function value(val) {
    return ({ time: val[0], type: val[1], game: val[2], from: val[3], to: val[4], link: val[5] });
}
let arr = [
    value(['8:00', 'Soccer', 'US Major League Soccer', 'Los Angeles FC', 'Vancouver Whitecaps', '#']),
    value(['8:00', 'Soccer', 'US Major League Soccer', 'Los An', 'Vancouver Whitecaps', '#']),
    value(['20:00', 'Soccer', 'US Major ', 'Los Angeles FC', ' Whitecaps', '#']),
    value(['11:00', 'Soccer', 'US Major League Soccer', 'Los Angeles FC', 'Vancouver Whitecaps', '#']),
    value(['7:00', 'Tennis', 'US  League Soccer', 'Los Angeles FC', 'Vancouver Whitecaps', '#']),
    value(['3:00', 'Soccer', 'US Major League Soccer', 'Los Angeles FC', null, '#']),
    value(['9:00', 'Soccer', 'US Major League ', 'Los Angeles FC', 'Vancouver Whitecaps', '#']),
    value(['11:00', 'Soccer', 'US Major League Soccer', 'Los Angeles FC', 'Vancouver Whitecaps', '#']),
    value(['10:00', 'Soccer', 'US Major  Soccer', 'Los Angeles FC', 'Vancouver ', '#']),
    value(['8:00', 'Soccer', 'US Major League Soccer', 'Los Angeles FC', 'Vancouver Whitecaps', '#']),
    value(['8:00', 'Soccer', 'US Major League Soccer', 'Los An', 'Vancouver Whitecaps', '#']),
    value(['20:00', 'Soccer', 'US Major ', 'Los Angeles FC', ' Whitecaps', '#']),
    value(['11:00', 'Soccer', 'US Major League Soccer', 'Los Angeles FC', 'Vancouver Whitecaps', '#']),
    value(['7:00', 'Tennis', 'US  League Soccer', 'Los Angeles FC', 'Vancouver Whitecaps', '#']),
    value(['3:00', 'Soccer', 'US Major League Soccer', 'Los Angeles FC', null, '#']),
    value(['9:00', 'Soccer', 'US Major League ', 'Los Angeles FC', 'Vancouver Whitecaps', '#']),
    value(['11:00', 'Soccer', 'US Major League Soccer', 'Los Angeles FC', 'Vancouver Whitecaps', '#']),
    value(['10:00', 'Soccer', 'US Major  Soccer', 'Los Angeles FC', 'Vancouver ', '#']),
    value(['8:00', 'Soccer', 'US Major League Soccer', 'Los Angeles FC', 'Vancouver Whitecaps', '#']),
    value(['8:00', 'Soccer', 'US Major League Soccer', 'Los An', 'Vancouver Whitecaps', '#']),
    value(['20:00', 'Soccer', 'US Major ', 'Los Angeles FC', ' Whitecaps', '#']),
    value(['11:00', 'Soccer', 'US Major League Soccer', 'Los Angeles FC', 'Vancouver Whitecaps', '#']),
    value(['7:00', 'Tennis', 'US  League Soccer', 'Los Angeles FC', 'Vancouver Whitecaps', '#']),
    value(['3:00', 'Soccer', 'US Major League Soccer', 'Los Angeles FC', null, '#']),
    value(['9:00', 'Soccer', 'US Major League ', 'Los Angeles FC', 'Vancouver Whitecaps', '#']),
    value(['11:00', 'Soccer', 'US Major League Soccer', 'Los Angeles FC', 'Vancouver Whitecaps', '#']),
    value(['10:00', 'Soccer', 'US Major  Soccer', 'Los Angeles FC', 'Vancouver ', '#']),
];

function createData(name1, value, name2, play, link, calories, fat, carbs, protein, time) {
    return { name1, value, name2, play, link, calories, fat, carbs, protein, time };
}

function createData1(
    sport,
    event_id,
    name,
    value,
    action,
    backFirst,
    layFirst,
    backSecond,
    laySecond,
    selection1Id,
    selection2Id,
    test,
    is_fancy,
    is_bookmaker

) {


    return {
        sport, event_id, name, value, action, backFirst, layFirst, backSecond, laySecond, selection1Id, selection2Id, is_fancy, is_bookmaker
    };
}

export default function Inplay() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const [eventListData, setEventListData] = React.useState([]);
    const history = useHistory();
    const userInfo = JSON.parse(window.sessionStorage.getItem("userData"));
    const isLoggedIn = window.sessionStorage.getItem("loggedIn") && window.sessionStorage.getItem("loggedIn") != "false" ? true : false;

    React.useEffect(() => {
        getDashboardData()
    }, [])
    const handleChange = (event, newValue) => {
        if (newValue === 3) {
            history.push('/result');
        }
        setValue(newValue);
    };

    function getDashboardData() {
        var data = JSON.stringify({
            // user_id: userInfo._id,
            event_type_id: 4,
        });

        var config = {
            method: "post",
            url: `${Appconfig.apiUrl}eventsDashboard/getDashboardData`,
            headers: {
                "Content-Type": "application/json",
            },
            data: data,
        };

        axios(config)
            .then(function (response) {
                console.log('eventListData', response.data.resultData);
                renderEventData(response.data.resultData);
                loader_remove();
                console.log(response.data.resultData);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    const renderEventData = (eventData) => {
        let tempData = [];
        eventData.map((Crick, index) => {
            Crick.competitions.map((competition, index) => {
                competition.events.map((event, index) => {
                    if (event.marketTypes.length) {
                        if (event.marketTypes[0].marketRunners.length) {
                            // console.log('check for market runnner',event.marketTypes[0].marketRunners[0])
                            if (event.marketTypes[0].marketRunners[0].selection_id) {
                                // console.log('test for empty',event.marketTypes[0].marketRunners.length)
                                let sport_type = '';
                                console.log(event);
                                if (event.event_type == 4) {
                                    sport_type = 'Cricket';

                                } else if (event.event_type == 2) {
                                    sport_type = 'Tennis';

                                }
                                else if (event.event_type == 1) {
                                    sport_type = 'Soccer';
                                }

                                console.log('event.marketTypes[0].marketRunners[0].open_date',event);

                                let eventDetail = {
                                    sport: sport_type,
                                    event_id: event.event_id,
                                    name: event.event_name,
                                    // time: moment(event.open_date).format("hh:mm"),
                                    time:moment(event.open_date, "MM/DD/YYYY hh:mm:ss A").format("HH:mm"),
                                    date:moment(event.open_date, "MM/DD/YYYY hh:mm:ss A").format("YYYY-MM-DD"),

                                    // date: moment(event.open_date).format("YYYY-MM-DD"),
                                    is_inplay: event.is_inplay == 'True' ? "Inplay" : "Going Inplay",
                                    backFirst: event.marketTypes[0].marketRunners[0].back_1_price,
                                    layFirst: event.marketTypes[0].marketRunners[0].lay_1_price,
                                    backSecond: event.marketTypes[0].marketRunners[1].back_1_price,
                                    laySecond: event.marketTypes[0].marketRunners[1].lay_1_price,
                                    selection1Id: event.marketTypes[0].marketRunners[0].selection_id,
                                    selection2Id: event.marketTypes[0].marketRunners[1].selection_id,
                                    is_fancy: event.is_fancy,
                                    is_bookmaker: event.is_bookmaker,


                                }

                                console.log('eventDetail',eventDetail);

                                tempData.push(eventDetail);
                            }
                        }
                        else {
                            // console.log('its empty',event.marketTypes[0].marketRunners.length)
                        }
                    }


                });
            });
        });
        setEventListData(tempData);
    };



    console.log('eventListData', eventListData);

    const root = classes.root + " inplay-parent";
    const today = moment().format("YYYY-MM-DD");

    console.log('today',today);

    const tomorrow = moment().add(1, 'days').format("YYYY-MM-DD");
    return (
        <>
            <Header />
            <div className={root}>
            {isLoggedIn ? null :
                <div id="poker_loading" class="loading-wrap" style={{ 'marginTop': '50px' }}>
                    <ul class="loading">
                        <li><img src={Loader} /></li>
                        <li>Loading…</li>
                    </ul>
                </div>
            }
                <Grid container spacing={1} style={{ backgroundColor: '#F0ECE1' }}>
                    <Grid item lg={9} xs={12} className="mini-games-grid">
                        <AppBar position="static" className="tab headerInplay">
                            <Tabs className="main_tabbing_border" value={value} variant="fullWidth" onChange={handleChange}>
                                <Tab label="In-Play" {...a11yProps(0)} />
                                <Tab label="Today" {...a11yProps(1)} />
                                <Tab label="Tomorrow" {...a11yProps(2)} />
                                <Tab label="Result" {...a11yProps(3)} />
                            </Tabs>
                        </AppBar>
                        <TabPanel className="inplay-tabs" value={value} index={0}>
                            <Accordion defaultExpanded={true} style={{ marginTop: 4 }}>
                                <AccordionSummary aria-controls="panel1a-content" className="bet-slip-bar" style={{ background: `linear-gradient(-180deg, #2E4B5E 0%, #243A48 82%)` }}>
                                    <Typography>Cricket</Typography>
                                </AccordionSummary>
                                <AccordionDetails className="d-none p-0" display="inline-block" style={{ height: "100%" }}>
                                    <Table className={classes.table} aria-label="simple table">
                                        <TableHead>
                                            <TableRow className="tbl_head xs-none">
                                                <TableCell colSpan="5"></TableCell>
                                                <TableCell align="right" colSpan="2">1</TableCell>
                                                <TableCell align="right" colSpan="2">x</TableCell>
                                                <TableCell align="right" colSpan="2">2</TableCell>
                                                <TableCell align="right"></TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody className="tbl_body">
                                            {eventListData.map((row) => (
                                                row.is_inplay == "Inplay" && row.sport == "Cricket" ?
                                                    <TableRow onClick={() => history.push(`/full-market/${row.event_id}`)} p={1} key={row.name1} className="odd_even_clr" button>
                                                        <TableCell className="table_first_row" colSpan="5">
                                                            <div className="md-none inplay-xs-title">
                                                                <FiberManualRecordIcon className="circle-size v-none" />
                                                                <img src={InPlayImg} alt="skyexchange" />

                                                                {
                                                                    row.is_fancy == "True" ?
                                                                        <>
                                                                            <FiberManualRecordIcon className="circle-size  v-none game-fancy" />
                                                                            <img src={FancyImg} alt="skyexchange" className="fancy-f-icon fancyImgall" />
                                                                        </> : null
                                                                }

                                                                {
                                                                    row.is_bookmaker == "True" ?
                                                                        <>
                                                                            <FiberManualRecordIcon className="circle-size v-none game-fancy" />
                                                                            <img src={BookmakerImg} alt="skyexchange" className="bookmarker-f-icon" />
                                                                        </> : null
                                                                }



                                                                {/* <img src={InPlayImg} alt="skyexchange" /> */}
                                                                <span className="time">In-Play</span>

                                                            </div>
                                                            <div className="text_left-in-play">
                                                                <FiberManualRecordIcon className="circle-size circle-inplay" />
                                                                <Link to="#" onClick={() => history.push(`/full-market/${row.event_id}`)} style={{ color: '#2789CE' }}>
                                                                    <Box display="contents">
                                                                    { 
                                                                        row.name.length > 36 ?
                                                                        `${row.name.substring(0, 36)}...` :
                                                                        
                                                                        row.name 
                                                                
                                                                    }
                                                                        {/* <span> {row.name.split("v")[0]} </span>
                                                                        <span className="in-play"> v </span>
                                                                        <span>{row.name.split("v")[1]}</span> */}
                                                                    </Box>
                                                                </Link>
                                                                <span className="in-play xs-none"> {row.action} <img alt="bookMark" src={bookmarkwithtime} /> </span>

                                                                {/* <span className="in-play xs-none icon-on-right"><img alt="bookMark" src={bookmarkwithtime} /> </span> */}
                                                            </div>
                                                        </TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{row.backFirst}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{row.layFirst}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{""}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{""}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{row.backSecond}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{row.laySecond}</span></TableCell>
                                                        <TableCell className="pin-icon" style={{ width: "30px" }}>
                                                            <label className="pin-toggle">
                                                                <input type="checkbox" />
                                                                <a className="inplay-last-icon"></a>
                                                            </label>
                                                        </TableCell>
                                                    </TableRow>
                                                    : ""

                                            ))}
                                        </TableBody>
                                    </Table>
                                </AccordionDetails>
                            </Accordion>
                            <Accordion defaultExpanded={true} style={{ marginTop: 10 }}>
                                <AccordionSummary aria-controls="panel1a-content" className="bet-slip-bar" style={{ background: `linear-gradient(-180deg, #2E4B5E 0%, #243A48 82%)` }}>
                                    <Typography>Soccer</Typography>
                                </AccordionSummary>
                                <AccordionDetails className="d-none p-0" display="inline-block" style={{ height: "100%" }}>
                                    <Table className={classes.table} aria-label="simple table">
                                        <TableHead>
                                            <TableRow className="tbl_head xs-none">
                                                <TableCell colSpan="5"></TableCell>
                                                <TableCell align="right" colSpan="2">1</TableCell>
                                                <TableCell align="right" colSpan="2">x</TableCell>
                                                <TableCell align="right" colSpan="2">2</TableCell>
                                                <TableCell align="right"></TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody className="tbl_body">

                                            {eventListData.map((row) => (
                                                row.sport == "Soccer" ?

                                                    <TableRow onClick={() => history.push(`/full-market/${row.event_id}`)} p={1} key={row.name1} className="odd_even_clr" button>
                                                        <TableCell className="table_first_row hello" colSpan="5">
                                                            <div className="md-none inplay-xs-title">
                                                                <FiberManualRecordIcon className="circle-size v-none" />
                                                                <img src={InPlayImg} alt="skyexchange" />
                                                                <img src={InPlayImg} alt="skyexchange" />
                                                                <span className="">{row.action}</span>
                                                                <span className="game-name xs-game-name"><img src={soccerE} alt="cricket" /></span>
                                                            </div>
                                                            <div className="text_left-in-play">
                                                                <FiberManualRecordIcon className="circle-size" />
                                                                <Link to='#' onClick={() => history.push(`/full-market/${row.event_id}`)} style={{ color: '#2789CE' }}>
                                                                    <Box display="contents">
                                                                        <span> {row.name.split("v")[0]} </span>
                                                                        <span className="in-play"> v </span>
                                                                        <span>{row.name.split("v")[1]}</span>
                                                                    </Box>
                                                                </Link>
                                                                <span className="in-play xs-none"> {row.action} <img alt="bookMark" src={bookmarkwithtime} /> </span>
                                                                <span className="game-name xs-none"><img src={cricketE} alt="cricket" /></span>
                                                                <span className="in-play xs-none icon-on-right"><img alt="bookMark" src={bookmarkwithtime} /> </span>
                                                            </div>
                                                        </TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{row.backFirst}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{row.layFirst}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{""}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{""}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{row.backSecond}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{row.laySecond}</span></TableCell>
                                                        <TableCell className="pin-icon" style={{ width: "30px" }}><label className="pin-toggle">
                                                            <input type="checkbox" />
                                                            <a className="inplay-last-icon"></a>
                                                        </label></TableCell>
                                                    </TableRow>

                                                    : ""
                                            ))}
                                        </TableBody>
                                    </Table>
                                </AccordionDetails>
                            </Accordion>
                            <Accordion defaultExpanded={true} style={{ marginTop: 10 }}>
                                <AccordionSummary aria-controls="panel1a-content" className="bet-slip-bar" style={{ background: `linear-gradient(-180deg, #2E4B5E 0%, #243A48 82%)` }}>
                                    <Typography>Tennis</Typography>
                                </AccordionSummary>
                                <AccordionDetails className="d-none p-0" display="inline-block" style={{ height: "100%" }}>
                                    <Table className={classes.table} aria-label="simple table">
                                        <TableHead>
                                            <TableRow className="tbl_head xs-none">
                                                <TableCell colSpan="5"></TableCell>
                                                <TableCell align="right" colSpan="2">1</TableCell>
                                                <TableCell align="right" colSpan="2">x</TableCell>
                                                <TableCell align="right" colSpan="2">2</TableCell>
                                                <TableCell align="right"></TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody className="tbl_body">
                                            {eventListData.map((row) => (
                                                row.sport == "Tennis" ?


                                                    <TableRow onClick={() => history.push(`/full-market/${row.event_id}`)} p={1} key={row.action} className="odd_even_clr" button>
                                                        <TableCell className="table_first_row" colSpan="5">
                                                            <div className="md-none inplay-xs-title">
                                                                <FiberManualRecordIcon className="circle-size v-none" />
                                                                <img src={InPlayImg} alt="skyexchange" />
                                                                <img src={InPlayImg} alt="skyexchange" />
                                                                <span className="">{row.action}</span>
                                                                <span className="game-name xs-game-name"><img src={tennisE} alt="cricket" /></span>
                                                            </div>
                                                            <div className="text_left-in-play">
                                                                <FiberManualRecordIcon className="circle-size" />
                                                                <Link to='#' onClick={() => history.push(`/full-market/${row.event_id}`)} style={{ color: '#2789CE' }}>
                                                                    <Box display="contents">
                                                                        <span> {row.name.split("v")[0]} </span>
                                                                        <span className="in-play"> v </span>
                                                                        <span>{row.name.split("v")[1]}</span>
                                                                    </Box>
                                                                </Link>
                                                                <span className="in-play xs-none"> {row.action} <img alt="bookMark" src={bookmarkwithtime} /> </span>
                                                                <span className="game-name xs-none"><img src={cricketE} alt="cricket" /></span>
                                                                <span className="in-play xs-none icon-on-right"><img alt="bookMark" src={bookmarkwithtime} /> </span>
                                                            </div>
                                                        </TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{row.backFirst}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{row.layFirst}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{""}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{""}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{row.backSecond}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{row.laySecond}</span></TableCell>
                                                        <TableCell className="pin-icon" style={{ width: "30px" }}><label className="pin-toggle">
                                                            <input type="checkbox" />
                                                            <a className="inplay-last-icon"></a>
                                                        </label></TableCell>
                                                    </TableRow>
                                                    : ""
                                            ))}
                                        </TableBody>
                                    </Table>
                                </AccordionDetails>
                            </Accordion>
                        </TabPanel>
                        <TabPanel value={value} index={1} className="in-play-table">
                            <div className="in-play-box xs-none">
                                <Box display="flex" p={1} bgcolor="background.paper">
                                    <p varient="h5"><b>Sport Filter:</b></p>
                                    <p p={2}>
                                        <span className="paddingX-1">
                                            <span className="paddingX-1">Cricket</span>
                                            <FiberManualRecordIcon className="circle-size" />
                                            <span className="paddingX-1">Soccer</span>
                                            <FiberManualRecordIcon className="circle-size" />
                                            <span className="paddingX-1">Tennis</span>
                                        </span>
                                    </p>
                                    <Button variant="outlined" style={{ height: 30, backgroundColor: '#fff', }}>Filter</Button>
                                </Box>
                            </div>
                            <BrowserView>
                                <div className={classes.heightSet}>
                                    <Paper className={classes.paperStyle}>
                                        <List component="p" aria-label="secondary mailbox folders" className={classes.listStyle}>
                                            {eventListData.map((item) => (
                                                 1 == 1
                                                    ?
                                                    <div>
                                                        <ListItemLink className="xs-none" href={item.link} p={1}>
                                                            <Typography className={classes.time}>{item.value}</Typography>
                                                            <Typography className={classes.textSize}>
                                                                <span>{item.sport}</span>
                                                                <PlayArrowIcon color="disabled" className={classes.textSize} />
                                                                <span>{item.name}</span>
                                                                <PlayArrowIcon color="disabled" className={classes.textSize} />
                                                                <Link to='/full-market' style={{ color: '#2789CE' }}>
                                                                    <b>{item.name}</b>
                                                                </Link>
                                                            </Typography>
                                                        </ListItemLink>
                                                        <div className="iplay-now-head md-none">
                                                            <img src={InPlayImg} alt="skyexchange" />
                                                            <span className="">{item.time}</span>
                                                        </div>
                                                        <div className="inplay-now-xsblock md-none">
                                                            <FiberManualRecordIcon className="circle-size" /> &nbsp;
                                                            <Link to='/full-market' style={{ color: '#2789CE' }}>
                                                                <b>{item.from} v {item.to}</b>
                                                            </Link>
                                                        </div>
                                                        <Divider />
                                                    </div>
                                                    : ""
                                            ))}
                                        </List>
                                    </Paper>
                                </div>
                            </BrowserView>
                            <MobileView>
                                <Accordion defaultExpanded={true} style={{ marginTop: 10 }}>
                                    <AccordionSummary aria-controls="panel1a-content" className="bet-slip-bar" style={{ background: `linear-gradient(-180deg, #2E4B5E 0%, #243A48 82%)` }}>
                                        <Typography>Cricket</Typography>
                                    </AccordionSummary>
                                    <AccordionDetails className="d-none p-0" display="inline-block" style={{ height: "100%" }}>
                                        <Table className={classes.table} aria-label="simple table">
                                            <TableHead>
                                                <TableRow className="tbl_head xs-none">
                                                    <TableCell colSpan="5"></TableCell>
                                                    <TableCell align="right" colSpan="2">1</TableCell>
                                                    <TableCell align="right" colSpan="2">x</TableCell>
                                                    <TableCell align="right" colSpan="2">2</TableCell>
                                                    <TableCell align="right"></TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody className="tbl_body">
                                                {eventListData.map((row) => (
                                                    row.is_inplay == "Going Inplay" && row.sport == "Cricket" && String(row.date) == String(today) ?
                                                        <TableRow onClick={() => history.push(`/full-market/${row.event_id}`)} p={1} key={row.name1} className="odd_even_clr" button>
                                                            <TableCell className="table_first_row" colSpan="5">
                                                            <div className="md-none inplay-xs-title">
                                                                <FiberManualRecordIcon className="circle-size v-none" />
                                                                <img src={InPlayImg} alt="skyexchange" />

                                                                {
                                                                    row.is_fancy == "True" ?
                                                                        <>
                                                                            <FiberManualRecordIcon className="circle-size v-none game-fancy" />
                                                                            <img className="fancyImgall" src={FancyImg} alt="skyexchange" />
                                                                        </> : null
                                                                }

                                                                {
                                                                    row.is_bookmaker == "True" ?
                                                                        <>
                                                                            <FiberManualRecordIcon className="circle-size v-none game-fancy" />
                                                                            <img src={BookmakerImg} alt="skyexchange" />
                                                                        </> : null
                                                                }

                                                                {/* <img src={InPlayImg} alt="skyexchange" /> */}
                                                                <span className="time">{row.time}</span>

                                                            </div>
                                                                <div className="text_left-in-play">
                                                                    <FiberManualRecordIcon className=" going-inplay-cricle" />

                                                                    <Link to="#" onClick={() => history.push(`/full-market/${row.event_id}`)} style={{ color: '#2789CE' }}>
                                                                        <Box display="contents">
                                                                        { 
                                                                            row.name.length > 36 ?
                                                                            `${row.name.substring(0, 36)}...` :
                                                                            
                                                                            row.name 
                                                                
                                                                        }
                                                                            {/* <span> {row.name.split("v")[0]} </span>
                                                                            <span className="in-play"> v </span>
                                                                            <span>{row.name.split("v")[1]}</span> */}
                                                                        </Box>
                                                                    </Link>
                                                                    <span className="in-play xs-none"> {row.action} <img alt="bookMark" src={bookmarkwithtime} /> </span>

                                                                    {/* <span className="in-play xs-none icon-on-right"><img alt="bookMark" src={bookmarkwithtime} /> </span> */}
                                                                </div>
                                                            </TableCell>
                                                            <TableCell className="xs-none" align="right"><span>{row.backFirst}</span></TableCell>
                                                            <TableCell className="xs-none" align="right"><span>{row.layFirst}</span></TableCell>
                                                            <TableCell className="xs-none" align="right"><span>{""}</span></TableCell>
                                                            <TableCell className="xs-none" align="right"><span>{""}</span></TableCell>
                                                            <TableCell className="xs-none" align="right"><span>{row.backSecond}</span></TableCell>
                                                            <TableCell className="xs-none" align="right"><span>{row.laySecond}</span></TableCell>
                                                            <TableCell className="pin-icon" style={{ width: "30px" }}>
                                                                <label className="pin-toggle">
                                                                    <input type="checkbox" />
                                                                    <a className="inplay-last-icon"></a>
                                                                </label>
                                                            </TableCell>
                                                        </TableRow>
                                                        : ""

                                                ))}
                                            </TableBody>
                                        </Table>
                                    </AccordionDetails>
                                </Accordion>
                                <Accordion defaultExpanded={true} style={{ marginTop: 10 }}>
                                    <AccordionSummary aria-controls="panel1a-content" className="bet-slip-bar" style={{ background: `linear-gradient(-180deg, #2E4B5E 0%, #243A48 82%)` }}>
                                        <Typography>Soccer</Typography>
                                    </AccordionSummary>
                                    <AccordionDetails className="d-none p-0" display="inline-block" style={{ height: "100%" }}>
                                        <Table className={classes.table} aria-label="simple table">
                                            <TableHead>
                                                <TableRow className="tbl_head xs-none">
                                                    <TableCell colSpan="5"></TableCell>
                                                    <TableCell align="right" colSpan="2">1</TableCell>
                                                    <TableCell align="right" colSpan="2">2</TableCell>
                                                    <TableCell align="right"></TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody className="tbl_body">

                                            </TableBody>
                                        </Table>
                                    </AccordionDetails>
                                </Accordion>
                                <Accordion defaultExpanded={true} style={{ marginTop: 10 }}>
                                    <AccordionSummary aria-controls="panel1a-content" className="bet-slip-bar" style={{ background: `linear-gradient(-180deg, #2E4B5E 0%, #243A48 82%)` }}>
                                        <Typography>Tennis</Typography>
                                    </AccordionSummary>
                                    <AccordionDetails className="d-none p-0" display="inline-block" style={{ height: "100%" }}>
                                        <Table className={classes.table} aria-label="simple table">
                                            <TableHead>
                                                <TableRow className="tbl_head xs-none">
                                                    <TableCell colSpan="5"></TableCell>
                                                    <TableCell align="right" colSpan="2">1</TableCell>
                                                    <TableCell align="right" colSpan="2">2</TableCell>
                                                    <TableCell align="right"></TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody className="tbl_body">

                                            </TableBody>
                                        </Table>
                                    </AccordionDetails>
                                </Accordion>
                            </MobileView>
                        </TabPanel>
                        <TabPanel value={value} index={2} className='in-play-table'>
                            <div className="in-play-box xs-none">
                                <Box display="flex" p={1} bgcolor="background.paper">
                                    <p varient="h5"><b>Sport Filter:</b></p>
                                    <p p={2}>
                                        <span className="paddingX-1">
                                            <span className="paddingX-1">Cricket</span>
                                            <FiberManualRecordIcon className="circle-size" />
                                            <span className="paddingX-1">Soccer</span>
                                            <FiberManualRecordIcon className="circle-size" />
                                            <span className="paddingX-1">Tennis</span>
                                        </span>
                                    </p>
                                    <Button variant="outlined" style={{ height: 30, backgroundColor: '#fff', }}>Filter</Button>
                                </Box>
                            </div>
                            <BrowserView>
                                <div className={classes.heightSet}>
                                    <Paper className={classes.paperStyle}>
                                        <List component="p" aria-label="secondary mailbox folders" className={classes.listStyle}>
                                            {eventListData.map((row) => (
                                                row.is_inplay == "Going Inplay" && row.sport == "Cricket" && row.action == today ?
                                                    <TableRow onClick={() => history.push(`/full-market/${row.event_id}`)} p={1} key={row.name1} className="odd_even_clr" button>
                                                        <TableCell className="table_first_row" colSpan="5">
                                                        <div className="md-none inplay-xs-title">
                                                                <FiberManualRecordIcon className="circle-size v-none" />
                                                                <img src={InPlayImg} alt="skyexchange" />

                                                                {
                                                                    row.is_fancy == "True" ?
                                                                        <>
                                                                            <FiberManualRecordIcon className="circle-size v-none game-fancy" />
                                                                            <img className="fancyImgall" src={FancyImg} alt="skyexchange" />
                                                                        </> : null
                                                                }

                                                                {
                                                                    row.is_bookmaker == "True" ?
                                                                        <>
                                                                            <FiberManualRecordIcon className="circle-size v-none game-fancy" />
                                                                            <img src={BookmakerImg} alt="skyexchange" />
                                                                        </> : null
                                                                }



                                                                {/* <img src={InPlayImg} alt="skyexchange" /> */}
                                                                <span className="">{row.time}</span>

                                                            </div>
                                                            <div className="text_left-in-play">
                                                                <FiberManualRecordIcon className=" going-inplay-cricle" />

                                                                <Link to="#" onClick={() => history.push(`/full-market/${row.event_id}`)} style={{ color: '#2789CE' }}>
                                                                    <Box display="contents">
                                                                        <span> {row.name.split("v")[0]} </span>
                                                                        <span className="in-play"> v </span>
                                                                        <span>{row.name.split("v")[1]}</span>
                                                                    </Box>
                                                                </Link>
                                                                <span className="in-play xs-none"> {row.action} <img alt="bookMark" src={bookmarkwithtime} /> </span>

                                                                {/* <span className="in-play xs-none icon-on-right"><img alt="bookMark" src={bookmarkwithtime} /> </span> */}
                                                            </div>
                                                        </TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{row.backFirst}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{row.layFirst}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{""}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{""}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{row.backSecond}</span></TableCell>
                                                        <TableCell className="xs-none" align="right"><span>{row.laySecond}</span></TableCell>
                                                        <TableCell className="pin-icon" style={{ width: "30px" }}>
                                                            <label className="pin-toggle">
                                                                <input type="checkbox" />
                                                                <a className="inplay-last-icon"></a>
                                                            </label>
                                                        </TableCell>
                                                    </TableRow>
                                                    : ""

                                            ))}
                                        </List>
                                    </Paper>
                                </div>
                            </BrowserView>
                            <MobileView>
                                <Accordion defaultExpanded={true} style={{ marginTop: 10 }}>
                                    <AccordionSummary aria-controls="panel1a-content" className="bet-slip-bar" style={{ background: `linear-gradient(-180deg, #2E4B5E 0%, #243A48 82%)` }}>
                                        <Typography>Cricket</Typography>
                                    </AccordionSummary>
                                    <AccordionDetails className="d-none p-0" display="inline-block" style={{ height: "100%" }}>
                                        <Table className={classes.table} aria-label="simple table">
                                            <TableHead>
                                                <TableRow className="tbl_head xs-none">
                                                    <TableCell colSpan="5"></TableCell>
                                                    <TableCell align="right" colSpan="2">1</TableCell>
                                                    <TableCell align="right" colSpan="2">x</TableCell>
                                                    <TableCell align="right" colSpan="2">2</TableCell>
                                                    <TableCell align="right"></TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody className="tbl_body ">
                                                {eventListData.map((row) => (
                                                   // row.is_inplay == "Going Inplay" && 
                                                    row.sport == "Cricket" && row.date == tomorrow ?
                                                        <TableRow onClick={() => history.push(`/full-market/${row.event_id}`)} p={1} key={row.name1} className="odd_even_clr" button>
                                                            <TableCell className="table_first_row hellll" colSpan="5">
                                                                <div className="md-none inplay-xs-title">
                                                                    <FiberManualRecordIcon className="circle-size v-none" />

                                                                {
                                                                    row.is_fancy == "True" ?
                                                                    <>
                                                                    <FiberManualRecordIcon className="circle-size v-none game-fancy" />
                                                                    <img className="fancyImgall" src={FancyImg} alt="skyexchange" />
                                                                    </> : null
                                                                }

                                                                {
                                                                    row.is_bookmaker == "True" ?
                                                                    <>
                                                                    <FiberManualRecordIcon className="circle-size v-none" />
                                                                    <img className="bookImgall" src={BookmakerImg} alt="skyexchange" />
                                                                    </> : null
                                                                
                                                                }    
                                                                {/*
                                                                    <FiberManualRecordIcon className="circle-size v-none" />
                                                                    <img src={PremiumImg} alt="skyexchange" />
                                                                */}
                                                                    <span className="time">{`Tomorrow ${row.time}`}</span>

                                                                </div>
                                                                <div className="text_left-in-play">
                                                                    <FiberManualRecordIcon className=" going-inplay-cricle" />

                                                                    <Link to="#" onClick={() => history.push(`/full-market/${row.event_id}`)} style={{ color: '#2789CE' }}>
                                                                        <Box display="contents">
                                                                           {/* {row.name} */ }
                                                                            { 
                                                                                row.name.length > 36 ?
                                                                                `${row.name.substring(0, 30)}...` :
                                                                                
                                                                                row.name 
                                                                
                                                                            } 
                                                                            {/* <span> {row.name.split("v")[0]} </span>
                                                                            <span className="in-play"> v </span>
                                                                            <span>{row.name.split("v")[1]}</span> */}
                                                                        </Box>
                                                                    </Link>
                                                                    <span className="in-play xs-none"> {row.action} <img alt="bookMark" src={bookmarkwithtime} /> </span>

                                                                    {/* <span className="in-play xs-none icon-on-right"><img alt="bookMark" src={bookmarkwithtime} /> </span> */}
                                                                </div>
                                                            </TableCell>
                                                            <TableCell className="xs-none" align="right"><span>{row.backFirst}</span></TableCell>
                                                            <TableCell className="xs-none" align="right"><span>{row.layFirst}</span></TableCell>
                                                            <TableCell className="xs-none" align="right"><span>{""}</span></TableCell>
                                                            <TableCell className="xs-none" align="right"><span>{""}</span></TableCell>
                                                            <TableCell className="xs-none" align="right"><span>{row.backSecond}</span></TableCell>
                                                            <TableCell className="xs-none" align="right"><span>{row.laySecond}</span></TableCell>
                                                            <TableCell className="pin-icon" style={{ width: "30px" }}>
                                                                <label className="pin-toggle">
                                                                    <input type="checkbox" />
                                                                    <a className="inplay-last-icon"></a>
                                                                </label>
                                                            </TableCell>
                                                        </TableRow>
                                                        : ""

                                                ))}
                                            </TableBody>
                                        </Table>
                                    </AccordionDetails>
                                </Accordion>
                                <Accordion defaultExpanded={true} style={{ marginTop: 10 }}>
                                    <AccordionSummary aria-controls="panel1a-content" className="bet-slip-bar" style={{ background: `linear-gradient(-180deg, #2E4B5E 0%, #243A48 82%)` }}>
                                        <Typography>Soccer</Typography>
                                    </AccordionSummary>
                                    <AccordionDetails className="d-none p-0" display="inline-block" style={{ height: "100%" }}>
                                        <Table className={classes.table} aria-label="simple table">
                                            <TableHead>
                                                <TableRow className="tbl_head xs-none">
                                                    <TableCell colSpan="5"></TableCell>
                                                    <TableCell align="right" colSpan="2">1</TableCell>
                                                    <TableCell align="right" colSpan="2">2</TableCell>
                                                    <TableCell align="right"></TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody className="tbl_body">

                                            </TableBody>
                                        </Table>
                                    </AccordionDetails>
                                </Accordion>
                                <Accordion defaultExpanded={true} style={{ marginTop: 10 }}>
                                    <AccordionSummary aria-controls="panel1a-content" className="bet-slip-bar" style={{ background: `linear-gradient(-180deg, #2E4B5E 0%, #243A48 82%)` }}>
                                        <Typography>Tennis</Typography>
                                    </AccordionSummary>
                                    <AccordionDetails className="d-none p-0" display="inline-block" style={{ height: "100%" }}>
                                        <Table className={classes.table} aria-label="simple table">
                                            <TableHead>
                                                <TableRow className="tbl_head xs-none">
                                                    <TableCell colSpan="5"></TableCell>
                                                    <TableCell align="right" colSpan="2">1</TableCell>
                                                    <TableCell align="right" colSpan="2">2</TableCell>
                                                    <TableCell align="right"></TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody className="tbl_body">

                                            </TableBody>
                                        </Table>
                                    </AccordionDetails>
                                </Accordion>
                            </MobileView>
                        </TabPanel>
                    </Grid>

                    <Grid item lg={3} xs={0} spacing={2} className="betSlipGrid xs-none">
                        <BetSlip />
                    </Grid>
                </Grid>
            </div>
        </>
    );
}
